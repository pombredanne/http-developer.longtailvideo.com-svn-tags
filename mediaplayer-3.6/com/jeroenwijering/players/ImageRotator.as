﻿/**
* Manages startup and overall control of the Flash Image Rotator
*
* @author	Jeroen Wijering
* @version	1.4
**/


import com.jeroenwijering.players.*;


class com.jeroenwijering.players.ImageRotator extends AbstractPlayer { 


	/** Array with all config values **/
	public var config:Object = {
		autostart:true,
		backcolor:0x000000,
		callback:undefined,
		enablejs:false,
		file:"playlist.xml",
		frontcolor:0xffffff,
		height:undefined,
		lightcolor:0x990000,
		linkfromdisplay:true,
		linktarget:"",
		logevents:undefined,
		logo:undefined,
		overstretch:false,
		playerclip:undefined,
		repeat:true,
		rotatetime:5,
		showicons:false,
		shownavigation:false,
		shuffle:true,
		transition:"fade",
		width:undefined
	}
	/** Accepted types of mediafiles **/
	private var fileTypes:Array = new Array("jpg","png","gif");


	/** Constructor **/
	function ImageRotator(tgt:MovieClip,fil:String) { super(tgt,fil); };


	/** Setup all necessary MCV blocks. **/
	private function setupMCV():Void {
		controller = new RotatorController(this);
		var rov = new RotatorView(controller,this);
		var ipv = new InputView(controller,this);
		var vws:Array = new Array(rov,ipv);
		if(config["enablejs"] == "true") {
			var jsv = new JavascriptView(controller,this);
			vws.push(jsv);
		}
		if(config["callback"] != undefined) {
			var cbv = new CallbackView(controller,this);
			vws.push(cbv);
		}
		config["displayheight"] = config["height"];
		var im1=new ImageModel(vws,controller,this,config["clip"].img1,true);
		var im2=new ImageModel(vws,controller,this,config["clip"].img2,true);
		var mds:Array = new Array(im1,im2);
		controller.startMCV(mds);
	};


}