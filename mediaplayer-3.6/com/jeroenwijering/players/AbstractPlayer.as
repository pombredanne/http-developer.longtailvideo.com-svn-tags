﻿/**
* Abstract player class, extended by all other players.
* Class loads config and file XML's and sets up MCV triangle.
*
* @author	Jeroen Wijering
* @version	1.8
**/


import com.jeroenwijering.players.*;
import com.jeroenwijering.utils.FeedParser;


class com.jeroenwijering.players.AbstractPlayer { 


	/** Array with all config values **/
	public var config:Object;
	/** Array with all playlist items **/
	public var feed:Array;
	/** Array with all file values **/
	private var fileElements:Object = {
		file:"",
		title:"",
		link:"",
		id:"",
		image:"",
		author:"",
		captions:"",
		category:"",
		start:"",
		type:""
	}
	/** Accepted types of mediafiles **/
	private var fileTypes:Array;
	/** reference to the XML parser **/
	private var feedParser:FeedParser;
	/** reference to the controller **/
	private var controller:AbstractController;


	/**
	* Player application startup
	*
	* @param tgt	movieclip that contains all player graphics
	* @param fil	file that should be played
	**/
	public function AbstractPlayer(tgt:MovieClip,fil:String) {
		config["clip"] = tgt;
		config["clip"]._visible = false;
		fil == undefined ? null: config["file"] = fil;
		loadConfig();
	};


	/** Set config variables or load them from flashvars. **/
	private function loadConfig() {
		config["width"] = Stage.width;
		config["height"] = Stage.height;
		for(var cfv in config) {
			if(_root[cfv] != undefined) {
				config[cfv] = unescape(_root[cfv]);
			}
		}
		if (config["displayheight"] == undefined) {
			if(config["largecontrols"] == "true") { var mod = 2; }
			else { var mod = 1; }
			config["displayheight"] = config["height"] - 20*mod;
		}
		if (config["displaywidth"] == undefined) {
			config["displaywidth"] = config["width"];
		}
		config["enablejs"] == "true" ? enableLoadFile(): null;
		loadFile(config["file"]);
	};


	/** 
	* Load an XML playlist or single media file.
	* 
	* @param fil	The file to load. It can be an array or single file string
	* @param tit,lnk,fid,img,aut,cap,cat,stt,typ	Additional file elements.
	**/
	public function loadFile(fil,tit,lnk,fid,img,aut,cap,cat,stt,typ) {
		if(controller != undefined) {
			controller.getEvent("stop");
			delete controller;
		}
		var i = 0;
		for (var itm in fileElements) {
			if(arguments[i] != undefined) { _root[itm] = arguments[i] }
			i++;
		}
		feed = new Array();
		var ftp = "list";
		for(var i=0; i<fileTypes.length; i++) {
			if(fil.substr(-3) == fileTypes[i] || _root.type == fileTypes[i] ||
				fil.substr(0,4)  == fileTypes[i]) {
				ftp = fileTypes[i]; 
			}
		}
		if (ftp == "list") {
			var ref = this;
			feedParser = new FeedParser();
			feedParser.onParseComplete = function() { 
				ref.feed = this.parseArray;
				ref.config["clip"]._visible = true;
				_root.activity._visible = false;
				ref.setupMCV();
			};
			feedParser.parse(fil);
		} else {
			feed[0] = new Object();
			feed[0]['type'] = ftp;
			var pso = SharedObject.getLocal("com.jeroenwijering.players","/");
			for(var cfv in fileElements) {
				if(_root[cfv] != undefined) { 
					feed[0][cfv] = unescape(_root[cfv]);  
				}
			}
			config["clip"]._visible = true;
			_root.activity._visible = false;
			setupMCV();
		}	
	};


	/** Setup all necessary MCV blocks. **/
	private function setupMCV() {
		controller = new AbstractController(this);
		var asv = new AbstractView(controller,this);
		var vws:Array = new Array(asv);
		var asm = new AbstractModel(vws,controller,this);
		var mds:Array = new Array(asm);
		controller.startMCV(mds);
	};


	/** Enable javascript access to loadFile command **/
	private function enableLoadFile() {
		if(flash.external.ExternalInterface.available) {
			var lfc = flash.external.ExternalInterface.addCallback("loadFile",
				this,loadFile);
		}
	};


}