﻿/**
* User input management of the players MCV pattern.
*
* @author	Jeroen Wijering
* @version	1.4
**/


import com.jeroenwijering.players.*;
import com.jeroenwijering.utils.Randomizer;


class com.jeroenwijering.players.PlayerController extends AbstractController {


	/** use SharedObject to save current file, item and volume **/
	private var playerSO:SharedObject;
	/** reference to the current volume **/
	private var currentVolume:Number;
	/** Array with all file values **/
	private var fileElements:Object = {
		file:"",
		title:"",
		link:"",
		id:"",
		image:"",
		author:"",
		captions:"",
		category:"",
		start:"",
		type:""
	}
	/** LoadVars object for event logging **/
	private var logObject:LoadVars;


	/** Constructor, save arrays and set currentItem. **/
	function PlayerController(ply:Object) {
		super(ply);
		player.config["clip"].captions._visible = false;
		if(playerSO.data.showcaptions != "false") { setCaptions(); }
		if(player.config["logevents"] != undefined) { 
			logObject = new LoadVars();
		}
	};


	/** Complete the build of the MCV cycle and start flow of events. **/
	public function startMCV(mar:Array) {
		registeredModels = mar;
		sendChange("item",currentItem);
		if(player.config["autostart"] != "true" && 
			player.feed[currentItem]["category"] != "commercial") {
			sendChange("pause",0);
			isPlaying = false;
		} else { 
			sendChange("start",0);
			isPlaying = true;
		}
		if(playerSO.data.volume != undefined && _root.volume == undefined) {
			sendChange("volume",playerSO.data.volume);
			currentVolume = playerSO.data.volume;
		} else {
			sendChange("volume",Number(player.config["volume"]));
			currentVolume = Number(player.config["volume"]);
		}
	};


	/** Receive events from the views. **/
	public function getEvent(typ:String,prm:Number):Void {
		super(typ,prm);
		if(player.config["logevents"] != undefined) { logControls(typ,prm); }
	};


	/** Check volume percentage and forward to models. **/
	private function setVolume(prm) {
		if (prm < 0 ) { prm = 0; } else if (prm > 100) { prm = 100; }
		if(currentVolume == 0 && prm == 0) { prm = 80; }
		currentVolume = prm;
		sendChange("volume",prm);
		playerSO.data.volume = prm;
		playerSO.flush();
	};


	
	/** Fullscreen switch function. **/
	private function setFullscreen() {
		if(Stage["displayState"] == "normal") { 
			Stage["displayState"] = "fullScreen";
		} else if (Stage["displayState"] == "fullScreen") {
			Stage["displayState"] = "normal";
		} else {
			if(player.config["fullscreenmode"] == "false") { 
				getURL(unescape(player.config["fsreturnpage"]));
			} else {
				if(player.feed.length == 1) {
					for(var elm in fileElements) {
						playerSO.data[elm] = player.feed[0][elm];
					}
				} else {
					playerSO.data.item = currentItem;
				} 
				playerSO.data.file = player.config["file"];
				playerSO.data.fsreturnpage = player.config["fsreturnpage"];
				playerSO.data.largecontrols = player.config["largecontrols"];
				playerSO.flush();
				sendChange("stop");
				getURL(unescape(player.config["fullscreenpage"]),
					player.config["linktarget"]);
			}
		}
	};


	/** Captions toggle **/
	private function setCaptions() {
		if(captionsOn == true) {
			captionsOn = false;
			player.config["clip"].captions._visible = false;
		} else {
			captionsOn = true;
			player.config["clip"].captions._visible = true;
		}
		playerSO.data.capon = captionsOn;
		playerSO.flush();
	};


	/** logging all button presses to a certain callback **/
	private function logControls(typ:String,prm:Number) {
		logObject.file = player.feed[currentItem]["file"];
		logObject.id = player.feed[currentItem]["id"];
		logObject.type = typ;
		logObject.parameter = prm;
		logObject.sendAndLoad(player.config["logevents"],logObject,"POST");
	};


}