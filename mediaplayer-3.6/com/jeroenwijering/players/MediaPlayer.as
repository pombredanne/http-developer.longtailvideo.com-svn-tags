﻿/**
* Player that reads all media formats Flash can read.
*
* @author	Jeroen Wijering
* @version	1.8
**/


import com.jeroenwijering.players.*;


class com.jeroenwijering.players.MediaPlayer extends AbstractPlayer { 


	/** Array with all config values **/
	private var config:Object = {
		autoscroll:false,
		autostart:false,
		backcolor:0xffffff,
		bufferlength:5,
		callback:undefined,
		clip:undefined,
		displayheight:undefined,
		displaywidth:undefined,
		enablejs:false,
		file:"playlist.xml",
		frontcolor:0x000000,
		height:undefined,
		largecontrols:false,
		lightcolor:0x000000,
		linkfromdisplay:false,
		linktarget:"",
		logevents:undefined,
		logo:undefined,
		overstretch:"false",
		repeat:false,
		rotatetime:10,
		showcaptions:true,
		showdigits:true,
		showeq:false,
		showfsbutton:true,
		showicons:true,
		shuffle:true,
		streamscript:undefined,
		thumbsinplaylist:false,
		volume:80,
		width:undefined
	}
	/** Accepted types of mediafiles **/
	private var fileTypes:Array = new Array(
		"mp3",
		"flv",
		"rtmp",
		"jpg",
		"png",
		"gif",
		"swf",
		"rbs"
	);


	/** Constructor **/
	public function MediaPlayer(tgt:MovieClip,fil:String) { super(tgt,fil); };


	/** Setup all necessary MCV blocks. **/
	private function setupMCV() {
		// set controller
		controller = new PlayerController(this);
		// set default views
		var dpv = new DisplayView(controller,this);
		var ipv = new InputView(controller,this);
		var vws:Array = new Array(dpv,ipv);
		// set optional views
		if(config["displayheight"] < config["height"]) {
			var cbv = new ControlbarView(controller,this);
			vws.push(cbv);
		} else {
			config["clip"].controlbar._visible = false;
		}
		if(config["displayheight"] < config["height"] - 40 || 
			config["displaywidth"] < config["width"] - 20) {
			var plv = new PlaylistView(controller,this);
			vws.push(plv);
		} else {
			config["clip"].playlist._visible = false; 
		}
		if(config["showeq"] == "true") {
			var eqv = new EqualizerView(controller,this);
			vws.push(eqv);
		} else {
			config["clip"].equalizer._visible = false;
		}
		if(feed[0]["captions"] != undefined) {
			var cpv = new CaptionsView(controller,this);
			vws.push(cpv);
		} else {
			config["clip"].captions._visible = false;
		}
		if(config["enablejs"] == "true") {
			var jsv = new JavascriptView(controller,this);
			vws.push(jsv);
		}
		if(config["callback"] != undefined) {
			var cav = new CallbackView(controller,this);
			vws.push(cav);
		}
		// set models
		var mp3 = new MP3Model(vws,controller,this,
			config["clip"]);
		var flv = new FLVModel(vws,controller,this,
			config["clip"].display.video);
		var img = new ImageModel(vws,controller,this,
			config["clip"].display.image);
		var mds:Array = new Array(mp3,flv,img);
		// start mcv cycle
		controller.startMCV(mds);
	};


}