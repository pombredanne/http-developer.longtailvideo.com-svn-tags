﻿/**
* Abstract controller class of the MCV pattern, extended by all controlllers.
*
* @author	Jeroen Wijering
* @version	1.5
**/


import com.jeroenwijering.players.*;
import com.jeroenwijering.utils.Randomizer;


class com.jeroenwijering.players.AbstractController {


	/** Randomizer instance **/
	private var  randomizer:Randomizer;
	/** array with all registered models **/
	private var registeredModels:Array;
	/** reference to the config array **/
	private var player:Object;
	/** Current item **/
	private var currentItem:Number;
	/** Current item **/
	private var isPlaying:Boolean;
	/** Number of items played to track play-once repeat **/
	private var itemsPlayed:Number = 0;


	/**
	* Constructor, save arrays and set currentItem.
	*
	* @param car	reference to the config array
	* @param far	reference to the file array
	**/
	function AbstractController(ply:Object) {
		player = ply;
		if(player.config["shuffle"] == "false") {
			currentItem = 0;
		} else {
			randomizer = new Randomizer(player.feed);
			currentItem = randomizer.pick();
		}
	};


	/**
	* Complete the build of the MCV cycle and start flow of events.
	*
	* @param mar	All models the controller should send events to.
	**/
	public function startMCV(mar:Array) {};


	/**
	* Receive events from the views.
	* 
	* @param typ	Event type.
	* @param prm	Parameter value.
	**/
	public function getEvent(typ:String,prm:Number):Void { 
		if( player.feed[currentItem]["category"] == "commercial" && 
			typ != "volume" && typ != "getlink" && typ != "complete") {
			return;
		}
		trace("controller: "+typ+": "+prm);
		switch(typ) {
			case "playpause": 
				setPlaypause();
				break;
			case "prev":
				setPrev();
				break;
			case "next":
				setNext();
				break;
			case "stop":
				setStop();
				break;
			case "scrub":
				setScrub(prm);
				break;
			case "volume":
				setVolume(prm);
				break;
			case "playitem":
				setPlayitem(prm);
				break;
			case "getlink":
				setGetlink(prm);
				break;
			case "fullscreen":
				setFullscreen();
				break;
			case "complete":
				setComplete();
				break;
			case "captions":
				setCaptions();
				break;
			default:
				trace("controller: incompatible event received");
				break;
		}
	};


	/** PlayPause switch **/
	private  function setPlaypause() {
		if(isPlaying == true) {
			isPlaying = false;
			sendChange("pause");
		} else { 
			isPlaying = true;
			sendChange("start");
		}
	};


	/** Play previous item. **/
	private  function setPrev() {
		if(currentItem == 0) { var i:Number = player.feed.length - 1; } 
		else { var i:Number = currentItem-1; }
		setPlayitem(i);
	};


	/** Play next item. **/
	private function setNext() {
		if(currentItem == player.feed.length - 1) { var i:Number = 0; } 
		else { var i:Number = currentItem+1; }
		setPlayitem(i);
	};


	/** Stop and clear item. **/
	private function setStop() { 
		sendChange("pause",0);
		sendChange("stop");
		sendChange("item",currentItem);
		isPlaying = false;
	};


	/** Forward scrub number to model. **/
	private function setScrub(prm) {
		isPlaying == true ? sendChange("start",prm): sendChange("pause",prm);
	};


	/** Play a new item. **/
	private function setPlayitem(itm:Number) {
		if(itm != currentItem) {
			sendChange("stop");
			itm > player.feed.length-1 ? itm = player.feed.length-1: null;
			currentItem = itm;
			sendChange("item",itm);
		}
		if(player.feed[itm]["start"] == undefined) {
			sendChange("start",0);
		} else {
			sendChange("start",player.feed[itm]["start"]);
		}
		isPlaying = true;
	};


	/** Get url from an item if link exists, else playpause. **/
	private function setGetlink(idx:Number) {
		if(player.feed[idx]["link"] == undefined) { 
			setPlaypause();
		} else {
			getURL(player.feed[idx]["link"],player.config["linktarget"]);
		}
	};


	/** Determine what to do if an item is completed. **/
	private function setComplete() { 
		itemsPlayed++;
		if(player.config["repeat"]=="false" || (player.config["repeat"]=="list"
		 	&& itemsPlayed==player.feed.length)) {
			sendChange("pause",0);
			isPlaying = false;
			itemsPlayed = 0;
		} else {
			if(player.config["shuffle"] == "true") {
				var i:Number = randomizer.pick();
			} else if(currentItem == player.feed.length - 1) {
				var i:Number = 0;
			} else { 
				var i:Number = currentItem+1;
			}
			setPlayitem(i);
		}
	};


	/** Volume event handler **/
	private function setVolume(prm:Number) {};


	/** Switch fullscreen mode **/
	private function setFullscreen() {};


	/** Switch captions on and off **/
	private function setCaptions() {};


	/** Sending changes to all registered models. **/
	private function sendChange(typ:String,prm:Number):Void {
		for(var i=0; i<registeredModels.length; i++) {
			registeredModels[i].getChange(typ,prm);
		}
	};


}