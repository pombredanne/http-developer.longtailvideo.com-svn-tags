.. _overview:

JW Player for Flash
===================

Introduction
------------

The JW Player for Flash is the Internet’s most popular and flexible media player. It supports playback of any format the Adobe Flash Player can handle, as well as HTTP and RTMP streaming and various XML playlist formats. A wide range of settings (flashvars) can be set, and an extensive javascript API is available. The player's skinning functionality allows you to completely customize its look, and the plugin architecture allows you to easily extend the player with features such as sharing, analytics and ad serving.

Version 5 marks the first time the JW Player can be built entirely using free and open-source software.