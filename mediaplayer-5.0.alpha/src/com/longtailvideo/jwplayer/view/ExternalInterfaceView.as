package com.longtailvideo.jwplayer.view {
	import com.longtailvideo.jwplayer.player.Player;

	/**
	 * Represents link to Javascript API
	 *
	 * @author Pablo Schklowsky
	 */
	public class ExternalInterfaceView {

		/** Constructor **/
		public function ExternalInterfaceView(player:Player) {}

	}
}