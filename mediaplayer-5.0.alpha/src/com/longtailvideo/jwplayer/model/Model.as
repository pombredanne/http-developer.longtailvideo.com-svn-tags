package com.longtailvideo.jwplayer.model {
	import com.longtailvideo.jwplayer.events.GlobalEventDispatcher;
	import com.longtailvideo.jwplayer.events.MediaEvent;
	import com.longtailvideo.jwplayer.events.PlayerEvent;
	import com.longtailvideo.jwplayer.events.PlayerStateEvent;
	import com.longtailvideo.jwplayer.media.HTTPMediaProvider;
	import com.longtailvideo.jwplayer.media.ImageMediaProvider;
	import com.longtailvideo.jwplayer.media.MediaProvider;
	import com.longtailvideo.jwplayer.media.RTMPMediaProvider;
	import com.longtailvideo.jwplayer.media.SoundMediaProvider;
	import com.longtailvideo.jwplayer.media.VideoMediaProvider;
	import com.longtailvideo.jwplayer.media.YouTubeMediaProvider;
	import com.longtailvideo.jwplayer.player.PlayerState;
	
	import flash.events.Event;

	/**
	 * @eventType com.longtailvideo.jwplayer.events.MediaEvent.JWPLAYER_MEDIA_BUFFER
	 */
	[Event(name="jwplayerMediaBuffer", type = "com.longtailvideo.jwplayer.events.MediaEvent")]

	/**
	 * @eventType com.longtailvideo.jwplayer.events.MediaEvent.JWPLAYER_MEDIA_LOADED
	 */
	[Event(name="jwplayerMediaLoaded", type = "com.longtailvideo.jwplayer.events.MediaEvent")]

	/**
	 * @eventType com.longtailvideo.jwplayer.events.MediaEvent.JWPLAYER_MEDIA_TIME
	 */
	[Event(name="jwplayerMediaTime", type = "com.longtailvideo.jwplayer.events.MediaEvent")]

	/**
	 * @eventType com.longtailvideo.jwplayer.events.MediaEvent.JWPLAYER_MEDIA_VOLUME
	 */
	[Event(name="jwplayerMediaVolume", type = "com.longtailvideo.jwplayer.events.MediaEvent")]

	/**
	 * @eventType com.longtailvideo.jwplayer.events.PlayerStateEvent.JWPLAYER_PLAYER_STATE
	 */
	[Event(name="jwplayerPlayerState", type = "com.longtailvideo.jwplayer.events.PlayerStateEvent")]

	/**
	 * @eventType com.longtailvideo.jwplayer.events.PlayerEvent.JWPLAYER_ERROR
	 */
	[Event(name="jwplayerError", type = "com.longtailvideo.jwplayer.events.PlayerEvent")]

	/**
	 * @author Pablo Schklowsky
	 */
	public class Model extends GlobalEventDispatcher {
		private var _config:PlayerConfig;
		private var _playlist:Playlist;

		private var _mute:Boolean = false;
		private var _fullscreen:Boolean = false;

		private var _currentMedia:MediaProvider;

		private var _mediaSources:Object;
		
		/** Constructor **/
		public function Model() {
			_playlist = new Playlist();
			_config = new PlayerConfig(_playlist);

			_playlist.addGlobalListener(forwardEvents);

			setupMediaProviders();
			
			//TODO: Set initial mute state based on user configuration
		}

		/** The player config object **/
		public function get config():PlayerConfig {
			return _config;
		}

		public function set config(conf:PlayerConfig):void {
			_config = conf;
		}

		/** The currently loaded MediaProvider **/
		public function get media():MediaProvider {
			return _currentMedia;
		}

		/**
		 * The current player state
		 */
		public function get state():String {
			return _currentMedia ? _currentMedia.state : PlayerState.IDLE;
		}

		/**
		 * The loaded playlist
		 */
		public function get playlist():Playlist {
			return _playlist;
		}

		/** The current fullscreen state of the player **/
		public function get fullscreen():Boolean {
			return _fullscreen;
		}

		public function set fullscreen(b:Boolean):void {
			_fullscreen = b;
		}

		/** The current mute state of the player **/
		public function get mute():Boolean {
			return _mute;
		}

		public function set mute(b:Boolean):void {
			_mute = b;
			_currentMedia.mute(b);
		}

		private function setupMediaProviders():void {
			_mediaSources = {};
			setMediaProvider('default', new MediaProvider());
			setMediaProvider('video', new VideoMediaProvider());
			setMediaProvider('http', new HTTPMediaProvider());
			setMediaProvider('rtmp', new RTMPMediaProvider());
			setMediaProvider('sound', new SoundMediaProvider());
			setMediaProvider('image', new ImageMediaProvider());
			setMediaProvider('youtube', new YouTubeMediaProvider());

			setActiveMediaProvider('default');
		}

		/**
		 * Whether the Model has a MediaProvider handler for a given type.
		 */
		public function hasMediaProvider(type:String):Boolean {
			return (_mediaSources[url2type(type)] is MediaProvider);
		}

		/**
		 * Add a MediaProvider to the list of available sources.
		 */
		public function setMediaProvider(type:String, provider:MediaProvider):void {
			if (!hasMediaProvider(type)) {
				_mediaSources[url2type(type)] = provider;
				provider.initializeMediaProvider(config);
			}
		}

		public function setActiveMediaProvider(type:String):Boolean {
			if (!hasMediaProvider(type))
				type = "video";

			var newMedia:MediaProvider = _mediaSources[url2type(type)] as MediaProvider;

			if (_currentMedia != newMedia) {
				if (_currentMedia) {
					_currentMedia.removeGlobalListener(forwardEvents);
				}
				newMedia.addGlobalListener(forwardEvents);
				_currentMedia = newMedia;
			}

			return true;
		}

		
		private function forwardEvents(evt:Event):void {
			if (evt is PlayerEvent) {
				if (evt.type == MediaEvent.JWPLAYER_MEDIA_ERROR) {
					// Translate media error into player error.
					dispatchEvent(new PlayerEvent(PlayerEvent.JWPLAYER_ERROR, (evt as MediaEvent).message));
				} else {
					dispatchEvent(evt);
				}
			}
		}

		/** e.g. http://providers.longtailvideo.com/5/myProvider.swf --> myprovider **/
		private function url2type(type:String):String {
			return type.substring(type.lastIndexOf("/") + 1, type.length).replace(".swf", "").toLowerCase();
		}

	}
}