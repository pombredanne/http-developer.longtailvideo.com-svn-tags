package com.longtailvideo.jwplayer.player {
	
	
	public class PlayerVersion {
		protected static var _version:String = "5.2.1151";
		
		public static function get version():String {
			return _version;
		}
		
		public static var id:String = "";
	}
}