package com.longtailvideo.jwplayer.player {
	
	
	public class PlayerVersion {
		protected static var _version:String = "5.0.674 rc1";
		
		public static function get version():String {
			return _version;
		}
	}
}