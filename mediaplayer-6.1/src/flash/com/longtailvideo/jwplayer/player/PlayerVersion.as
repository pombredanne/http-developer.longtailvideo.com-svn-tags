package com.longtailvideo.jwplayer.player {
	
	
	public class PlayerVersion {
		protected static var _version:String = '6.1.2972';
		
		public static function get version():String {
			return _version;
		}
		
		public static var id:String = "";
	}
}