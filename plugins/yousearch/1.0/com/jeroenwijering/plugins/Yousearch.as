/**
* Show a YouTube searchbar that loads the results into the player.
**/
package com.jeroenwijering.plugins {


import com.jeroenwijering.events.*;

import flash.display.*;
import flash.events.*;
import flash.text.*;


public class Yousearch extends MovieClip implements PluginInterface {


	/** Embed of the Yousearch backdrop. **/
	[Embed(source="../../../yousearch.png")]
	private const Backdrop:Class;


	/** Reference to the graphics. **/
	public var config:Object = {
		script:'http://gdata.youtube.com/feeds/api/videos?vq=QUERY&format=5'
	};
	/** Reference to the graphics. **/
	public var clip:MovieClip;
	/** Reference to the View of the player. **/
	private var view:AbstractView;
	/** Reference to the background image. **/
	private var back:Sprite;
	/** Reference to the textfield. **/
	private var query:TextField;


	/** Constructor. **/
	public function Yousearch():void {
		clip = this;
	};


	/** Build the clip graphics. **/
	private function buildClip():void {
		back = new Sprite();
		back.addChild(new Backdrop());
		clip.addChild(back);
		query = new TextField();
		query.type = TextFieldType.INPUT;
		query.defaultTextFormat = new TextFormat("_sans",12,0x000000);
		query.width = 180;
		query.height = 30;
		query.x = 10;
		query.y = 10;
		query.text = '...';
		clip.addChild(query);
	};


	/** Start a search. **/
	private function clickHandler(evt:MouseEvent=null):void {
		var que:String = encodeURI(query.text);
		if(que.length > 3) { 
			view.sendEvent('LOAD',config['script'].replace('QUERY',que));
		}
		query.text = '';
	};


	/** Clear the field on focus. **/
	private function focusHandler(evt:FocusEvent):void {
		if(query.text == '...') {
			query.text = '';
		}
	};


	/** The initialize call is invoked by the player View. **/
	public function initializePlugin(vie:AbstractView):void {
		view = vie;
		view.addModelListener(ModelEvent.STATE,stateHandler);
		view.addControllerListener(ControllerEvent.RESIZE,resizeHandler);
		view.config['icons'] = false;
		buildClip();
		back.addEventListener(MouseEvent.CLICK,clickHandler);
		back.buttonMode = true;
		back.mouseChildren = false;
		query.addEventListener(FocusEvent.FOCUS_IN,focusHandler);
		query.addEventListener(KeyboardEvent.KEY_DOWN,keyHandler);
		resizeHandler();
	};


	/** Start the search when pressing the enter key. **/
	private function keyHandler(evt:KeyboardEvent):void {
		if(evt.charCode == 13) { 
			clickHandler();
		}
	};


	/** Handle a resize. **/
	private function resizeHandler(evt:ControllerEvent=undefined):void {
		clip.x = view.config['width']/2-140;
		clip.y = view.config['height']/2-20;
	};


	/** Show the searchbox on video completed. **/
	private function stateHandler(evt:ModelEvent):void { 
		switch(evt.data.newstate) {
			case ModelStates.BUFFERING:
			case ModelStates.PLAYING:
				clip.visible = false;
				break;
			default:
				clip.stage.focus = query;
				clip.visible = true;
				break;
		}
	};


}


}