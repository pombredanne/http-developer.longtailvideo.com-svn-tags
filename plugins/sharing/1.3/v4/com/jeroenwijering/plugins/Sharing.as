package com.jeroenwijering.plugins {


import com.jeroenwijering.events.*;

import flash.display.MovieClip;
import flash.display.Sprite;
import flash.events.MouseEvent;
import flash.external.ExternalInterface;
import flash.net.navigateToURL;
import flash.net.URLRequest;
import flash.text.TextField;
import flash.text.TextFormat;
import flash.system.System;
import flash.utils.setTimeout;


/**
* Sharing plugin; shows a "share" and "embed" button in the (4.5+) dock or (4.3+) controlbar.
**/
public class Sharing extends MovieClip implements PluginInterface {


	[Embed(source="../../../embedButton.png")]
	private const EmbedButton:Class;
	[Embed(source="../../../embedIcon.png")]
	private const EmbedIcon:Class;
	[Embed(source="../../../embedScreen.png")]
	private const EmbedScreen:Class;
	[Embed(source="../../../shareButton.png")]
	private const ShareButton:Class;
	[Embed(source="../../../shareIcon.png")]
	private const ShareIcon:Class;
	[Embed(source="../../../shareScreen.png")]
	private const ShareScreen:Class;


	/** List with configuration settings. **/
	public var config:Object = {
		code:undefined,
		link:'',
		title:'Great%20video'
	};
	/** Reference to the View of the player. **/
	private var view:AbstractView;
	/** Reference to the embed container. **/
	private var embed:Sprite;
	/** Reference to the embed graphics. **/
	private var embedScreen:Sprite;
	/** Reference to the embed textfield. **/
	private var embedField:TextField;
	/** Reference to the share graphics. **/
	private var share:Sprite;
	/** Reference to the share graphics. **/
	private var shareScreen:Sprite;
	/** Reference to the share textfield. **/
	private var shareField:TextField;
	/** Reference to the transparent bg area. **/
	private var back:Sprite;
	/** Remenber which item is visible. **/
	private var showing:String;


	/** Constructor; builds the background area. **/
	public function Sharing():void {
		back = new Sprite();
		embed = new Sprite();
		share = new Sprite();
		back.graphics.beginFill(0x000000,0);
		back.graphics.drawRect(0,0,320,240);
		back.buttonMode = true;
		back.addEventListener(MouseEvent.CLICK,backHandler);
		addChild(back);
	};


	/** Screenground clicked; hide the screen. **/
	private function backHandler(evt:MouseEvent=null):void {
		showHide('none');
	};


	/** Embed area is clicked; select the code and copy to clipboard.. **/
	private function codeHandler(evt:MouseEvent):void {
		stage.focus = embedField;
		embedField.setSelection(0,999999);
		System.setClipboard(config['code']);
	};


	/** Set the embed buttons and graphics. **/
	private function drawEmbed():void {
		if(view.getPlugin('dock') && view.config['dock']) {
		 	view.getPlugin('dock').addButton(new EmbedIcon(),'embed',embedHandler);
		} else if (view.getPlugin('controlbar')) {
		 	view.getPlugin('controlbar').addButton(new EmbedButton(),'embed',embedHandler);
		}
		embedScreen = new Sprite();
		embedScreen.addChild(new EmbedScreen());
		embedScreen.buttonMode = true;
		embedScreen.addEventListener(MouseEvent.CLICK,codeHandler);
		embed.addChild(embedScreen);
		embedField = new TextField();
		embedField.defaultTextFormat = new TextFormat('_sans',11);
		embedField.multiline = true;
		embedField.wordWrap = true;
		embedField.x = 22;
		embedField.y = 32;
		embedField.text = config['code'];
		embedField.width = 258;
		embedField.height = 64;
		embed.addChild(embedField);
		addChild(embed);
	};


	/** Set the embed buttons and graphics. **/
	private function drawShare():void {
		if(view.getPlugin('dock') && view.config['dock']) {
		 	view.getPlugin('dock').addButton(new ShareIcon(),'share',shareHandler);
		} else if (view.getPlugin('controlbar')) {
			view.getPlugin('controlbar').addButton(new ShareButton(),'share',shareHandler);
		}
		shareScreen = new Sprite();
		shareScreen.addChild(new ShareScreen());
		shareScreen.buttonMode = true;
		shareScreen.addEventListener(MouseEvent.CLICK,linkHandler);
		share.addChild(shareScreen);
		shareField = new TextField();
		shareField.defaultTextFormat = new TextFormat('_sans',11);
		shareField.x = 22;
		shareField.y = 32;
		shareField.text = config['link'];
		shareField.width = 258;
		shareField.height = 20;
		share.addChild(shareField);
		addChild(share);
	};



	/** Embed icon is clicked; toggle the embed screen. **/
	private function embedHandler(evt:MouseEvent):void {
		showHide('embed');
	};


	/** The initialize call is invoked by the player View. **/
	public function initializePlugin(vie:AbstractView):void {
		view = vie;
		view.addControllerListener(ControllerEvent.RESIZE,resizeHandler);
		if(view.config['sharing.code']) {
			config['code'] = view.config['sharing.code'];
			drawEmbed();
		}
		if(view.config['sharing.link']) {
			if(view.config['sharing.link'] != 'none') {
				config['link'] = view.config['sharing.link'];
				drawShare();
			}
		} else {
			setTimeout(pageLink,1000);
			drawShare();
			view.addControllerListener(ControllerEvent.ITEM,itemHandler);
			view.addControllerListener(ControllerEvent.PLAYLIST,itemHandler);
		}
		showHide('none');
	};


	/** Share area is clicked; select the link and copy to clipboard or follow a shortcut. **/
	private function linkHandler(evt:MouseEvent):void {
		if(shareScreen.mouseY < 80) { 
			if(shareScreen.mouseX < 80) {
				navigateToURL(new URLRequest(config['link']),view.config['linktarget']);
			} else {
				stage.focus = shareField;
				shareField.setSelection(0,999999);
				System.setClipboard(config['link']);
			}
		} else {
			var url:String;
			if (shareScreen.mouseX < 75) {
				url = 'http://www.facebook.com/share.php?u='+encodeURI(config['link']);
				navigateToURL(new URLRequest(url));
			} else if (shareScreen.mouseX < 150) {
				url = 'http://twitter.com/home?status='+config['title']+': '+encodeURI(config['link']);
				navigateToURL(new URLRequest(url));
			} else if (shareScreen.mouseX < 225) {
				if(config['code']) {
					url = 'http://www.myspace.com/Modules/PostTo/Pages/?t='+config['title']+'&c='+encodeURI(config['code']);
				} else {
					url = 'http://www.myspace.com/Modules/PostTo/Pages/?t='+config['title']+'&c='+encodeURI(config['link']);
				}
				navigateToURL(new URLRequest(url));
			} else {
				url = 'mailto:?subject='+config['title']+'&body='+encodeURI(config['link']);
				navigateToURL(new URLRequest(url),'_self');
			}
		}
	};


	/** Save link from new item. **/
	private function itemHandler(evt:ControllerEvent):void {
		config['title'] = encodeURI(view.playlist[view.config['item']]['title']);
		if (view.playlist[view.config['item']]['link']) {
			config['link'] = view.playlist[view.config['item']]['link'];
			shareField.text = config['link'];
		}
	};


	/** Grab the page link. **/
	private function pageLink():void {
		if(ExternalInterface.available && !config['link']) {
			try {
				config['link'] = ExternalInterface.call("function(){return window.location.href;}");
				shareField.text = config['link'];
			} catch (err:Error) {}
		}
	};


	/** Resize the clip when on screen. **/
	private function resizeHandler(evt:ControllerEvent):void {
		back.width = view.config['width'];
		back.height = view.config['height'];
		embed.x = share.x = view.config['width']/2-150
		embed.y = share.y = view.config['height']/2-65
	};

	/** Share icon is clicked; toggle the share screen. **/
	private function shareHandler(evt:MouseEvent):void {
		showHide('share');
	};


	/** Show or hide the display icons. **/
	private function showHide(stt:String):void {
		if(stt == showing && stt != 'none') {
			stt = 'none';
		}
		switch (stt) {
			case 'none':
				back.visible = false;
				embed.visible = false;
				share.visible = false;
				break;
			case 'embed':
				back.visible = true;
				embed.visible = true;
				share.visible = false;
				break;
			case 'share':
				back.visible = true;
				embed.visible = false;
				share.visible = true;
				break;
		}
		showing = stt;
	};


};


}