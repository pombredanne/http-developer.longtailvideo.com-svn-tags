# This is a simple script that compiles the plugin using the free Flex SDK on Windows.
# Learn more at http://developer.longtailvideo.com/trac/wiki/PluginsCompiling

FLEXPATH="C:/Program Files (x86)/Adobe/Adobe Flash Builder Beta 2/sdks/3.4.1"


echo "Compiling sharing plugin..."
$FLEXPATH/bin/mxmlc ./plugins/Sharing/Sharing.as -sp ./ -o ./Sharing.swf -library-path+=./lib -load-externs sdk-classes.xml -use-network=false -debug=true