package
{
	
	import com.longtailvideo.jwplayer.player.IPlayer;
	import com.longtailvideo.jwplayer.plugins.IPlugin;
	import com.longtailvideo.jwplayer.plugins.PluginConfig;
	
	import flash.display.DisplayObject;
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	import flash.external.ExternalInterface;
	import flash.net.URLRequest;
	import flash.net.navigateToURL;
	import flash.system.System;
	import flash.text.TextField;
	import flash.text.TextFormat;
	
	public class sharing extends Sprite implements IPlugin
	{
		[Embed(source="embedButton.png")]
		private const EmbedButton:Class;
		[Embed(source="embedIcon.png")]
		private const EmbedIcon:Class;
		[Embed(source="embedScreen.png")]
		private const EmbedScreen:Class;
		[Embed(source="shareButton.png")]
		private const ShareButton:Class;
		[Embed(source="shareIcon.png")]
		private const ShareIcon:Class;
		[Embed(source="shareScreen.png")]
		private const ShareScreen:Class;
		
		/** Reference to embed icon **/
		private var skinEmbed:DisplayObject;
		/** Reference so share icon **/
		private var skinShare:DisplayObject;
		
		/** Reference to the Player API **/
		private var api:IPlayer;
		
		/** Reference to the sharing.code flashvar **/
		private var code:String;
		/** Reference to the sharing.link flashvar **/
		private var link:String;
		/** Reference to video title **/
		private var title:String;

		/** Reference to the embed container. **/
		private var embed:Sprite;
		/** Reference to the embed graphics. **/
		private var embedScreen:Sprite;
		/** Reference to the embed textfield. **/
		private var embedField:TextField;
		/** Reference to the share graphics. **/
		private var share:Sprite;
		/** Reference to the share graphics. **/
		private var shareScreen:Sprite;
		/** Reference to the share textfield. **/
		private var shareField:TextField;
		/** Reference to the transparent bg area. **/
		private var back:Sprite;
		/** Remenber which item is visible. **/
		private var showing:String;
		
		/** Let the player know what the name of your plugin is. **/
		public function get id():String { return "sharing"; }
		
		/** Constructor; builds the background area. **/
		public function sharing():void
		{
			back = new Sprite();
			embed = new Sprite();
			share = new Sprite();
			back.graphics.beginFill(0x000000,0);
			back.graphics.drawRect(0,0,320,240);
			back.graphics.endFill();
			back.buttonMode = true;
			back.addEventListener(MouseEvent.CLICK, backHandler);
			addChild(back);
		}
		
		/**
		 * Called by the player after the plugin has been created.
		 *  
		 * @param player A reference to the player's API
		 * @param config The plugin's configuration parameters.
		 */
		public function initPlugin(player:IPlayer, config:PluginConfig):void {
			api = player;
			skinEmbed = api.skin.getSkinElement("sharing", "embedIcon");
			if (skinEmbed == null) {
				skinEmbed = new EmbedIcon();
			}
			skinShare = api.skin.getSkinElement("sharing", "shareIcon");
			if (skinShare == null) {
				skinShare = new ShareIcon();
			}
			if(config['code']) {
				code = config['code'];
				drawEmbed();
			}
			if(config['link']) {
				if(config['link'] != 'none') {
					link = config['link'];
					drawShare();
				}
			} else if (ExternalInterface.available) {
				link = ExternalInterface.call("function(){return window.location.href;}");
				drawShare();
			}
			if(api.playlist.currentItem.title) {
				title = encodeURI(api.playlist.currentItem.title);
			}
			showHide('none');
		}
		
		/**
		 * Called When the player resizes itself, it sets the x/y coordinates of all components and plugins.  
		 *  
		 * @param width Width of the plugin's layout area, in pixels 
		 * @param height Height of the plugin's layout area, in pixels
		 */	
		public function resize(wid:Number, hei:Number):void {
			back.width = wid;
			back.height = hei;
			embed.x = share.x = wid/2-150;
			embed.y = share.y = hei/2-65;
		}
		
		/** Screenground clicked; hide the screen. **/
		private function backHandler(evt:MouseEvent=null):void {
			showHide('none');
		}
		
		/** Embed area is clicked; select the code and copy to clipboard.. **/
		private function codeHandler(evt:MouseEvent):void {
			stage.focus = embedField;
			embedField.setSelection(0,999999);
			System.setClipboard(code);
		}
		
		/** Set the embed buttons and graphics. **/
		private function drawEmbed():void {
			if(api.config.pluginConfig('dock') && api.config.dock) {
				api.controls.dock.addButton(skinEmbed, "embed", embedHandler);
			} else if (api.config.pluginConfig('controlbar')) {
				api.controls.controlbar.addButton(new EmbedButton(), "embed", embedHandler);
			}
			embedScreen = new Sprite();
			embedScreen.addChild(new EmbedScreen());
			embedScreen.buttonMode = true;
			embedScreen.addEventListener(MouseEvent.CLICK,codeHandler);
			embed.addChild(embedScreen);
			embedField = new TextField();
			embedField.defaultTextFormat = new TextFormat('_sans',11);
			embedField.multiline = true;
			embedField.wordWrap = true;
			embedField.x = 22;
			embedField.y = 32;
			embedField.text = code;
			embedField.width = 258;
			embedField.height = 64;
			embed.addChild(embedField);
			addChild(embed);
		}
		
		/** Set the embed buttons and graphics. **/
		private function drawShare():void {
			if(api.config.pluginConfig('dock') && api.config.dock) {
				api.controls.dock.addButton(skinShare, "share", shareHandler);
			} else if (api.config.pluginConfig('controlbar')) {
				api.controls.controlbar.addButton(new ShareButton(), "share", shareHandler);
			}
			shareScreen = new Sprite();
			shareScreen.addChild(new ShareScreen());
			shareScreen.buttonMode = true;
			shareScreen.addEventListener(MouseEvent.CLICK,linkHandler);
			share.addChild(shareScreen);
			shareField = new TextField();
			shareField.defaultTextFormat = new TextFormat('_sans',11);
			shareField.x = 22;
			shareField.y = 32;
			shareField.text = link;
			shareField.width = 258;
			shareField.height = 20;
			share.addChild(shareField);
			addChild(share);
		}		
		
		/** Embed icon is clicked; toggle the embed screen. **/
		private function embedHandler(evt:MouseEvent):void {
			showHide('embed');
		}
		
		/** Share area is clicked; select the link and copy to clipboard or follow a shortcut. **/
		private function linkHandler(evt:MouseEvent):void {
			if(shareScreen.mouseY < 80) { 
				stage.focus = shareField;
				shareField.setSelection(0,999999);
				System.setClipboard(link);
			} else {
				var url:String;
				if (shareScreen.mouseX < 75) {
					url = 'http://www.facebook.com/share.php?u='+encodeURI(link);
					navigateToURL(new URLRequest(url));
				} else if (shareScreen.mouseX < 150) {
					url = 'http://twitter.com/home?status='+title+': '+encodeURI(link);
					navigateToURL(new URLRequest(url));
				} else if (shareScreen.mouseX < 225) {
					if(code) {
						url = 'http://www.myspace.com/Modules/PostTo/Pages/?t='+title+'&c='+encodeURI(code);
					} else {
						url = 'http://www.myspace.com/Modules/PostTo/Pages/?t='+title+'&c='+encodeURI(link);
					}
					navigateToURL(new URLRequest(url));
				} else {
					url = 'mailto:?subject='+title+'&body='+encodeURI(link);
					navigateToURL(new URLRequest(url),'_self');
				}
			}
		}
		
		/** Share icon is clicked; toggle the share screen. **/
		private function shareHandler(evt:MouseEvent):void {
			showHide('share');
		}
		
		/** Show or hide the display icons. **/
		private function showHide(stt:String):void {
			if(stt == showing && stt != 'none') {
				stt = 'none';
			}
			switch (stt) {
				case 'none':
					back.visible = false;
					embed.visible = false;
					share.visible = false;
					break;
				case 'embed':
					back.visible = true;
					embed.visible = true;
					share.visible = false;
					break;
				case 'share':
					back.visible = true;
					embed.visible = false;
					share.visible = true;
					break;
			}
			showing = stt;
		}
	}
	
}