(function(jwplayer) {


/**
* Displays a video embed screen and video share screen (with Facebook/Twitter shortcuts).
**/
var template = function(_player, _options, _div) {


    /** Reference to the container with elements. **/
    var _container;
    /** Reference to the link title. **/
    var _label1;
    /** Reference to the link field. **/
    var _field1;
    /** Reference to the code title. **/
    var _label2;
    /** Reference to the code area. **/
    var _field2;
    /** Reference to the facebook link. **/
    var _facebook;
    /** Reference to the twitter link. **/
    var _twitter;
    /** styling for titles. **/
    var _labelStyle = {
        color: '#FFF',
        display: 'inline-block',
        font: '13px/24px Arial,sans-serif',
        fontWeight: 'bold',
        width: '82px',
        textAlign: 'right',
        textShadow: '#000 1px 1px 0',
        paddingRight: '8px'
    };
    /** Styling for fields **/
    var _fieldStyle = {
        display: 'inline-block',
        borderRadius: '4px',
        fontFamily: 'Arial,sans-serif',
        fontSize: '11px',
        border: '1px solid #000',
        padding: '0 6px',
        background: '#FFF',
        width: '220px',
        height: '26px',
        margin: '0 0 14px 0'
    };
    /** Styling for buttons. **/
    var _buttonStyle = {
        textIndent: '-9999px',
        margin: '0 0 0 90px',
        display: 'inline-block',
        width: '100px',
        height: '30px'
    };
    /** Facebook sharing URL **/
    var _facebookURL = 'http://www.facebook.com/sharer/sharer.php?u=';
    /** Twitter sharing URL **/
    var _twitterURL = 'http://twitter.com/intent/tweet?url=';


    /** Display the embed menu. **/
    var show = function() {
        _div.style.visibility = 'visible';
        try { _player.getPlugin("display").hide(); } catch (e) { /* Only 5.7+ */ }
    };

    function _hide(event) { 
        if(event.target == _div) {
            _div.style.visibility = 'hidden';
            try { _player.getPlugin("display").show(); } catch (e) { /* Only 5.7+ */ }
        }
    };


    /** Playlist item selection **/
    function _item(event) {
        // Set the embed code.
        var item = _player.getPlaylist()[event.index];
        var code = undefined;
        if(item['sharing.code']) {
            code = item['sharing.code'];
        } else if(_options.code) {
            code = _options.code;
        }
        if(code) {
            _style(_label1,{display:'inline-block'});
            _style(_field1,{display:'inline-block'});
            if(code.substr(0,3) == '%3C') { code = decodeURIComponent(code); }
            _field1.value = code;
        } else {
            _style(_label1,{display:'none'});
            _style(_field1,{display:'none'});
            _field1.value = '';
        }
        // Set the video link.
        var link = window.location;
        if(window.top != window) {
            link = document.referrer;
        }
        if(item['sharing.link']) {
            link = item['sharing.link'];
        } else if(_options.link) {
            link = _options.link;
        }
        _field2.value = link;
        _facebook.setAttribute('href',_facebookURL + encodeURIComponent(link));
        _facebook.setAttribute('target','_top');
        _twitter.setAttribute('href',_twitterURL + encodeURIComponent(link));
        _facebook.setAttribute('target','_top');
    };


    /** Set dock buttons when player is ready. **/
    function _setup() {
        if(_player.getRenderingMode() == 'flash') { return; }
        _player.getPlugin("dock").setButton('share',show,'../assets/share.png');
        _div.onclick = _hide;
        _container = document.createElement("div");
        _div.style.visibility = 'hidden';
        _div.appendChild(_container);
        _label1 = document.createElement("label");
        _label1.innerHTML = "Embed code";
        _container.appendChild(_label1);
        _style(_label1,_labelStyle);
        _field1 = document.createElement("input");
        _field1.setAttribute("type","text");
        _field1.setAttribute("readonly","readonly");
        _container.appendChild(_field1);
        _style(_field1,_fieldStyle);
        _label2 = document.createElement("label");
        _label2.innerHTML = "Video link";
        _container.appendChild(_label2);
        _style(_label2,_labelStyle);
        _field2 = document.createElement("input");
        _field2.setAttribute("type","text");
        _field2.setAttribute("readonly","readonly");
        _container.appendChild(_field2);
        _style(_field2,_fieldStyle);
        _facebook = document.createElement("a");
        _facebook.innerHTML = 'Post to Facebook';
        _container.appendChild(_facebook);
        _style(_facebook,_buttonStyle);
        _style(_facebook,{background:'transparent url(../assets/facebook.png)'});
        _twitter = document.createElement("a");
        _twitter.innerHTML = 'Post to Twitter';
        _container.appendChild(_twitter);
        _style(_twitter,_buttonStyle);
        _style(_twitter,{background:'transparent url(../assets/twitter.png)', marginLeft: '5px'});
        _player.onPlaylistItem(_item);
    };
    _player.onReady(_setup);


    /** Reposition elements upon a resize. **/
    this.resize = function(width,height) {
        if(_player.getRenderingMode() == 'flash') { return; }
        _style(_div,{
            backgroundImage: 'url(../assets/sheet.png)',
            backgroundRepeat: 'no-repeat',
            backgroundSize: '100% 100%',
            cursor: 'pointer',
            height: height,
            width: width
        });
        _style(_container,{
            cursor: 'default',
            margin: 'auto',
            marginTop: Math.round(height/2 - 48)+'px',
            width: '320px'
        });
    };


    /** Apply CSS styles to elements. **/
    function _style(element,styles) {
        for(var property in styles) {
          element.style[property] = styles[property];
        }
     };


};


/** Register the plugin with JW Player. **/
jwplayer().registerPlugin('sharing', template,'./sharing.swf');


})(jwplayer);