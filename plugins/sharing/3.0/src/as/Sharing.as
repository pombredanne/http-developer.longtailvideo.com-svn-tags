package {

    import com.longtailvideo.jwplayer.events.*;
    import com.longtailvideo.jwplayer.player.*;
    import com.longtailvideo.jwplayer.plugins.*;

    import flash.display.DisplayObject;
    import flash.display.Sprite;
    import flash.events.MouseEvent;
    import flash.external.ExternalInterface;
    import flash.filters.DropShadowFilter;
    import flash.net.URLRequest;
    import flash.net.navigateToURL;
    import flash.system.System;
    import flash.text.TextField;
    import flash.text.TextFormat;


    /** This plugin displays an embed code and video link for viral sharing. **/
    public class Sharing extends Sprite implements IPlugin {


        /** Embedding the image assets. **/
        [Embed(source="../../assets/controlbar.png")]
        private const ControlbarIcon:Class;
        [Embed(source="../../assets/copy.png")]
        private const CopyButton:Class;
        [Embed(source="../../assets/facebook.png")]
        private const FacebookButton:Class;
        [Embed(source="../../assets/icon.png")]
        private const DockIcon:Class;
        [Embed(source="../../assets/input.png")]
        private const InputBackground:Class;
        [Embed(source="../../assets/sheet.png")]
        private const BackSheet:Class;
        [Embed(source="../../assets/twitter.png")]
        private const TwitterButton:Class;


        /** URL for Facebook and Twitter sharing dialogues. **/
        public const FACEBOOK_URL:String = 'http://www.facebook.com/sharer/sharer.php?u=';
        public const TWITTER_URL:String = 'http://twitter.com/intent/tweet?url=';


        /** Reference to the background sheet. **/
        private var _back:Sprite;
        /** The current embed code. **/
        private var _code:String;
        /** Reference to the code field background. **/
        private var _codeBack:DisplayObject;
        /** Reference to the code copy button. **/
        private var _codeButton:Sprite;
        /** Reference to the code text field. **/
        private var _codeField:TextField;
        /** The form with all code elements. **/
        private var _codeForm:Sprite;
        /** Reference to the code label. **/
        private var _codeLabel:TextField;
        /** The plugin configuration options.**/
        private var _config:Object;
        /** The form with all buttons. **/
        private var _form:Sprite;
        /** The current video link. **/
        private var _link:String;
        /** Reference to the link field background. **/
        private var _linkBack:DisplayObject;
        /** Reference to the link copy button. **/
        private var _linkButton:Sprite;
        /** Reference to the link text field. **/
        private var _linkField:TextField;
        /** Reference to the link form. **/
        private var _linkForm:Sprite;
        /** Reference to the link label. **/
        private var _linkLabel:TextField;
        /** Reference to the player. **/
        private var _player:IPlayer;
        /** Reference to the facebook button. **/
        private var _facebookButton:Sprite;
        /** Reference to the twitter button. **/
        private var _twitterButton:Sprite;


        /** The background screen was clicked. **/
        private function _backHandler(evt:MouseEvent):void {
            _back.visible = false;
            _form.visible = false;
            // Only 5.7+...
            try { (_player.controls.display as Object).show(); } catch (error:Error) {}
        };


        /** The controlbar/dock button was clicked. **/
        private function _buttonHandler(evt:MouseEvent):void {
            if(_back.visible) {
                _backHandler(evt);
            } else {
                _back.visible = true;
                _form.visible = true;
                // Only 5.7+...
                try { (_player.controls.display as Object).hide(); } catch (error:Error) {}
            }
        };


        /** The code copy button is clicked. **/
        private function _codeHandler(evt:MouseEvent):void {
            stage.focus = _codeField;
            _codeField.setSelection(0,999999);
            System.setClipboard(_code);
        };


        /** The facebook button is clicked. **/
        private function _facebookHandler(evt:MouseEvent):void {
            navigateToURL(new URLRequest(FACEBOOK_URL+encodeURIComponent(_link)));
        };


        /** Grab the page URL with some javascript magic. **/
        private function _getPageURL():String {
            var url:String = '';
            if(ExternalInterface.available) {
                try {
                    url =  ExternalInterface.call('function(){if(window.top==window)return window.location.toString();else return document.referrer;}');
                } catch (err:Error) {}
            }
            return url;
        };


        /** Returns the plugin name. **/
        public function get id():String {
            return "sharing";
        };


        /** Called by the player to initialize; setup events and dock buttons.  */
        public function initPlugin(player:IPlayer, config:PluginConfig):void {
            _player = player;
            _config = config;
            _player.addEventListener(PlaylistEvent.JWPLAYER_PLAYLIST_ITEM, _itemHandler);
            if(_player.config.dock === false) { 
                _player.controls.controlbar.addButton(new ControlbarIcon(), "share", _buttonHandler);
            } else {
                _player.controls.dock.addButton(new DockIcon(), "share", _buttonHandler);
            }
            // Add the background and form.
            _back = new Sprite();
            _back.visible = false;
            _back.buttonMode = true;
            _back.addChild(new BackSheet());
            _back.addEventListener(MouseEvent.CLICK,_backHandler);
            addChild(_back);
            _form = new Sprite();
            _form.visible = false;
            addChild(_form);

            // Add the embed code fields
            _codeForm = new Sprite();
            _form.addChild(_codeForm);
            _codeLabel = new TextField();
            _codeLabel.defaultTextFormat = new TextFormat('Arial', 13, 0xFFFFFF, true);
            _codeLabel.text = "Embed code";
            _codeLabel.x = -5;
            _codeLabel.y = 3;
            _codeLabel.filters = new Array(new DropShadowFilter(1,45,0,1,0,0,1));
            _codeForm.addChild(_codeLabel);
            _codeBack = new InputBackground();
            _codeForm.addChild(_codeBack);
            _codeBack.x = 80;
            _codeField = new TextField();
            _codeField.defaultTextFormat = new TextFormat('Arial',11,0x000000);
            _codeForm.addChild(_codeField);
            _codeField.x = 84;
            _codeField.y = 5;
            _codeField.width = 178;
            _codeField.height = 20;
            _codeButton = new Sprite();
            _codeForm.addChild(_codeButton);
            _codeButton.buttonMode = true;
            _codeButton.addChild(new CopyButton());
            _codeButton.addEventListener(MouseEvent.CLICK,_codeHandler);
            _codeButton.x = 260;

            // Add the video link fields
            _linkForm = new Sprite();
            _form.addChild(_linkForm);
            _linkForm.y = 40;
            _linkLabel = new TextField();
            _linkLabel.defaultTextFormat = new TextFormat('Arial', 13, 0xFFFFFF, true);
            _linkLabel.text = "Video link";
            _linkLabel.x = 10;
            _linkLabel.y = 3;
            _linkLabel.filters = new Array(new DropShadowFilter(1,45,0,1,0,0,1));
            _linkForm.addChild(_linkLabel);
            _linkBack = new InputBackground();
            _linkForm.addChild(_linkBack);
            _linkBack.x = 80;
            _linkField = new TextField();
            _linkField.defaultTextFormat = new TextFormat('Arial',11,0x000000);
            _linkForm.addChild(_linkField);
            _linkField.x = 84;
            _linkField.y = 5;
            _linkField.width = 178;
            _linkField.height = 20;
            _linkButton = new Sprite();
            _linkForm.addChild(_linkButton);
            _linkButton.buttonMode = true;
            _linkButton.addChild(new CopyButton());
            _linkButton.addEventListener(MouseEvent.CLICK,_linkHandler);
            _linkButton.x = 260;

            // Add facebook and twitter buttons.
            _facebookButton = new Sprite();
            _facebookButton.addChild(new FacebookButton());
            _facebookButton.buttonMode = true;
            _facebookButton.addEventListener(MouseEvent.CLICK,_facebookHandler);
            _facebookButton.x = 80;
            _facebookButton.y = 40;
            _linkForm.addChild(_facebookButton);
            _twitterButton = new Sprite();
            _twitterButton.addChild(new TwitterButton());
            _twitterButton.buttonMode = true;
            _twitterButton.addEventListener(MouseEvent.CLICK,_twitterHandler);
            _twitterButton.x = 190;
            _twitterButton.y = 40;
            _linkForm.addChild(_twitterButton);
        };


        /** Change the embed code when an item changes.  */
        public function _itemHandler(evt:PlaylistEvent):void {
            // Set the embed code.
            var item:Object = _player.playlist.currentItem;
            _code = '';
            if(item['sharing.code']) {
                _code = item['sharing.code'];
            } else if(_config.code) {
                _code = _config.code;
            }
            if(_code.substr(0,3) == '%3C') { 
                _code = decodeURIComponent(_code); 
            }
            _codeField.text = _code;
            if(_code == '') {
                _codeForm.visible = false;
                _linkForm.y = 20;
            } else {
                _codeForm.visible = true;
                _linkForm.y = 40;
            }
            // Set the video link.
            _link = _getPageURL();
            if(item['sharing.link']) {
                _link = item['sharing.link'];
            } else if(_config.link) {
                _link = _config.link;
            }
            _linkField.text = _link;
        };


        /** The link copy button is clicked. **/
        private function _linkHandler(evt:MouseEvent):void {
            stage.focus = _linkField;
            _linkField.setSelection(0,999999);
            System.setClipboard(_link);
        };


        /** Reposition the screens when the player resizes itself **/
        public function resize(wid:Number, hei:Number):void {
            _back.width = wid;
            _back.height = hei;
            _form.x = Math.round(wid/2 - 160);
            _form.y = Math.round(hei/2 - 54);
        };


        /** The twitter button is clicked. **/
        private function _twitterHandler(evt:MouseEvent):void {
            navigateToURL(new URLRequest(TWITTER_URL+encodeURIComponent(_link)));
        };


    }
}
