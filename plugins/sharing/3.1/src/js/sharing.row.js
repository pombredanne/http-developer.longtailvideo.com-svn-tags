(function(jwplayer) {


    /** Renders a single sharing button. **/
    jwplayer.sharing.row = function(_name) {


        /** Overall div. **/
        var _div;
        /** styling for the div. **/
        var _divStyle = {
            border: 'none',
            position: 'absolute',
            left: '0px',
            width: '312px',
            height: '30px',
            margin: '0 0 0 0',
            padding: '0 0 0 0'
        };
        /** Textfield element. **/
        var _field;
        /** Styling for fields **/
        var _fieldStyle = {
            position: 'absolute',
            left: '80px',
            top: '0px',
            font: '11px/24px Arial,sans-serif',
            border: '1px solid #000',
            webkitBoxShadow: '0 0 4px #000',
            MozBoxShadow: '0 0 4px #000',
            msBoxShadow: '0 0 4px #000',
            oBoxShadow: '0 0 4px #000',
            boxShadow: '0 0 4px #000',
            padding: '0px 5px',
            background: '#FFF',
            width: '222px',
            height: '24px',
            margin: '0 0 0 0'
        };
        /** Label element **/
        var _label;
        /** styling for the labels. **/
        var _labelStyle = {
            border: 'none',
            color: '#FFF',
            display: 'block',
            font: '12px/25px Arial,sans-serif',
            fontWeight: 'bold',
            overflow: 'hidden',
            width: '74px',
            margin: '0 0 0 0',
            textAlign: 'right',
            textTransform: 'none',
            textShadow: '#000 1px 1px 0',
            padding: '0 0 0 0'
        };


         /** Return the main div for positioning. **/
         this.getDiv = function() { return _div; };


         /** Set the contents of the field (on each item change). **/
         this.setText = function(text) { _field.setAttribute('value', text);};


        /** Create all elements. **/
        function _setup() {
            _div = document.createElement("div");
            _style(_div,_divStyle);
            _label = document.createElement("label");
            _label.innerHTML = _name;
            _div.appendChild(_label);
            _style(_label,_labelStyle);
            _field = document.createElement("input");
            _field.setAttribute('type','text');
            // Only set readonly on non-touch devices.
            try {
                document.createEvent("TouchEvent");
            } catch (e) {
                _field.setAttribute('readonly','readonly');
            }
            _style(_field,_fieldStyle);
            _div.appendChild(_field);
        };


        /** Apply CSS styles to elements. **/
        function _style(element,styles) {
            for(var property in styles) {
              element.style[property] = styles[property];
            }
         };


        /** Setup the shortcut and return instance. **/
        _setup();


    };


})(jwplayer);