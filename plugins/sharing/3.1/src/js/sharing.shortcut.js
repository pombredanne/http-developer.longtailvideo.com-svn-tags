(function(jwplayer) {


    /**
    * Renders a sharing shortcut in the dialog.
    **/
    jwplayer.sharing.shortcut = function(_thumb,_name,_handler) {


        /** Reference to main element. **/
        var _div;
        /** Styling for the texts. **/
        var _divStyle = {
            cursor: 'pointer',
            position: 'absolute',
            border: 'none',
            color: '#FFF',
            display: 'block',
            font: '11px/18px Arial,sans-serif',
            margin: '0 0 0 0',
            textAlign: 'left',
            textDecoration: 'none',
            textShadow: '#000 1px 1px 0',
            textTransform: 'none',
            padding: '0 0 0 36px',
            width: '76px',
            height: '30px'
        };
        /** Reference to the icon. **/
        var _icon;
        /** Style settings for the glow div. **/
        var _iconStyle = {
            border: '1px solid #000',
            webkitBoxShadow: '0 0 4px #000',
            MozBoxShadow: '0 0 4px #000',
            msBoxShadow: '0 0 4px #000',
            oBoxShadow: '0 0 4px #000',
            boxShadow: '0 0 4px #000',
            position: 'absolute',
            left: '0px',
            top: '0px',
            width: '30px',
            height: '30px',
            margin: '0 0 0 0',
            padding: '0 0 0 0'
        };
        /** Title of the shortcut. **/
        var _title;
        /** Styling for the title. **/
        var _titleStyle = {
            position: 'absolute',
            border: 'none',
            color: '#FFF',
            display: 'block',
            font: '12px/15px Arial,sans-serif',
            fontWeight: 'bold',
            height: '15px',
            left: '36px',
            margin: '0 0 0 0',
            textAlign: 'left',
            textDecoration: 'none',
            textShadow: '#000 1px 1px 0',
            textTransform: 'none',
            top: '15px',
            padding: '0 0 0 0',
            width: '76px'
        };
        


        /** Set white shadow when rolling over shortcut. **/
        function _over(event) {
            _style(_icon,{
                border: '1px solid #FFF',
                webkitBoxShadow: '0 0 4px #FFF',
                MozBoxShadow: '0 0 4px #FFF',
                msBoxShadow: '0 0 4px #FFF',
                oBoxShadow: '0 0 4px #FFF',
                boxShadow: '0 0 4px #FFF'
            });
        };


        /** Set black shadow when rolling out shortcut. **/
        function _out(event) {
            _style(_icon,{
                border: '1px solid #000',
                webkitBoxShadow: '0 0 4px #000',
                MozBoxShadow: '0 0 4px #000',
                msBoxShadow: '0 0 4px #000',
                oBoxShadow: '0 0 4px #000',
                boxShadow: '0 0 4px #000'
            });
        };


        /** Create all elements. **/
        function _setup() {
            _div = document.createElement("div");
            _div.innerHTML = "Post to";
            _div.onclick = _handler;
            _div.onmouseout = _out;
            _div.onmouseover = _over;
            _style(_div,_divStyle);
            _icon = document.createElement("img");
            _icon.setAttribute('src',_thumb);
            _style(_icon,_iconStyle);
            _div.appendChild(_icon);
            _title = document.createElement("div");
            _title.innerHTML = _name;
            _style(_title,_titleStyle);
            _div.appendChild(_title);
            return _div;
        };


        /** Apply CSS styles to elements. **/
        function _style(element,styles) {
            for(var property in styles) {
              element.style[property] = styles[property];
            }
         };


        /** Setup the shortcut and return instance. **/
        return _setup();


    };


})(jwplayer);