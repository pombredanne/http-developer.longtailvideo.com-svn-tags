(function(jwplayer) {


/**
* Displays a video sharing screen (with Facebook/Twitter shortcuts).
**/
var template = function(_player, _options, _div) {


    /** The current embed code. **/
    var _code;
    /** The form row displaying the code. **/
    var _codeRow;
    /** The current player dimensions. **/
    var _dimensions;
    /** Reference to the facebook link. **/
    var _facebookButton;
    /** Facebook sharing URL **/
    var _facebookURL = 'http://www.facebook.com/sharer/sharer.php?u=';
    /** Reference to the container with elements. **/
    var _form;
    /** The form heading text. **/
    var _heading;
    /** The current video link. **/
    var _link;
    /** The form row displaying the link. **/
    var _linkRow;
    /** Reference to the twitter link. **/
    var _twitterButton;
    /** Twitter sharing URL **/
    var _twitterURL = 'http://twitter.com/intent/tweet?url=';


    /** The facebook button/icon was clicked. **/
    function _facebook(event) {
        window.top.location.href = _facebookURL + encodeURIComponent(_link);
        return false;
    };


    /** Hiding the dialog when close button is clicked. **/
    function _hide(event) {
        if(event.target == _div) { 
            setTimeout(_hideWrap,200);
            _div.style.opacity = 0;
            try { 
                _player.getPlugin("display").show();
                _player.getPlugin("dock").show();
            } catch (e) { /* Only 5.7+ */ }
        }
    };


    /** Wrapping the dialog hiding animation. **/
    function _hideWrap() {
        _div.style.visibility = 'hidden';
    };


    /** Playlist item selection **/
    function _item(event) {
        var item = _player.getPlaylist()[event.index];
        // Define the embed code.
        _code = undefined;
        if(item['sharing.code']) {
            _code = item['sharing.code'];
        } else if(_options.code) {
            _code = _options.code.replace('MEDIAID',item['mediaid']);
        }
        if(_code && _code.substr(0,3) == '%3C') {
            _code = decodeURIComponent(_code);
        }
        // Define the video link.
        _link = window.location;
        if(window.top != window) {
            _link = document.referrer;
        }
        if(item['sharing.link']) {
            _link = item['sharing.link'];
        } else if(item.link) {
            _link = item.link;
        } else if(_options.link) {
            _link = _options.link.replace('MEDIAID',item['mediaid']);
        }
        // Update the interface if needed.
        if(_form) {
            _codeRow.setText(_code);
            _linkRow.setText(_link);
            if(_code) {
                _style(_codeRow.getDiv(),{left:'0px',top:'30px',display:'block'});
                _style(_linkRow.getDiv(),{left:'0px',top:'72px'});
                _style(_facebookButton,{left:'80px',top:'114px'});
                _style(_twitterButton,{left:'200px',top:'114px'});
            } else {
                _style(_codeRow.getDiv(),{display:'none'});
                _style(_linkRow.getDiv(),{left:'0px',top:'30px'});
                _style(_facebookButton,{left:'80px',top:'72px'});
                _style(_twitterButton,{left:'200px',top:'72px'});
            }
            _resize();
        }
    };


    /** Reposition elements upon a resize. **/
    this.resize = function(width,height) {
        if(_player.getRenderingMode() == 'flash' || !_form) { return; }
        _dimensions = [width,height];
        _resize();
    };


    /** Internal resize, so it can be called after an item change too. **/
    function _resize() {
        _style(_div,{
            width: _dimensions[0]+'px',
            height: _dimensions[1]+'px'
        });
        var left = Math.round(_dimensions[0]/2 - 160);
        var top = Math.round(_dimensions[1]/2 - 50);
        if(_code) { top -= 20; }
        _style(_form,{
            left: left+'px',
            top: top+'px'
        });
    };


    /** Set dock buttons when player is ready. **/
    function _setup() {
        if(_player.getRenderingMode() == 'flash') { return; }
        _player.onPlaylistItem(_item);
        // Setup the dock buttons
        if(_options.shortcuts) {
            _player.getPlugin("dock").setButton('share',function(){_facebook();},'../assets/shareButton.png');
            _player.getPlugin("dock").setButton('tweet',function(){_twitter();},'../assets/tweetButton.png');
        } else {
            _player.getPlugin("dock").setButton('share',function(){_show();},'../assets/dialogButton.png');
            _div.style.visibility = 'hidden';
            _div.onclick = _hide;
            _setupForm();
        }
    };
    _player.onReady(_setup);


    /** Render the dialog form if shortcuts are disabled. **/
    function _setupForm() {
        // Style the overall container.
        _style(_div,{
            backgroundImage: 'url(../assets/closeButton.png),url(../assets/dialogBack.png)',
            backgroundPosition: 'right top,center center',
            backgroundRepeat: 'no-repeat,no-repeat',
            backgroundSize: 'auto auto,100% 100%',
            opacity: 0,
            webkitTransition: 'opacity 150ms linear',
            mozTransition: 'opacity 150ms linear',
            msTransition: 'opacity 150ms linear',
            oTransition: 'opacity 150ms linear',
            transition: 'opacity 150ms linear',
            cursor: 'pointer'
        });
        // Draw the form wrapper.
        _form = document.createElement("div");
        _div.appendChild(_form);
        _style(_form,{
            cursor: 'default',
            position: 'absolute',
            border: 'none',
            width: '320px'
        });
        // Draw the heading
        _heading = document.createElement("div");
        if(typeof(_options.heading) == 'string') {
            _heading.innerHTML = _options.heading;
        } else {
            _heading.innerHTML = "Share this video";
        }
        _style(_heading,{
            position: 'absolute',
            border: 'none',
            color: '#FFF',
            display: 'block',
            font: '16px/20px Arial,sans-serif',
            fontWeight: 'bold',
            overflow: 'hidden',
            width: '230px',
            height: '20px',
            left: '80px',
            margin: '0 0 0 0',
            textTransform: 'none',
            textShadow: '#000 1px 1px 0',
            padding: '0 0 0 0'
        });
        _form.appendChild(_heading);
        // Draw the code/link rows
        _codeRow = new jwplayer.sharing.row('Embed code');
        _form.appendChild(_codeRow.getDiv());
        _linkRow = new jwplayer.sharing.row('Video link');
        _form.appendChild(_linkRow.getDiv());
        // Draw the facebook/twitter buttons
        _facebookButton = jwplayer.sharing.shortcut(
            '../assets/facebookIcon.png',
            'Facebook',
            _facebook
        );
        _form.appendChild(_facebookButton);
        _twitterButton = jwplayer.sharing.shortcut(
            '../assets/twitterIcon.png',
            'Twitter',
            _twitter
        );
        _form.appendChild(_twitterButton);
    };


    /** Private call for showing the sharing dialog. **/
    function _show() {
        if(_player.getState() == 'PLAYING') {
            _player.pause();
        }
        _div.style.visibility = 'visible';
        _div.style.opacity = 1;
        try { 
            _player.getPlugin("display").hide();
            _player.getPlugin("dock").hide();
        } catch (e) { /* Only 5.7+ */ }
    };


    /** Apply CSS styles to elements. **/
    function _style(element,styles) {
        for(var property in styles) {
          element.style[property] = styles[property];
        }
     };


    /** The twitter button/icon was clicked. **/
    function _twitter(event) {
        window.top.location.href = _twitterURL + encodeURIComponent(_link);
        return false;
    };


};


/** Register the plugin and add namespace for the button/row objects. **/
jwplayer().registerPlugin('sharing', template,'./sharing.swf');
jwplayer.sharing = {};


})(jwplayer);