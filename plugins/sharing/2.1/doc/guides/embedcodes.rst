.. _embedcodes:


Setting Clean Embed Codes 
=========================

This tutorial explains how to create simple, short player embed codes that can be used for re-embedding (viral sharing) of a player.



Introduction
------------

Viral sharing has become one of the most important aspects of video online. The JW Player supports viral sharing options through several plugins, for example:

 * `D-sharing <http://www.longtailvideo.com/addons/plugins/76/D-Sharing>`_ (by Agne Lund)
 * `Embed <http://www.longtailvideo.com/addons/plugins/49/Embed>`_ (by Jaron Barends)
 * `Sharing <http://www.longtailvideo.com/addons/plugins/110/Sharing>`_ (by Jeroen Wijering)
 * `Viral <http://www.longtailvideo.com/addons/plugins/47/Viral>`_ (by Philip Yen)

With all these plugin, the end user can copy the video embed code and then paste it into his own blog or profile. The video will then appear on his blogpost or profile page.

The shorter the actual embed codes are, the easier it is for end-users to copy/paste them. Clean, short embed codes also prevent problems such as:

* Certain services denying the code.
* Breaking of the option to re-share the code.



SWFObject
---------

A typical player embed is done with a javascript library such as `SWF Object <http://www.longtailvideo.com/support/jw-player/jw-player-for-flash-v5/12538/supported-player-embed-methods#SWFobject>`_. The end result is perfect, but the HTML code is fairly lengthy:

.. code-block:: html

   <script type="text/javascript"
     src="http://ajax.googleapis.com/ajax/libs/swfobject/2.2/swfobject.js">
   </script>
   
   <p id="container1">Please install the Flash Plugin</p>
   
   <script type="text/javascript">
      var flashvars = { file:'/data/bbb.mp4',autostart:'true' };
      var params = { allowfullscreen:'true', allowscriptaccess:'always' };
      var attributes = { id:'player1', name:'player1' };
      swfobject.embedSWF('player.swf','container1','480','270','9.0.115','false',
         flashvars, params, attributes);
   </script>

Next to being long, the code fully relies upon javascript. This means that the code cannot be used for viral sharing of your videos through Facebook, Myspace, Blogger and such, because these services all block javascript in their profile or blogpost editors.



Embed
-----

You can workaround this by using the plain old *<embed>* tag for embedding a video. Semantically, it is not the nicest HTML, but it is short and works in all services and on all browsers:

.. code-block:: html

    <embed
      allowfullscreen="true"
      flashvars="file=/data/bbb.mp4&autostart=true"
      height="270"
      src="player.swf"
      width="480"
    />

This code is already sufficient in basic cases, but there are two problems:

* First, since so many `options can be configured <http://www.longtailvideo.com/support/jw-player/jw-player-for-flash-v5/12536/configuration-options>`_, the list of **flashvars** is typically pretty long with a JW Player embed. Ten flashvars or more (some containing long http:// links) is no edge case. At that point, the embed code is large and easy to break.
* Second, since the sharing plugins typically require you to set the embed code as a flashvar too, re-sharing of videos is not possible. Re-sharing means that the players people post on their blogs or profiles in turn also contain *sharing* buttons. If you would want to enable such a feature, you'd have to set the full sharing embed code inside the flashvars again.

In other words, the second problem is recursion issue. True viral sharing means that someone shares a video which in turn is shared again, and then shared again, and so forth. With an *<embed>* with flashvars, you'd have to put the sharing embedcode flashvar inside the embedcode flashvar, inside the embedcode flashvar, and so forth. Impossible...


Rewrites
--------

Luckily there is a solution that fixes both issues. Instead of using the *flashvars* attribute of the embed tag, you could actually load the flashvars into the player by using a serverside rewrite. Here's how this works. 

.. note:: 

   This requires some knowledge of webservers (such as Apache) and serverside scripting (such as PHP). 

Virtual player
^^^^^^^^^^^^^^

First, instead of setting the *src* attribute of the embed tag to the actual JW Player on your server, you set it to a virtual player; a section on your site that contains some serverside code that listens to your players:

.. code-block:: html

    <embed
      allowfullscreen="true"
      height="270"
      src="http://server.com/players/12345.swf"
      width="480"
    />

Rewrite rule
^^^^^^^^^^^^

Second, we could setup the webserver to send all incoming requests to this section (*/players/*) to a PHP file. In Apache for example, this is done with a `rewrite rule <http://www.addedbytes.com/for-beginners/url-rewriting-for-beginners/>`_:

.. code-block:: html

   RewriteEngine  on
   RewriteRule    ^players/(.*)\.swf$    player.php?id=$1


Database lookup
^^^^^^^^^^^^^^^

Last, in the PHP script we could gather the flashvars and create a second redirect, this time to the actual *player.swf*. Let's say we retrieve the flashvars for the player from a simple MySQL table:

.. code-block:: php

   <?php
     # This is the database ID of this particular player setup.
     $id = $_GET['id'];
     
     # Grab the flashvars from a database table
     mysql_connect("localhost","user","pass") or die(mysql_error());
     mysql_select_db("players") or die(mysql_error());
     $result = mysql_query("SELECT * FROM flashvars WHERE id=$id") or die(mysql_error());
     $row = mysql_fetch_array($result,MYSQL_ASSOC);
     
     # Append the flashvar as querystring parameters to the actual player.swf.
     $url = "http://server.com/static/player.swf?";
     foreach($row as $key => $val) { 
       $url .= $key . ':' . urlencode($val) . '&';
     }
     
     # We now have constructed the full URL. Redirecting ...
     header("Location: $url");
     exit;
   ?>

That's all! The embedcode is clean and short, and your webserver takes care of the actual flashvars by adding them to the player URL on load time. 


There's a lot of different ways in which this same goal could be achieved (different scripting languages, webservers, frameworks, caching layers, etc.). The basic principle remains the same: 

* The embed code contains the URL of a virtual player.
* The serverside code returns the URL of the actual player, with the flashvars appended as querystring parameters.

