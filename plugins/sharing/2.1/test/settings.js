var settings = {
	/** Player versions to test. **/
	players: {
		'5.2':'/player/tags/mediaplayer-5.2/player.swf',
		'5.1':'/player/tags/mediaplayer-5.1/player.swf',
		'5.0':'/player/tags/mediaplayer-5.0/player.swf'
	},
	/** Plugins to test. **/
	plugins: {
		sharing:'../sharing.swf'
	},
	/** Skins to test. **/
	skins: {
		'':' ',
		beelden:'/player/skins/beelden/beelden.xml',
		bekle:'/player/skins/bekle/bekle.xml',
		bluemetal:'/player/skins/bluemetal/bluemetal.swf',
		classic:'/player/skins/classic/classic.swf',
		glow:'/player/skins/glow/glow.xml',
		modieus:'/player/skins/modieus/modieus.xml',
		snel:'/player/skins/snel/snel.swf',
        stijl:'/player/skins/stijl/stijl.xml'
	},
	/** All the setup examples with their flashvars. **/
	examples: {
		'': {},
		'Basic setup; both link and code (test copying!)': {
		    controlbar:'over',
			file:'http://content.bitsontherun.com/videos/nPripu9l-327.mp4',
			height:270,
			width:480,
			plugins:'sharing',
			'sharing.link':'http://www.bitsontherun.com/',
			'sharing.code':'%3Cembed%20src%3D%22http%3A%2F%2Fcontent.bitsontherun.com%2Fplayers%2FnPripu9l-ALJ3XQCI.swf%22%20width%3D%22480%22%20height%3D%22270%22%20allowfullscreen%3D%22true%22%20%2F%3E'
		},
		'Playlist setup; not all entries have link and code': {
		    controlbar:'over',
			file:'assets/playlist.xml',
			height:240,
			shuffle:true,
			width:720,
			playlist:'right',
			playlistsize:320,
			plugins:'sharing',
			'sharing.code':true,
			'sharing.link':true,
		},
		' ': {},
		'BitsontheRun embed code hack; using player key.': {
		    controlbar:'over',
			file:'http://content.bitsontherun.com/jwp/LasfrJ72.xml',
			height:270,
			width:680,
			playlist:'right',
			plugins:'sharing',
			'sharing.link': true,
			'sharing.code':'ALJ3XQCI',
		},
	}
}
