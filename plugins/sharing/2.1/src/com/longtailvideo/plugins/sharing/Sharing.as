package com.longtailvideo.plugins.sharing {

    import com.longtailvideo.jwplayer.events.PlaylistEvent;
    import com.longtailvideo.jwplayer.player.IPlayer;
    import com.longtailvideo.jwplayer.plugins.IPlugin;
    import com.longtailvideo.jwplayer.plugins.PluginConfig;
    import com.longtailvideo.jwplayer.view.components.DockButton;
    import com.longtailvideo.jwplayer.utils.Logger;

    import flash.display.DisplayObject;
    import flash.display.LoaderInfo;
    import flash.display.Sprite;
    import flash.events.MouseEvent;
    import flash.external.ExternalInterface;
    import flash.net.URLRequest;
    import flash.net.navigateToURL;
    import flash.system.System;
    import flash.text.TextField;
    import flash.text.TextFormat;
    import flash.utils.setTimeout;


    /**
    * This plugin contains two options for viral sharing of videos: embed code and share link.
    * It places buttons in the dock, works with playlists and can be skinned with the PNG skinning model.
    **/
    public class Sharing extends Sprite implements IPlugin {


        /** Embedding the default assets. **/
        [Embed(source="../../../../../assets/embedButton.png")]
        private const EmbedButton:Class;
        [Embed(source="../../../../../assets/embedIcon.png")]
        private const EmbedIcon:Class;
        [Embed(source="../../../../../assets/embedScreen.png")]
        private const EmbedScreen:Class;
        [Embed(source="../../../../../assets/shareButton.png")]
        private const ShareButton:Class;
        [Embed(source="../../../../../assets/shareIcon.png")]
        private const ShareIcon:Class;
        [Embed(source="../../../../../assets/shareScreen.png")]
        private const ShareScreen:Class;


        /** Reference to the background slate **/
        private var _backScreen:Sprite;
        /** The pluginconfiguration options.**/
        private var _config:Object;
        /** Current state of the plugin (share,embed or none). **/
        private var _currentState:String = 'none';
        /** Reference to the dock embed button. **/
        private var _embedButton:DockButton;
        /** Makeshift embed code. **/
        private var _embedCode:String;
        /** Reference to the embed text field. **/
        private var _embedField:TextField;
        /** Reference to the embed screen **/
        private var _embedScreen:Sprite;
        /** The page URL. **/
        private var _pageURL:String;
        /** Reference to the player. **/
        private var _player:IPlayer;
        /** Reference to the dock share button **/
        private var _shareButton:DockButton;
        /** Reference to the share text field. **/
        private var _shareField:TextField;
        /** Reference to the share screen **/
        private var _shareScreen:Sprite;




        /** Add the embed button and populate the embed screen. **/
        private function addEmbed():void {
            if(_player.config.dock === false) {
                var embedButton:DisplayObject = _player.skin.getSkinElement("sharing", "embedButton");
                if (embedButton == null) { embedButton = new EmbedButton(); }
                _player.controls.controlbar.addButton(embedButton, "embed", embedHandler);
            } else { 
                var embedIcon:DisplayObject = _player.skin.getSkinElement("sharing", "embedIcon");
                if (embedIcon == null) { embedIcon = new EmbedIcon(); }
                _embedButton = _player.controls.dock.addButton(embedIcon, "embed", embedHandler) as DockButton;
            }
            var embedScreen:DisplayObject = _player.skin.getSkinElement("sharing", "embedScreen");
            if (embedScreen == null) { embedScreen = new EmbedScreen(); }
            _embedScreen.addChild(embedScreen);
            _embedScreen.buttonMode = true;
            _embedScreen.addEventListener(MouseEvent.CLICK,codeHandler);
            _embedField = new TextField();
            _embedField.defaultTextFormat = new TextFormat('_sans',11);
            _embedField.multiline = true;
            _embedField.wordWrap = true;
            _embedField.x = 22;
            _embedField.y = 32;
            _embedField.width = 258;
            _embedField.height = 64;
            _embedScreen.addChild(_embedField);
        };


        /** Add the back/embed/share screens to the display stack. **/
        private function addScreens():void {
            _backScreen = new Sprite();
            _backScreen.graphics.beginFill(0x000000,0.25);
            _backScreen.graphics.drawRect(0,0,400,300);
            _backScreen.addEventListener(MouseEvent.CLICK,backHandler);
            addChild(_backScreen);
            _backScreen.visible = false;
            _embedScreen = new Sprite();
            addChild(_embedScreen);
            _embedScreen.visible = false;
            _shareScreen = new Sprite();
            addChild(_shareScreen);
            _shareScreen.visible = false;
        };


        /** Add the share button and populate the share screen. **/
        private function addShare():void {
            if(_player.config.dock === false) {
                var shareButton:DisplayObject = _player.skin.getSkinElement("sharing", "shareButton");
                if (shareButton == null) { shareButton = new ShareButton(); }
                _player.controls.controlbar.addButton(shareButton, "share", shareHandler);
            } else { 
                var shareIcon:DisplayObject = _player.skin.getSkinElement("sharing", "shareIcon");
                if (shareIcon == null) { shareIcon = new ShareIcon(); }
                _shareButton = _player.controls.dock.addButton(shareIcon, "share", shareHandler) as DockButton;
            }
            var shareScreen:DisplayObject = _player.skin.getSkinElement("sharing", "shareScreen");
            if (shareScreen == null) { shareScreen = new ShareScreen(); }
            _shareScreen.addChild(shareScreen);
            _shareScreen.buttonMode = true;
            _shareScreen.addEventListener(MouseEvent.CLICK,linkHandler);
            _shareField = new TextField();
            _shareField.defaultTextFormat = new TextFormat('_sans',11);
            _shareField.x = 22;
            _shareField.y = 32;
            _shareField.width = 258;
            _shareField.height = 20;
            _shareScreen.addChild(_shareField);
        };


        /** The background screen was clicked. **/
        private function backHandler(evt:MouseEvent):void {
            switchState('none');
        };


        /** The embed screen was clicked. **/
        private function codeHandler(evt:MouseEvent):void {
            stage.focus = _embedField;
            _embedField.setSelection(0,999999);
            System.setClipboard(_embedField.text);
        };


        /** Embed button is clicked. **/
        private function embedHandler(evt:MouseEvent):void {
            _currentState == 'embed' ?  switchState('none'): switchState('embed');
        };


        /** hack for backward-compatible BOTR embeds **/
        private function getBotrEmbed():String {
            Logger.log(_player.config.playlistfile,'sharing');
            var code:String = '<embed src="';
            code += _player.config.playlistfile.substr(0,_player.config.playlistfile.indexOf('/',8));
            code += '/players/' + _player.playlist.currentItem['image'].substr(-12,8);
            code += '-' + _config['code'] + '.swf"';
            code += ' width="' + stage.stageWidth + '"';
            code += ' height="' + stage.stageHeight + '"';
            code += ' allowfullscreen="true" />';
            return code;
        };


        /** Grab the page URL with some javascript magic. **/
        private function getEmbedCode():void {
            _embedCode  = '<embed src="' + loaderInfo.loaderURL + '"';
            _embedCode += ' flashvars="file=' + encodeURIComponent(_player.config.file) + '"';
            _embedCode += ' width="' + stage.stageWidth + '"';
            _embedCode += ' height="' + stage.stageHeight + '"';
            _embedCode += ' allowfullscreen="true" />';
        };


        /** Grab the page URL with some jaascript magic. **/
        private function getPageURL():void {
            if(ExternalInterface.available) {
                try {
                    _pageURL = ExternalInterface.call("function(){return window.location.href;}");
                } catch (err:Error) {}
            }
        };


        /** Returns the plugin name. **/
        public function get id():String {
            return "sharing";
        };


        /** Called by the player to initialize; setup events and dock buttons.  */
        public function initPlugin(player:IPlayer, config:PluginConfig):void {
            _player = player;
            _config = config;
            addScreens();
            if(_config['code']) {
                getEmbedCode();
                addEmbed();
                _player.addEventListener(PlaylistEvent.JWPLAYER_PLAYLIST_ITEM,itemEmbedHandler);
            }
            if(_config['link']) {
                getPageURL();
                addShare();
                _player.addEventListener(PlaylistEvent.JWPLAYER_PLAYLIST_ITEM,itemShareHandler);
            }
        };


        /** Change the embed code when an item changes.  */
        public function itemEmbedHandler(evt:PlaylistEvent):void {
            if(_player.playlist.currentItem['sharing.code'] && 
                _player.playlist.currentItem['sharing.code'] != _config['code']) {
                _embedField.text = _player.playlist.currentItem['sharing.code'];
            } else if (_config['code'] == true) {
                _embedField.text = _embedCode;
            } else if (_config['code'].length == 8) {
                _embedField.text = getBotrEmbed();
            } else { 
                _embedField.text = _config['code'];
            }
        };


        /** Change the sharing link when an item changes.  */
        public function itemShareHandler(evt:PlaylistEvent):void {
            if(_player.playlist.currentItem['sharing.link'] && 
                _player.playlist.currentItem['sharing.link'] != _config['link']) {
                _shareField.text = _player.playlist.currentItem['sharing.link'];
            } else if(_player.playlist.currentItem['link'] && 
                _player.playlist.currentItem['link'] != _player.config['link'] && 
                _config['link'] == true) {
                _shareField.text = _player.playlist.currentItem['link'];
            } else if (_config['link'] == true) {
                _shareField.text = _pageURL;
            } else { 
                _shareField.text = _config['link'];
            }
        };


        /** The share screen was clicked. **/
        private function linkHandler(evt:MouseEvent):void {
            if(_shareScreen.mouseY < 80) { 
                if(_shareScreen.mouseX < 80) {
                    navigateToURL(new URLRequest(_shareField.text));
                } else {
                    stage.focus = _shareField;
                    _shareField.setSelection(0,999999);
                    System.setClipboard(_shareField.text);
                }
            } else {
                var url:String;
                var title:String = _player.playlist.currentItem['title'];
                if (_shareScreen.mouseX < 75) {
                    url = 'http://www.facebook.com/share.php?u='+encodeURI(_shareField.text);
                    navigateToURL(new URLRequest(url));
                } else if (_shareScreen.mouseX < 150) {
                    if(title) { 
                        url = 'http://twitter.com/home?status='+ title +': '+encodeURI(_shareField.text);
                    } else {
                        url = 'http://twitter.com/home?status=' + encodeURI(_shareField.text);
                    }
                    navigateToURL(new URLRequest(url));
                } else if (_shareScreen.mouseX < 225) {
                    if(_config['code']) {
                        url = 'http://www.myspace.com/Modules/PostTo/Pages/?c='+encodeURI(_embedField.text);
                    } else {
                        url = 'http://www.myspace.com/Modules/PostTo/Pages/?c='+encodeURI(_shareField.text);
                    }
                    if(title) { url += '&t=' + title; }
                    navigateToURL(new URLRequest(url));
                } else {
                    url = 'mailto:?body='+encodeURI(_shareField.text);
                    if(title) { url += '&subject=' + title; }
                    navigateToURL(new URLRequest(url),'_self');
                }
            }
        };


        /** Reposition the screens when the player resizes itself **/
        public function resize(wid:Number, hei:Number):void {
            _backScreen.width = wid;
            _backScreen.height = hei;
            _embedScreen.x = _shareScreen.x = Math.round(wid/2 - 150);
            _embedScreen.y = _shareScreen.y = Math.round(hei/2 - 70);
        };


        /** Share button is clicked. **/
        private function shareHandler(evt:MouseEvent):void {
            _currentState == 'share' ?  switchState('none'): switchState('share');
        };


        /** Change the display state. **/
        private function switchState(state:String):void {
            switch (state) {
                case "none":
                    _backScreen.visible = false;
                    _embedScreen.visible = false;
                    _shareScreen.visible = false;
                    break;
                case "embed":
                    _backScreen.visible = true;
                    _embedScreen.visible = true;
                    _shareScreen.visible = false;
                    break;
                case "share":
                    _backScreen.visible = true;
                    _embedScreen.visible = false;
                    _shareScreen.visible = true;
                    break;
            }
            _currentState = state;
        };


    }
}