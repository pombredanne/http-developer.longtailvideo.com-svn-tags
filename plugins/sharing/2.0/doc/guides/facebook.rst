Facebook Video Sharing
======================

This tutorial explains how to setup sharing of videoplayers to Facebook. 



Introduction
------------

Facebook provides a simple but effective mechanism for sharing web pages with your friends:

.. code-block:: html

   http://www.facebook.com/share.php?u=http://www.site-to-share.com

Tons of sharing services use this functionality, amongst which the Sharing Plugin of the JW Player. This plugin is available through our `AddOns library <http://www.longtailvideo.com/addons/plugins/110/Sharing>`_ and built into our `Bits on the Run platform <http://www.bitsontherun.com/>`_.

While sharing of a regular web page might be enough for some users, other users might be more interested in actually sharing the videoplayer on that page. This is also possible, but two additional steps are required:

* You need to set certain meta tags in the *<head>* of the page you want to share.
* You need to contact Facebook to get your domain approved for video sharing.



Meta tags
---------

The first step is setting the meta tags. At `this developer wiki page <http://wiki.developers.facebook.com/index.php/Facebook_Share/Specifying_Meta_Tags>`_, Facebook explains in detail which meta tags you need to set in order to share a video. Here is an example block:

.. code-block:: html

   <meta name="title" content="Bits on the Run introduction video" />
   <meta name="description" content="Bits on the Run is a clear and flexible platform 
       for transcoding, managing and streaming your video online." />
   <link rel="image_src" href="http://content.bitsontherun.com/thumbs/yYul4DRz-320.jpg" />
   <link rel="video_src" href="http://content.bitsontherun.com/players/yYul4DRz-tBYU3ED4.swf"/>
   <meta name="video_height" content="245" />
   <meta name="video_width" content="400" />
   <meta name="video_type" content="application/x-shockwave-flash" />

All these tags should be placed between the *<head>* and *</head>* tags of your page's HTML (`example <http://www.bitsontherun.com/demo/facebook/>`_). Here's what they do:

* The *title* and *description* are straightforward: they provide some basic metadata on your video.
* The *image_src* provides a preview image of your video. When your friends click that image, the videoplayer will be loaded on their page.
* The *video_src* provides the link to your video **player** (not the actual video). In case your players are setup with :ref:`clean embed codes <embedcodes>` (e.g. when you use `Bits on the Run <http://www.bitsontherun.com/>`_), this is a single URL. When your player also needs flashvars, you should amend them the the URL of the player as querystring variables, like this:
   
   .. code-block:: html
   
      http://www.mysite/com/player.swf?file=http://www.mysite.comvideo.mp4&image=...
   
* The *video_height* and *video_width* define the dimensions of your video. The maximum width is **420** pixels and the maximum height is **280** pixels. Don't worry about setting other dimensions here than with the player on your own site; the JW Player will automatically resize itself.
* The *video_type* tells Facebook what media type the video player is. This should always be set to *application/x-shockwave-flash*.

Share page
^^^^^^^^^^

Here's an image that shows two instances of the Facebook **share** page. In the first example, the metatags are not included on the page that is shared, in the second example they are. As you can see, the second example is a lot richer in content:

.. image:: ../assets/facebookshare.png
	:alt: Facebook share page without and with metatags.



Getting approved
----------------

The second step is getting your website approved for video embeds with Facebook. That can be done by `filling out this form <http://www.facebook.com/help/contact.php?show_form=video_embed_whitelist>`_. Note that you should specify the domain where your JW Player and videos reside, not the domain where you embed your player at (should they differ).

If you host your videos through `Bits on the Run <http://www.bitsontherun.com/>`_, you're all done at this point, since *content.bitsontherun.com* is already approved by Facebook.

Facebook will get back to you within a few days, at which point your video embeds should be working. 

End result
^^^^^^^^^^

Here's how a video player will look in the news feed of a person who shared it:

.. image:: ../assets/facebookfeed.png
	:alt: Facebook feed with video.

Clicking the thumbnail will replace the description with your actual videoplayer, so people can watch your video inline.