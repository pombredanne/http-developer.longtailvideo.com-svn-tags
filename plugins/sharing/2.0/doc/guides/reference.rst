Sharing Plugin Reference
========================

The Sharing plugin for the JW Player for Flash contains two options for viral sharing of a video:

* The plugin can display a screen to share an *embed code* of the video, similar to Youtube or Vimeo.
* The plugin can display a screen to share a *share link* of the video, e.g. the URL of a page this video is included in. 

The link sharing screen also provides a couple of shortcuts for easy sharing using popular services:

* Facebook
* Twitter
* Myspace
* Email

.. note::

   With Facebook, only the basic link is shared by default. However, it is possible to share the full video player to Facebook by `setting some meta tags <http://wiki.developers.facebook.com/index.php/Facebook_Share/Specifying_Meta_Tags>`_ in the <head> of your page.




Configuration Options
---------------------

The plugin is loaded with the **plugins=sharing-2** option in the JW Player. This ensures you load version 2 of the plugin, which includes the skinning and playlist support. In addition to the plugin assignor, two dedicated flashvars are supported:

.. describe:: sharing.link ( undefined )

   URL to display in the **share** screen of the plugin. If no *link* is set, the **share** screen is not shown. Example:
   
   .. code-block:: html
   
      http://www.mywebsite.com/videos/12345/

.. describe:: sharing.code ( undefined )

   Embed code to display in the **embed** screen of the plugin. If no *code* is set, the **embed** screen is not shown. Example:
   
   .. code-block:: html
   
      <embed src="http://www.mysite.com/players/12345.swf" 
        width="480" height="270" allowfullscreen="true" />
   
   .. note::

      Since the embed code may contain special characters like *<*, *"* or *&*, you should URIEncode the string in the embed code of your player. The JW Player will automatically decode your string after it was loaded. `See the beginning of this guide <http://www.longtailvideo.com/support/jw-player/jw-player-for-flash-v5/12536/configuration-options>`_ for more info.

Example
^^^^^^^

Here is a basic embed code example of a player using the sharing plugin. This example uses the SWFObject 2.0 embedding script:

.. code-block:: html

   <p id="container">The player will be placed here</p>
   
   <script type="text/javascript">
     swfobject.embedSWF("player.swf","container","480","270","9.0.115","false",{
       file: "/static/video/1775.mp4",
       plugins: "sharing",
       "sharing.code": encodeURIComponent(
         '<embed src="http://www.website.com/embeds/1775.swf" width="320 />'
       ),
       "sharing.link": "http://www.website.com/videos/1775/"
     });
   </script>




Using Playlists
---------------

In addition to setting them in the embed code, the two sharing options can also be set per playlist entry. The global configuration options should still be set:

* The *plugins* option to **sharing-2**.
* the *sharing.code* option to **true** if you want to display embed codes.
* the *sharing.link* option to **true** if you want to display share links.

Next, the two options (**sharing.code** and **sharing.link**) can be set per playlistItem. If the playlistItem is not set, the plugin displays fallback codes:

* If there's no **sharing.link**, the page URL is used.
* If there's no **sharing.code**, the following embed code is used:

   .. code-block:: html
   
      <embed allowfullscreen="true" flashvars="file=PLAYER_FILE" 
        height="PLAYER_HEIGHT" src="PLAYER_URL" width="PLAYER_WIDTH" />


Since none of the XML playlist formats define elements for setting sharing links and codes, the two options should be set using the `JWPlayer XML namespace <http://www.longtailvideo.com/support/jw-player/jw-player-for-flash-v5/12537/xml-playlist-support>`_. In practice, the namespace is enabled by:

* Setting an **xmlns:jwplayer** attribute in the main XML tag.
* Prefixing the XML elements with **jwplayer:**, e.g. *<jwplayer:sharing.code>*.

Example
^^^^^^^

Here is a basic example; an RSS playlist with one entry that has both a *sharing.link* and *sharing.code* defined.

.. code-block:: xml

   <rss version="2.0" xmlns:jwplayer="http://developer.longtailvideo.com/">
     <channel>
       <title>Example RSS playlist with sharing options</title>
   
       <item>
         <title>Both code and link</title>
         <enclosure url="http://content.bitsontherun.com/videos/nPripu9l.mp4" />
         <jwplayer:sharing.link>
           http://www.longtailvideo.com
         </jwplayer:sharing.link>
         <jwplayer:sharing.code>
           <![CDATA[
             <embed src="http://video.longtailvideo.com/players/nPripu9l-ALJ3XQCI.swf" 
               width="480" height="270" allowfullscreen="true" />
           ]]>
         </jwplayer:sharing.code>
       </item>
   
     </channel>
   </rss>

.. note::

   Since the embed code may contain special characters like *<*, *"* or *&*, you should enclose it in CDATA tags in the playlist, as displayed in the above example. Otherwise, the XML syntax of the playlist will be broken. `See the playlist support guide <http://www.longtailvideo.com/support/jw-player/jw-player-for-flash-v5/12537/xml-playlist-support>`_ for more info.




Skinning
--------

This plugin can be skinned using the `PNG Skinning Model <http://www.longtailvideo.com/support/jw-player/jw-player-for-flash-v5/14/skinning-the-jw-player-5>`_ of the JW Player. Three aspects of the plugin can be skinned:

* The **share** and **embed** buttons of the controlbar.
* The **share** and **embed** icons of the dock.
* The background of the **share** and **embed** screens.

The skinning functionality is basic and purely visual. It is not possible to rearrange the position of the various elements in the share and embed screens, or to add/remove elements. Here's two examples of what is possible with the skinning of this plugin:

.. image:: ../assets/exampleskins.png
	:alt: Skinning examples.

The sharing screens must both be 300x140 pixels, since the plugin simply maps the coordinates of a mouse click to the action that should be executed. Here is an overview of the mappings:

.. image:: ../assets/skinning.png
	:alt: Sharing plugin screens.


XML Block
^^^^^^^^^

Here is the full XML code block as supported by the sharing plugin:

.. code-block:: html

   ...
   <component name="sharing">
     <elements>
       <element name="embedButton" src="embedButton.png" />
       <element name="embedIcon" src="embedIcon.png" />
       <element name="embedScreen" src="embedScreen.png" />
       <element name="shareButton" src="shareButton.png" />
       <element name="shareIcon" src="shareIcon.png" />
       <element name="shareScreen" src="shareScreen.png" />
     </elements>
   </component>

All PNG images should be placed in a **sharing** subdirectory of the main skin directory.

Example Skins
^^^^^^^^^^^^^

Here's a few skins that include a custom look for the *sharing* plugin (all using the dock, not the controlbar). Feel free to download these and/or tweak their looks:

* `Glow <http://www.longtailvideo.com/addons/skins/196/Glow>`_
* `Modieus <http://www.longtailvideo.com/addons/skins/50/Modieus>`_
* `Bekle <http://www.longtailvideo.com/addons/skins/52/Bekle>`_
* `Stijl <http://www.longtailvideo.com/addons/skins/25/Stijl>`_




Changelog
---------

Version 2.0
^^^^^^^^^^^

* Switched from the 4.x to the 5.x plugin API. As of now, the plugin cannot be loaded in the JW Player v4.
* Added support for a *sharing.code* and *sharing.link* property per PlaylistItem.
* Added support for PNG skinning. As of this version, the buttons and the screens can be skinned.
* Added support for an embed code fallback and share link fallback in case no playlistItem code/link are set.



