# This is a simple script that compiles the plugin using MXMLC (free & cross-platform).
# Learn more at http://developer.longtailvideo.com/trac/wiki/PluginsCompiling

FLEXPATH=/Developer/SDKs/flex_sdk_3

echo "Compiling with MXMLC..."

$FLEXPATH/bin/mxmlc ../src/com/longtailvideo/plugins/sharing/Sharing.as -sp ../src -o ../sharing.swf -library-path+=../lib -load-externs=../lib/jwplayer-5-classes.xml -use-network=false -optimize=true -incremental=false

# $FLEXPATH/bin/compc -source-path ../src -output ../sharing.swc -compiler.library-path $FLEXPATH/frameworks/libs -compiler.library-path ../lib -include-classes com.longtailvideo.plugins.sharing.Sharing -link-report='../sharing-classes.xml'