package {

    import flash.display.Sprite;
    import flash.events.MouseEvent;
    import flash.filters.DropShadowFilter;
    import flash.system.System;
    import flash.text.TextField;
    import flash.text.TextFormat;


    /** 
    * Renders a form row with label, input and copy button
    **/
    public class SharingRow extends Sprite {


        /** background of the textfield. **/
        private var _back:Sprite;
        /** Copy button sprite. **/
        private var _button:Sprite;
        /** The text "Copy" inside the copy button. **/
        private var _copy:TextField;
        /** Actual contents text field. **/
        private var _field:TextField;
        /** Label text field. **/
        private var _label:TextField;


        /** Renders the entire button row. **/
        public function SharingRow(label:String) {

            // Render the label
            _label = new TextField();
            _label.defaultTextFormat = new TextFormat('Arial', 13, 0xFFFFFF, true);
            _label.text = label;
            _label.x = -5;
            _label.y = 3;
            _label.width = 80;
            _label.autoSize = 'right';
            _label.filters = new Array(new DropShadowFilter(1,45,0,1,0,0,1));
            addChild(_label);

            // Render the background
            _back = new Sprite();
            _back.graphics.beginFill(0,1);
            _back.graphics.drawRect(0,0,240,26);
            _back.graphics.beginFill(0xFFFFFF,1);
            _back.graphics.drawRect(1,1,238,24);
            _back.filters = new Array(new DropShadowFilter(0,45,0,1,4,4));
            _back.x = 80;
            addChild(_back);

            // Render the actual text field
            _field = new TextField();
            _field.defaultTextFormat = new TextFormat('Arial', 11, 0x000000);
            _field.x = 84;
            _field.y = 5;
            _field.width = 189;
            _field.height = 20;
            addChild(_field);
            
            // Render the copy button
            _button = new Sprite();
            _button.graphics.beginFill(0,1);
            _button.graphics.drawRect(1,1,44,24);
            _button.buttonMode = true;
            _button.mouseChildren = false;
            _button.addEventListener(MouseEvent.CLICK,_clickHandler);
            _button.addEventListener(MouseEvent.MOUSE_OUT,_outHandler);
            _button.addEventListener(MouseEvent.MOUSE_OVER,_overHandler);
            _button.x = 275;
            _copy = new TextField();
            _copy.x = 8;
            _copy.y = 4;
            _copy.width = 40;
            _copy.height = 20;
            _copy.defaultTextFormat = new TextFormat('Arial', 11, 0xFFFFFF);
            _copy.text = 'Copy';
            _button.addChild(_copy);
            addChild(_button);
        };

        /** Select text on click. **/
        private function _clickHandler(event:MouseEvent):void {
            stage.focus = _field;
            _field.setSelection(0,999999);
            System.setClipboard(_field.text);
        };


        /** Restore shadow color when rolling out button. **/
        private function _outHandler(event:MouseEvent):void {
            _back.graphics.clear();
            _back.graphics.beginFill(0,1);
            _back.graphics.drawRect(0,0,240,26);
            _back.graphics.beginFill(0xFFFFFF,1);
            _back.graphics.drawRect(1,1,238,24);
            _back.filters = new Array(new DropShadowFilter(0,45,0,1,4,4));
        };


        /** Change shadow color when rolling over button. **/
        private function _overHandler(event:MouseEvent):void {
            _back.graphics.clear();
            _back.graphics.beginFill(0xFFFFFF,1);
            _back.graphics.drawRect(0,0,240,26);
            _back.filters = new Array(new DropShadowFilter(0,45,0xFFFFFF,1,4,4));
        };


        /** Setter for the field contents. **/
        public function setText(text:String):void {
            _field.text = text;
        };


    }
}
