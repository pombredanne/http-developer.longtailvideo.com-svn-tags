package {

    import com.longtailvideo.jwplayer.events.*;
    import com.longtailvideo.jwplayer.player.*;
    import com.longtailvideo.jwplayer.plugins.*;
    import com.longtailvideo.jwplayer.utils.*;

    import flash.display.*;
    import flash.events.MouseEvent;
    import flash.external.ExternalInterface;
    import flash.filters.DropShadowFilter;
    import flash.net.URLRequest;
    import flash.net.navigateToURL;
    import flash.text.TextField;
    import flash.text.TextFormat;


    /**
    * This plugin displays a dialog with embed code, video link and Facebook/Twitter icons.
    **/
    public class Sharing extends Sprite implements IPlugin {


        /** Embedding the image assets. **/
        [Embed(source="../../assets/closeButton.png")]
        private const CloseButton:Class;
        [Embed(source="../../assets/dialogIcon.png")]
        private const DialogIcon:Class;
        [Embed(source="../../assets/dialogBack.png")]
        private const DialogBack:Class;
        [Embed(source="../../assets/facebookIcon.png")]
        private const FacebookIcon:Class;
        [Embed(source="../../assets/shareIcon.png")]
        private const ShareIcon:Class;
        [Embed(source="../../assets/tweetIcon.png")]
        private const TweetIcon:Class;
        [Embed(source="../../assets/twitterIcon.png")]
        private const TwitterIcon:Class;


        /** URL for Facebook and Twitter sharing dialogues. **/
        public const FACEBOOK_URL:String = 'http://www.facebook.com/sharer/sharer.php?u=';
        public const TWITTER_URL:String = 'http://twitter.com/intent/tweet?url=';


        /** Reference to the background sheet. **/
        private var _back:Sprite;
        /** Reference to the close button. **/
        private var _close:Sprite;
        /** The current embed code. **/
        private var _code:String;
        /** The complete form row with embed code. **/
        private var _codeRow:SharingRow;
        /** The plugin configuration options.**/
        private var _config:Object;
        /** Clip with all graphics. **/
        private var _container:MovieClip;
        /** Dock button list, which are hidden/restored for OVA. **/
        private var _dockButtons:Array;
        /** Reference to the facebook button. **/
        private var _facebookButton:SharingButton;
        /** The form with all buttons. **/
        private var _form:Sprite;
        /** The heading text. **/
        private var _headingText:TextField;
        /** The current video link. **/
        private var _link:String;
        /** The complete form row with video link. **/
        private var _linkRow:SharingRow;
        /** Reference to the player. **/
        private var _player:IPlayer;
        /** Reference to the twitter button. **/
        private var _twitterButton:SharingButton;


        /** Hide the dialog when clicking its background. **/
        private function _backHandler(evt:MouseEvent):void {
            new Animations(_container).fade(0,0.2);
            // Only 5.7+...
            try { 
                (_player.controls.display as Object).show();
                (_player.controls.dock as Object).show();
            } catch (error:Error) {}
        };


        /** Show the dialog when clicking its dock icon. **/
        private function _dialogHandler(evt:MouseEvent):void {
            _player.pause();
            new Animations(_container).fade(1,0.2);
            // Only 5.7+...
            try { 
                (_player.controls.display as Object).hide();
                (_player.controls.dock as Object).hide();
            } catch (error:Error) {}
        };


        /** The facebook button is clicked. **/
        private function _facebookHandler(evt:MouseEvent):void {
            navigateToURL(new URLRequest(FACEBOOK_URL+encodeURIComponent(_link)));
        };


        /** Grab the page URL with some javascript magic. **/
        private function _getPageURL():String {
            var url:String = '';
            if(ExternalInterface.available) {
                try {
                    url =  ExternalInterface.call('function(){if(window.top==window)return window.location.toString();else return document.referrer;}');
                } catch (err:Error) {}
            }
            return url;
        };


        /** Returns the plugin name. **/
        public function get id():String {
            return "sharing";
        };


        /** Called by the player to initialize; setup events and dock buttons.  **/
        public function initPlugin(player:IPlayer, config:PluginConfig):void {
            _player = player;
            _config = config;
            _player.addEventListener(PlaylistEvent.JWPLAYER_PLAYLIST_ITEM, _itemHandler);
            // Setup the dock buttons
            _dockButtons = new Array();
            if(_config.shortcuts) {
                _dockButtons.push(_player.controls.dock.addButton(new ShareIcon(), "share", _facebookHandler));
                _dockButtons.push(_player.controls.dock.addButton(new TweetIcon(), "tweet", _twitterHandler));
            } else {
                _dockButtons.push(_player.controls.dock.addButton(new DialogIcon(), "share", _dialogHandler));
                _renderDialog();
            }
        };


        /** Change the embed code when an item changes.  */
        public function _itemHandler(evt:PlaylistEvent):void {
            // Set the embed code.
            var item:Object = _player.playlist.currentItem;
            _code = '';
            if(item['sharing.code']) {
                _code = item['sharing.code'];
            } else if(_config.code) {
                _code = _config.code.replace('MEDIAID',item['mediaid']);;
            }
            if(_code.substr(0,3) == '%3C') { 
                _code = decodeURIComponent(_code);
            }
            // Set the video link.
            _link = _getPageURL();
            if(item['sharing.link']) {
                _link = item['sharing.link'];
            } else if(item.link) {
                _link = item.link;
            } else if(_config.link) {
                _link = _config.link.replace('MEDIAID',item['mediaid']);
            }
            // Update the form elements only if the dialog is available.
            if(_container) {
                _codeRow.setText(_code);
                _linkRow.setText(_link);
                // Show/hide the embed dialog per entry.
                if(_code == '') {
                    _codeRow.visible = false;
                    _linkRow.y = 32;
                    _facebookButton.y = _twitterButton.y = 74;
                } else {
                    _codeRow.visible = true;
                    _codeRow.y = 32;
                    _linkRow.y = 74;
                    _facebookButton.y = _twitterButton.y = 116;
                }
                resize(_back.width,_back.height);
            }
            // Hide the dock button(s) if OVA is used.
            if(item['ova.hidden']) { 
                _dockButtons[0].visible = false;
                if(_dockButtons[1]) { _dockButtons[1].visible = false; }
            } else { 
                _dockButtons[0].visible = true;
                if(_dockButtons[1]) { _dockButtons[1].visible = true; }
            }
        };


        /** The dialog is only rendered when the shortcuts are not set. **/
        private function _renderDialog():void {
            // Draw the background and close button.
            _container = new MovieClip();
            _container.visible = false;
            _container.alpha = 0;
            addChild(_container);
            _back = new Sprite();
            _back.buttonMode = true;
            _back.addChild(new DialogBack());
            _back.addEventListener(MouseEvent.CLICK,_backHandler);
            _container.addChild(_back);
            _close = new Sprite();
            _close.buttonMode = true;
            _close.addChild(new CloseButton());
            _close.addEventListener(MouseEvent.CLICK,_backHandler);
            _container.addChild(_close);

            // Draw the form wrapper and heading.
            _form = new Sprite();
            _container.addChild(_form);
            _headingText = new TextField();
            _headingText.width = 240;
            _headingText.height = 24;
            _headingText.defaultTextFormat = new TextFormat('Arial', 16, 0xFFFFFF, true);
            if(_config.heading is String) {
                _headingText.text = _config.heading;
            } else {
                _headingText.text = "Share this video";
            }
            _headingText.x = 80;
            _headingText.filters = new Array(new DropShadowFilter(1,45,0,1,0,0,2));
            _form.addChild(_headingText);


            // Draw the rows and buttons
            _codeRow = new SharingRow('Embed code');
            _form.addChild(_codeRow);
            _linkRow = new SharingRow('Video link');
            _form.addChild(_linkRow);
            _facebookButton = new SharingButton(new FacebookIcon(),'Facebook',_facebookHandler);
            _facebookButton.x = 80;
            _form.addChild(_facebookButton);
            _twitterButton = new SharingButton(new TwitterIcon(),'Twitter',_twitterHandler);
            _twitterButton.x = 200;
            _form.addChild(_twitterButton);
        };


        /** Reposition the screens when the player resizes itself **/
        public function resize(wid:Number, hei:Number):void {
            if(_container) {
                _back.width = wid;
                _back.height = hei;
                _close.x = wid - 50;
                _form.x = Math.round(wid/2 - 165);
                _form.y = Math.round(hei/2 - _form.height/2);
            }
        };


        /** The twitter button is clicked. **/
        private function _twitterHandler(evt:MouseEvent):void {
            navigateToURL(new URLRequest(TWITTER_URL+encodeURIComponent(_link)));
        };


    }
}
