package {

    import flash.display.DisplayObject;
    import flash.display.Sprite;
    import flash.events.MouseEvent;
    import flash.filters.DropShadowFilter;
    import flash.text.TextField;
    import flash.text.TextFormat;


    /** 
    * Renders a form row with label, input and copy button
    **/
    public class SharingButton extends Sprite {


        /** sprite to contain the icon. **/
        private var _thumb:Sprite;
        /** The small "post to" text. **/
        private var _post:TextField;
        /** Name of the button. **/
        private var _name:TextField;


        /** Renders the entire button row. **/
        public function SharingButton(icon:DisplayObject, name:String, handler:Function) {

            // Create area and add clickHandler
            graphics.beginFill(0,0);
            graphics.drawRect(0,0,80,32);
            buttonMode = true;
            mouseChildren = false;
            addEventListener(MouseEvent.CLICK,handler);
            addEventListener(MouseEvent.MOUSE_OUT,_outHandler);
            addEventListener(MouseEvent.MOUSE_OVER,_overHandler);

            // Render the icon
            _thumb = new Sprite();
            _thumb.graphics.beginFill(0,1);
            _thumb.graphics.drawRect(0,0,32,32);
            _thumb.filters = new Array(new DropShadowFilter(0,45,0,1,4,4));
            icon.x = icon.y = 1;
            _thumb.addChild(icon);
            addChild(_thumb);

            // Render the post to test
            _post = new TextField();
            _post.defaultTextFormat = new TextFormat('Arial', 11, 0xFFFFFF);
            _post.text = "Post to";
            _post.x = 38;
            _post.y = 1;
            _post.filters = new Array(new DropShadowFilter(1,45,0,1,0,0,2));
            _post.height = 20;
            _post.width = 40;
            addChild(_post);

            // Render the name
            _name = new TextField();
            _name.defaultTextFormat = new TextFormat('Arial', 12, 0xFFFFFF, true);
            _name.text = name;
            _name.x = 38;
            _name.y = 15;
            _name.height = 20;
            _name.width = 60;
            _name.filters = new Array(new DropShadowFilter(1,45,0,1,0,0,2));
            addChild(_name);
        };


        /** Restore shadow color when rolling out button. **/
        private function _outHandler(event:MouseEvent):void {
            _thumb.graphics.clear();
            _thumb.graphics.beginFill(0,1);
            _thumb.graphics.drawRect(0,0,32,32);
            _thumb.filters = new Array(new DropShadowFilter(0,45,0,1,4,4));
        };


        /** Change shadow color when rolling over button. **/
        private function _overHandler(event:MouseEvent):void {
            _thumb.graphics.clear();
            _thumb.graphics.beginFill(0xFFFFFF,1);
            _thumb.graphics.drawRect(0,0,32,32);
            _thumb.filters = new Array(new DropShadowFilter(0,45,0xFFFFFF,1,4,4));
        };


    }
}