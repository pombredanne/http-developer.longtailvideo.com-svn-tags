package com.jeroenwijering.plugins {


import com.jeroenwijering.events.*;
import com.jeroenwijering.utils.Logger;

import flash.display.*;
import flash.events.*;
import flash.media.*;
import flash.net.*;
import flash.utils.*;
import flash.text.*;


/**
* This plugin automatically polls an RTMP stream for availability every XX seconds.
* If the stream is available, it will kick in.
* It allows users to wait for a live event without having to refresh their page.
**/
public class Livestream extends MovieClip implements PluginInterface {


	[Embed(source="../../../animation.swf")]
	private const Animation:Class;


	/** Background image. **/
	private var back:Sprite;
	/** List with configuration settings. **/
	public var config:Object = {
		file:undefined,
		image:undefined,
		interval:15,
		message:'Checking for livestream...',
		streamer:undefined
	};
	/** Netconnection instance to check availability. **/
	private var connection:NetConnection;
	/** The textfield showing all messages. **/
	private var field:TextField;
	/** Interval for checking the connection. **/
	private var interval:Number;
	/** Netstream instance to check availability. **/
	private var stream:NetStream;
	/** Reference to the view. **/
	private var view:AbstractView;


	/** Constructor. **/
	public function Livestream():void {
		connection = new NetConnection();
		connection.client = new Object();
		build();
	};


	/** Build all stage graphics. **/
	private function build():void {
		back = new Sprite();
		back.graphics.beginFill(0x000000,0.9);
		back.graphics.drawRect(0,0,300,70);
		addChild(back);
		var anm:DisplayObject = new Animation();
		anm.x = 134;
		anm.y = 10;
		addChild(anm);
		field = new TextField();
		field.defaultTextFormat = new TextFormat('_sans',13,0xFFFFFF,null,null,null,null,null,TextFormatAlign.CENTER);
		field.width = 300;
		field.height = 20;
		field.y = 45;
		addChild(field);
		mouseEnabled = false;
		visible = false;
	};


	/** Try connecting to the livestream. **/
	private function check():void {
		connection.addEventListener(NetStatusEvent.NET_STATUS,statusHandler);
		connection.connect(config['streamer']);
		visible = true;
		setTimeout(hide,2000);
		setTimeout(drop,5000);
		Logger.log('Attempting to connect to stream.','livestream');
	};


	/** Drop the connection a few seconds after trying. **/
	private function drop():void {
		stream.removeEventListener(NetStatusEvent.NET_STATUS,statusHandler);
		connection.removeEventListener(NetStatusEvent.NET_STATUS,statusHandler);
		stream.close();
		connection.close();
		Logger.log('Dropping stream connection.','livestream');
	};


	/** Hide the icon again after a check. **/
	private function hide():void {
		visible = false;
	};


	/**
	* Start the plugin.
	*
	* @param vie	A reference to the View of the player; the API entrypoint.
	**/
	public function initializePlugin(vie:AbstractView):void {
		view = vie;
		view.addControllerListener(ControllerEvent.RESIZE,resizeHandler);
		field.text = config['message'];
		interval = setInterval(check,config['interval']*1000);
		setTimeout(check,2000);
	};


	/** The livestream is found. Now switch to it. **/
	private function load():void {
		view.sendEvent('LOAD',{
			duration:0,
			file:config['file'],
			image:config['image'],
			streamer:config['streamer'],
			type:'rtmp'
		});
		setTimeout(view.sendEvent,100,'ITEM',0);
	};


	/** Reposition the icon on resize. **/
	private function resizeHandler(evt:ControllerEvent):void {
		x = config['x'] + config['width']/2 - 150;
		y = config['y'] + config['height']/2 - 35;
	};


	/** Receive NetStream status updates. **/
	private function statusHandler(evt:NetStatusEvent):void {
		Logger.log(evt.info,'livestream');
		switch(evt.info.code) {
			case 'NetConnection.Connect.Success':
				stream = new NetStream(connection);
				stream.client = new Object();
				stream.addEventListener(NetStatusEvent.NET_STATUS,statusHandler);
				stream.bufferTime = 0.1;
				stream.play(config['file']);
				break;
			case 'NetStream.Buffer.Full':
				clearInterval(interval);
				setTimeout(load,1000);
				break;
		}
	};


};


}