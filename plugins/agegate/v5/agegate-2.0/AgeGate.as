package
{
	import com.longtailvideo.jwplayer.events.PlayerEvent;
	import com.longtailvideo.jwplayer.events.PlaylistEvent;
	import com.longtailvideo.jwplayer.events.ViewEvent;
	import com.longtailvideo.jwplayer.player.IPlayer;
	import com.longtailvideo.jwplayer.plugins.IPlugin;
	import com.longtailvideo.jwplayer.plugins.PluginConfig;
	import com.longtailvideo.jwplayer.utils.RootReference;
	
	import fl.controls.Label;
	import fl.core.UIComponent;
	import fl.data.DataProvider;
	import fl.transitions.Fade;
	import fl.transitions.Transition;
	import fl.transitions.TransitionManager;
	import fl.transitions.easing.Strong;
	
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.net.SharedObject;
	import flash.net.URLRequest;
	import flash.net.navigateToURL;
	import flash.text.TextFormat;
	
	public class AgeGate extends MovieClip implements IPlugin
	{				
		private var api:IPlayer;
		private var pluginConfig:PluginConfig;
		private const RESULT_OFFSET:Number = 50;
		private const HEADER_OFFSET:Number = 50;
		private const SUB_HEADER_OFFSET:Number = 30; 
		
		private var monthArray:Array = new Array({label: 'January', data: '1'},
			{label: 'February', data: '2'},
			{label: 'March', data: '3'},
			{label: 'April', data: '4'},
			{label: 'May', data: '5'},
			{label: 'June', data: '6'},
			{label: 'July', data: '7'},
			{label: 'August', data: '8'},
			{label: 'September', data: '9'},
			{label: 'October', data: '10'},
			{label: 'November', data: '11'},
			{label: 'December', data: '12'});
		private var currentDate:Date = new Date();
		/** Have the display elements been initialized **/
		private var displayInited:Boolean = false;
		/** The year input UI element **/
		private var yearInput:YearSelector;
		/** The month input UI element **/
		private var monthInput:MonthSelector;
		/** The day input UI element **/
		private var dayInput:DaySelector;
		/** The submit button UI element **/
		private var submitButton:SubmitButton;
		/** The failure message UI element **/
		private var result:Label;
		/** The description message UI element **/
		private var header:Label;
		/** The instructional message UI element **/
		private var subheader:Label;
		/** The background which covers the player. **/
		private var background:MovieClip;
		/** The fullscreen exit message UI element **/
		private var fullscreenexit_message:Label;
		/** The prefix for all flashvars **/
		private var prefix:String = "agegate";
		/** The default settings **/
		private var defaults:Object = {
			minage: 18,
			maxage: 100,
			message: 'You do not meet the age criteria for this video.',
			redirecturl: '',
			redirecttarget: '',
			backgroundcolor: 0x000000,
			textcolor: 0xFF0000,
			bordercolor: 0xFFFFFF,
			exitfullscreen_message: 'Exiting fullscreen',
			cookielife: 60,
			header: 'The content in this video requires age verification.',
			subheader: 'Please enter your date of birth below:',
			autostart: false
		};
		
		public function AgeGate()
		{
		}		
		
		public function initPlugin(player:IPlayer, config:PluginConfig):void
		{
			api = player;
			pluginConfig = config;
			api.addEventListener(PlaylistEvent.JWPLAYER_PLAYLIST_LOADED, playlistHandler);
			api.addEventListener(PlaylistEvent.JWPLAYER_PLAYLIST_ITEM, itemHandler);
			api.addEventListener(PlayerEvent.JWPLAYER_READY, readyHandler);
			api.config[prefix + '.autostart'] = api.config['autostart'];
			api.config['autostart'] = false;
		}
		
		public function resize(width:Number, height:Number):void
		{
			var h:Number = height;
			var w:Number = width;
			if (background){
				background.height = h;
				background.width = w;
			}
			if (result){
				var yHeight:Number = (h / 2) - 10;
				result.x = w / 2 - (result.width) / 2;
				result.y = yHeight + RESULT_OFFSET;
			}
			if (displayInited){
				centerDisplay(h, w);
			}
		}
		
		public function get id():String
		{
			return "agegate";
		}
		
		private function playlistHandler(evt:Event = null):void {
			if (!getStoredBirthdate()){
				if (!displayInited){
					initInput();
				}
				showInput();
			} else {
				if (getConfig('autostart') == 'true'){
					api.dispatchEvent(new ViewEvent(ViewEvent.JWPLAYER_VIEW_PLAY, true));
				}
			}
		}
		
		private function itemHandler(evt:Event = null):void {
			if (getStoredBirthdate()) {
				hideResult();
				if (!isOldEnough()) {
					api.dispatchEvent(new ViewEvent(ViewEvent.JWPLAYER_VIEW_STOP, true));
					showResult();
				}
			}
		}
		
		private function readyHandler(evt:Event = null):void {
			if (!getStoredBirthdate()) {
				api.lock(this, playlistHandler);
			}
		}
		
		private function centerDisplay(h:Number, w:Number):void {
			monthInput.x = (w / 2) - 150;
			dayInput.x = monthInput.x + 110;
			yearInput.x = dayInput.x + 60;
			submitButton.x = yearInput.x + 80;
			header.x = w / 2 - (header.width) / 2;
			subheader.x = w / 2 - (subheader.width) / 2;
			fullscreenexit_message.x = w / 2 - (fullscreenexit_message.width) / 2;
			var yHeight:Number = (h / 2) - 10;
			monthInput.y = yHeight;
			dayInput.y = yHeight;
			yearInput.y = yHeight;
			submitButton.y = yHeight;
			fullscreenexit_message.y = yHeight + SUB_HEADER_OFFSET;
			header.y = yHeight - HEADER_OFFSET;
			subheader.y = yHeight - SUB_HEADER_OFFSET;
			if (result){
				result.x = w / 2 - (result.width) / 2;
				result.y = yHeight + RESULT_OFFSET;
			}
		}
		
		private function getStoredBirthdate():Date {
			var birthDate:Date;
			var cookie:SharedObject = SharedObject.getLocal("birthdate");
			if (cookie.data['birthdate']){
				var saveDate:Date = cookie.data['savedate'] as Date;
				var currentDate:Date = new Date(); 
				if (currentDate.getTime() - saveDate.getTime() < Number(getConfig("cookielife")) * 60000) {
					birthDate = cookie.data['birthdate'] as Date;
				}
			}
			return birthDate;
		}
		
		private function initInput():void {
			initBackground();
			monthInput = new MonthSelector();
			monthInput.dataProvider = new DataProvider(monthArray);
			monthInput.selectedIndex = 0;
			monthInput.addEventListener(Event.CHANGE, changeHandler);
			addDisplayElement(monthInput, 100, 22, 0, 0);
			dayInput = new DaySelector();
			dayInput.dataProvider = new DataProvider(getDayArray());
			dayInput.selectedIndex = 0;
			addDisplayElement(dayInput, 50, 22, 110, 0);
			yearInput = new YearSelector();
			yearInput.dataProvider = new DataProvider(getYearArray());
			yearInput.selectedIndex = 0;
			addDisplayElement(yearInput, 60, 22, 170, 0);
			submitButton = new SubmitButton();
			submitButton.label = "Verify";
			submitButton.addEventListener(MouseEvent.CLICK, submitHandler);
			addDisplayElement(submitButton, 50, 22, 240, 0);
			header = new Label();
			header.textField.text = String(getConfig('header'));
			setFontColor(header, uint(0xFFFFFF), 14);
			addDisplayElement(header, api.config['width'], header.height, 0, api.config['height'] / 2 - header.height);
			subheader = new Label();
			subheader.textField.text = String(getConfig('subheader'));
			setFontColor(subheader, uint(0xFFCC00));
			addDisplayElement(subheader, api.config['width'], subheader.height, 0, api.config['height'] / 2 - subheader.height);
			fullscreenexit_message = new Label();
			fullscreenexit_message.visible = false;
			fullscreenexit_message.textField.text = String(getConfig('exitfullscreen_message'));
			setFontColor(fullscreenexit_message, uint(getConfig('textcolor')));
			addDisplayElement(fullscreenexit_message, api.config['width'], api.config['height'] / 2, 0, 
				api.config['height'] / 2 - header.height / 2);
			yearInput.addEventListener(MouseEvent.CLICK, avoidFullScreen);
			displayInited = true;
		}
		
		private function showInput():void {
			resize(api.config['width'], api.config['height']);
			setDisplayElementsVisibility(true);
			visible = true;
			background.visible = true;
		}
		
		private function getConfig(param:String):String {
			var result:String;
			if (api.playlist.getItemAt(api.config['item'])[prefix+'.'+param]){
				result = api.playlist.getItemAt(api.config['item'])[prefix+'.'+param];
			} else if (pluginConfig[param]) {
				result = pluginConfig[param];
			} else {
				result = defaults[param];
			}
			return result;
		}
		
		private function hideResult():void {
			initResult(false);
		}
		
		private function isOldEnough():Boolean {
			var result:Boolean = false;
			var birthDate:Date = getBirthDate();
			if ((currentDate.fullYear - birthDate.getFullYear()) > Number(getConfig('maxage'))) {
				result = false;
				return result;
			} else if ((currentDate.fullYear - birthDate.getFullYear()) == Number(getConfig('maxage'))) {
				if (currentDate.month < birthDate.getMonth()) {
					result = false;
					return result;
				}
				if (currentDate.month == birthDate.getMonth()) {
					if (currentDate.date >= birthDate.getDate()) {
						result = false;
						return result;
					}
				}
			}
			if ((currentDate.fullYear - birthDate.getFullYear()) > Number(getConfig('minage'))) {
				result = true;
			} else if ((currentDate.fullYear - birthDate.getFullYear()) > (Number(getConfig('minage')) - 1)) {
				if (currentDate.month > birthDate.getMonth()) {
					result = true;
				} else if (currentDate.month >= birthDate.getMonth()) {
					if (currentDate.date >= birthDate.getDate()) {
						result = true;
					}
				}
			}
			return result;
		}
		
		private function showResult():void {
			initResult(true);
			resize(api.config['width'], api.config['height']);
			visible = true;
		}
		
		private function initBackground(display:Boolean = true):void{
			if (!background){
				background = new MovieClip();
				background.alpha = 1;
				addChild(background);
				background.graphics.clear();
				background.graphics.beginFill(uint(getConfig('backgroundcolor')));
				background.graphics.drawRect(0, 0, 1000, 1000);
				setChildIndex(background, 0);
			}
			background.visible = display;
		}
		
		private function changeHandler(event:Event):void {
			dayInput.dataProvider = new DataProvider(getDayArray(Number(monthInput.value)));
		}
		
		private function addDisplayElement(displayElement:Sprite, width:Number = 0, height:Number = 0, x:Number = 0, y:Number = 0):void {
			displayElement.width = width;
			displayElement.height = height;
			displayElement.x = x;
			displayElement.y = y;
			addChild(displayElement);
		}
		
		private function getDayArray(i:Number = 0):Array {
			var result:Array = new Array();
			if (i == 2) {
				result = getRange(0, 29);
			} else if (i == 4 || i == 6 || i == 9 || i == 11) {
				result = getRange(0, 30);
			} else {
				result = getRange(0, 31);
			}
			return result;
		}
		
		private function getYearArray():Array {
			var result:Array = new Array();
			result = getRange(currentDate.getFullYear() - 100, currentDate.getFullYear());
			result.reverse();
			return result;
		}
		
		private function getRange(start:Number, end:Number, factor:Number = 1):Array {
			var result:Array = new Array();
			var i:Number = start;
			while (i != end) {
				i = i + factor;
				result.push(i);
			}
			return result;
		}
		
		private function submitHandler(evt:MouseEvent):void {
			if (isOldEnough()) {
				setBirthDate();				
				fadeOut();
				api.unlock(this);
			} else {
				showResult();
			}
		}
		
		private function setFontColor(displayElement:UIComponent, fontColor:uint, fontSize:uint = 12):void {
			var tf:TextFormat = new TextFormat();
			tf.color = fontColor;
			tf.align = "center";
			tf.font = "_sans";
			tf.size = fontSize;
			displayElement.setStyle("textFormat", tf);
		}
		
		private function avoidFullScreen(evt:Event):void {
			if (api.config['fullscreen']) {
				api.dispatchEvent(new ViewEvent(ViewEvent.JWPLAYER_VIEW_FULLSCREEN, false));
				fullscreenexit_message.visible = true;
			}
		}
		
		private function initResult(display:Boolean = false):void{
			initBackground(display);
			if (!result){
				result = new Label();
				result.textField.text = String(getConfig('message'));
				setFontColor(result, uint(getConfig('textcolor')));
				addDisplayElement(result, api.config['width'], result.height, 0, 0);
				result.addEventListener(MouseEvent.CLICK, redirecturl);
			}
			result.visible = display;
		}
		
		private function setDisplayElementsVisibility(state:Boolean):void {
			yearInput.visible = state;
			monthInput.visible = state;
			dayInput.visible = state;
			submitButton.visible = state;
			background.visible = state;
			header.visible = state;
			subheader.visible = state;
			if (state == false){
				fullscreenexit_message.visible = state;
			}
		}
		
		private function getBirthDate():Date {
			var birthDate:Date = getStoredBirthdate();
			if (!birthDate){
				var year:Number = Number(yearInput.textField.text);
				var month:Number = Number(monthInput.value) - 1;
				var day:Number = Number(dayInput.value);
				birthDate = new Date(year, month, day);
			}
			return birthDate;
		}
		
		private function setBirthDate():void {
			var cookie:SharedObject = SharedObject.getLocal("birthdate");
			cookie.data.birthdate = getBirthDate();
			cookie.data.savedate = new Date();
			cookie.flush();
		}
		
		private function fadeOut():void {
			hideInput();
			if (result) {
				result.visible = false;
			}
			fadeOutBackground();
		}
		
		private function hideInput():void {
			setDisplayElementsVisibility(false);
		}
		
		private function fadeOutBackground():void {
			var myTM:TransitionManager = new TransitionManager(this);
			myTM.startTransition({type: Fade, direction: Transition.OUT, duration: 3, easing: Strong.easeOut});
			myTM.addEventListener("allTransitionsOutDone", completeFadeOut);
		}
		
		private function completeFadeOut(evt:Event):void {
			visible = false;
			if (getConfig('autostart') == 'true'){
				api.dispatchEvent(new ViewEvent(ViewEvent.JWPLAYER_VIEW_PLAY, true));
			}
		}
		
		private function redirecturl(evt:MouseEvent):void {
			if (getConfig('redirecturl')) {
				var targetURL:String = String(getConfig('redirecturl'));
				navigateToURL(new URLRequest(targetURL), String(getConfig('redirecttarget')));
			}
		}
	}
}