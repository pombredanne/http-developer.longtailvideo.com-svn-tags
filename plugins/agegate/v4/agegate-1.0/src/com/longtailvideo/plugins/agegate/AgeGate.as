package com.longtailvideo.plugins.agegate {
	import com.jeroenwijering.events.AbstractView;
	import com.jeroenwijering.events.ControllerEvent;
	import com.jeroenwijering.events.PluginInterface;
	import com.jeroenwijering.events.ViewEvent;
	
	import fl.controls.ComboBox;
	import fl.controls.Label;
	import fl.controls.TextInput;
	import fl.core.UIComponent;
	import fl.data.DataProvider;
	import fl.transitions.Fade;
	import fl.transitions.Transition;
	import fl.transitions.TransitionManager;
	import fl.transitions.easing.Strong;
	
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.net.SharedObject;
	import flash.net.URLRequest;
	import flash.net.navigateToURL;
	import flash.text.TextField;
	import flash.text.TextFormat;
	
	
	public class AgeGate extends MovieClip implements PluginInterface {
		/** Reference to the plugin flashvars. **/
		public var config:Object = {};
		/** Reference to the View of the player. **/
		private var view:AbstractView;
		private var monthArray:Array = new Array({label: 'January', data: '1'},
												 {label: 'February', data: '2'},
												 {label: 'March', data: '3'},
												 {label: 'April', data: '4'},
												 {label: 'May', data: '5'},
												 {label: 'June', data: '6'},
												 {label: 'July', data: '7'},
												 {label: 'August', data: '8'},
												 {label: 'September', data: '9'},
												 {label: 'October', data: '10'},
												 {label: 'November', data: '11'},
												 {label: 'December', data: '12'});
		private var currentDate:Date = new Date();
		/** Have the display elements been initialized **/
		private var displayInited:Boolean = false;
		/** The year input UI element **/
		private var yearInput:TextInput;
		/** The month input UI element **/
		private var monthInput:ComboBox;
		/** The day input UI element **/
		private var dayInput:ComboBox;
		/** The submit button UI element **/
		private var submitButton:Label;
		/** The failure message UI element **/
		private var result:Label;
		/** The background which covers the player. **/
		private var background:MovieClip;
		/** The fullscreen exit message UI element **/
		private var fullscreenexit_message:Label;
		/** The prefix for all flashvars **/
		private var prefix:String = "agegate";
		/** The default settings **/
		private var defaults:Object = {
			minage: 18,
			maxage: 100,
			message: 'You do not meet the age criteria for this video.',
			redirecturl: '',
			redirecttarget: '',
			backgroundcolor: 0x000000,
			textcolor: 0xFFFFFF,
			bordercolor: 0xFFFFFF,
			exitfullscreen_message: 'Exiting fullscreen',
			autostart: false
		};
		
		
		/** Constructor. Formats a textfield and places it on the display. **/
		public function AgeGate():void {
		}
		
		
		/**
		 * The initialize call is invoked by the player on startup.
		 * It gives a reference to the player.
		 * With it, we can start listening to the time and playback state.
		 **/
		public function initializePlugin(view:AbstractView):void {
			view.addControllerListener(ControllerEvent.PLAYLIST, playlistHandler);			
			view.addControllerListener(ControllerEvent.ITEM, itemHandler);			
			view.addControllerListener(ControllerEvent.RESIZE, resizeHandler);
			this.view = view;
			view.config[prefix+'.autostart'] = view.config['autostart'];
			view.config['autostart'] = false;
		}
		
		
		private function playlistHandler(evt:Event = null):void {
			if (!getStoredBirthdate()){
				if (!displayInited){
					initInput();
				}
				showInput();
			} else {
				if (getConfig('autostart') == 'true'){
					view.sendEvent(ViewEvent.PLAY, true);
				}
			}
		}
		
		
		private function initInput():void {
			initBackground();
			monthInput = new ComboBox();
			monthInput.dataProvider = new DataProvider(monthArray);
			monthInput.addEventListener(Event.CHANGE, changeHandler);
			addDisplayElement(monthInput, 100, 20, 0, 0);
			dayInput = new ComboBox();
			dayInput.dataProvider = new DataProvider(getDayArray());
			addDisplayElement(dayInput, 50, 20, 110, 0);
			yearInput = new TextInput();
			yearInput.text = currentDate.getFullYear().toString();
			addDisplayElement(yearInput, 40, 20, 170, 0);
			submitButton = new Label();
			submitButton.text = "Submit";
			submitButton.addEventListener(MouseEvent.CLICK, submitHandler);
			setFontColor(submitButton, uint(getConfig('textcolor')));
			setBorderColor(submitButton.textField, uint(getConfig('bordercolor')));
			addDisplayElement(submitButton, 50, 20, 220, 0);
			fullscreenexit_message = new Label();
			fullscreenexit_message.visible = false;
			fullscreenexit_message.textField.text = String(getConfig('exitfullscreen_message'));
			setFontColor(fullscreenexit_message, uint(getConfig('textcolor')));
			addDisplayElement(fullscreenexit_message, view.config['width'], view.config['height'] / 2, 0, 0);
			yearInput.addEventListener(MouseEvent.CLICK, avoidFullScreen);
			displayInited = true;
		}
		
		
		private function showInput():void {
			resizeHandler();
			placeSelfOnTop();
			setDisplayElementsVisibility(true);
			visible = true;
		}
		
		private function hideInput():void {
			setDisplayElementsVisibility(false);
		}		
		
		private function setDisplayElementsVisibility(state:Boolean):void {
			yearInput.visible = state;
			monthInput.visible = state;
			dayInput.visible = state;
			submitButton.visible = state;
			background.visible = state;
			if (state = false){
				fullscreenexit_message.visible = state;
			}
		}
		
		
		private function initBackground(display:Boolean = true):void{
			if (!background){
				background = new MovieClip();
				background.alpha = 1;
				addChild(background);
				background.graphics.clear();
				background.graphics.beginFill(uint(getConfig('backgroundcolor')));
				background.graphics.drawRect(0, 0, 1000, 1000);
				setChildIndex(background, 0);
			}
			background.visible = display;
		}
		
		private function showResult():void {
			initResult(true);
			resizeHandler();
			placeSelfOnTop();
			visible = true;
		}
		
		private function hideResult():void {
			initResult(false);
		}
		
		private function initResult(display:Boolean = false):void{
			initBackground(display);
			if (!result){
				result = new Label();
				result.textField.text = String(getConfig('message'));
				setFontColor(result, uint(getConfig('textcolor')));
				addDisplayElement(result, view.config['width'], view.config['height'] / 2, 0, 0);
				result.addEventListener(MouseEvent.CLICK, redirecturl);
			}
			result.visible = display;
		}
		
		private function submitHandler(evt:MouseEvent):void {
			if (isOldEnough()) {
				fadeOut();
			} else {
				hideInput();
				showResult();
			}
		}
		
		
		private function itemHandler(evt:Event = null):void {
			hideResult();
			if (!isOldEnough()) {
				view.sendEvent('STOP');
				showResult();
			}
		}
		
		
		private function resizeHandler(evt:Event = null):void {
			var h:Number = view.config['height'];
			var w:Number = view.config['width'];
			if (background){
				background.height = h;
				background.width = w;
			}
			if (result){
				var yHeight:Number = (h / 2) - 10;
				result.x = w / 2 - (result.width) / 2;
				result.y = yHeight;
			}
			if (displayInited){
				centerDisplay(h, w);
			}
		}
		
		
		private function changeHandler(event:Event):void {
			dayInput.dataProvider = new DataProvider(getDayArray(Number(monthInput.value)));
		}
		
		
		private function setFontColor(displayElement:UIComponent, fontColor:uint):void {
			var tf:TextFormat = new TextFormat();
			tf.color = fontColor;
			tf.align = "center";
			displayElement.setStyle("textFormat", tf);
		}
		
		
		private function setBorderColor(displayElement:TextField, bordercolor:uint):void {
			displayElement.border = true;
			displayElement.borderColor = bordercolor;
		}
		
		
		private function addDisplayElement(displayElement:Sprite, width:Number = 0, height:Number = 0, x:Number = 0, y:Number = 0):void {
			displayElement.width = width;
			displayElement.height = height;
			displayElement.x = x;
			displayElement.y = y;
			addChild(displayElement);
		}
		
		
		private function getDayArray(i:Number = 0):Array {
			var result:Array = new Array();
			if (i == 2) {
				result = getRange(0, 29);
			} else if (i == 4 || i == 6 || i == 9 || i == 11) {
				result = getRange(0, 30);
			} else {
				result = getRange(0, 31);
			}
			return result;
		}
		
		
		/** Make sure this plugin's movie clip is placed on top of all other movie clips in the player. **/
		private function placeSelfOnTop():void {
			if (view.config['version'] > "4.4") {
				view.skin.setChildIndex(this, view.skin.numChildren - 1);
			} else if (view.config['version'] > "4.3") {
				view.skin.setChildIndex(loaderInfo.loader, view.skin.numChildren - 1);
			}
		}
		
		
		private function getRange(start:Number, end:Number, factor:Number = 1):Array {
			var result:Array = new Array();
			var i:Number = start;
			while (i != end) {
				i = i + factor;
				result.push(i);
			}
			return result;
		}
				
		
		private function redirecturl(evt:MouseEvent):void {
			if (getConfig('redirecturl')) {
				var targetURL:String = String(getConfig('redirecturl'));
				navigateToURL(new URLRequest(targetURL), String(getConfig('redirecttarget')));
			}
		}
		
		
		private function fadeOut():void {
			hideInput();
			fadeOutBackground();
		}
		
		
		private function fadeOutBackground():void {
			var myTM:TransitionManager = new TransitionManager(this);
			myTM.startTransition({type: Fade, direction: Transition.OUT, duration: 3, easing: Strong.easeOut});
			myTM.addEventListener("allTransitionsOutDone", completeFadeOut);
		}
		
		
		private function completeFadeOut(evt:Event):void {
			visible = false;
			if (getConfig('autostart') == 'true'){
				view.sendEvent(ViewEvent.PLAY, true);
			}
		}
		
		
		private function isOldEnough():Boolean {
			var result:Boolean = false;
			var birthDate:Date = getBirthDate();
			if ((currentDate.fullYear - birthDate.getFullYear()) > Number(getConfig('maxage'))) {
				result = false;
				return result;
			} else if ((currentDate.fullYear - birthDate.getFullYear()) == Number(getConfig('maxage'))) {
				if (currentDate.month < birthDate.getMonth()) {
					result = false;
					return result;
				}
				if (currentDate.month == birthDate.getMonth()) {
					if (currentDate.date >= birthDate.getDate()) {
						result = false;
						return result;
					}
				}
			}
			if ((currentDate.fullYear - birthDate.getFullYear()) > Number(getConfig('minage'))) {
				result = true;
			} else if ((currentDate.fullYear - birthDate.getFullYear()) > (Number(getConfig('minage')) - 1)) {
				if (currentDate.month > birthDate.getMonth()) {
					result = true;
				} else if (currentDate.month >= birthDate.getMonth()) {
					if (currentDate.date >= birthDate.getDate()) {
						result = true;
					}
				}
			}
			return result;
		}
		
		private function getBirthDate():Date {
			var birthDate:Date = getStoredBirthdate();
			if (!birthDate){
				birthDate = new Date(Number(yearInput.textField.text), Number(monthInput.value) - 1, Number(dayInput.value));
				var cookie:SharedObject = SharedObject.getLocal("birthdate");
				cookie.data.birthdate = birthDate;
				cookie.flush();
			}
			return birthDate;
		}
		
		private function getStoredBirthdate():Date {
			var birthDate:Date;
			var cookie:SharedObject = SharedObject.getLocal("birthdate");
			if (cookie.data['birthdate']){
				birthDate = cookie.data['birthdate'] as Date;
			}
			return birthDate;
		}
		
		
		private function centerDisplay(h:Number, w:Number):void {
			monthInput.x = (w / 2) - 135;
			dayInput.x = monthInput.x + 110;
			yearInput.x = dayInput.x + 60;
			submitButton.x = yearInput.x + 60;
			fullscreenexit_message.x = w / 2 - (fullscreenexit_message.width) / 2;
			var yHeight:Number = (h / 2) - 10;
			monthInput.y = yHeight;
			dayInput.y = yHeight;
			yearInput.y = yHeight;
			submitButton.y = yHeight;
			fullscreenexit_message.y = yHeight + 25;
			if (result){
				result.x = w / 2 - (result.width) / 2;
				result.y = yHeight;
			}
		}
		
		private function avoidFullScreen(evt:Event):void {
			if (view.config['fullscreen']) {
				view.sendEvent(ViewEvent.FULLSCREEN, false);
				fullscreenexit_message.visible = true;
			}
		}
		
		private function getConfig(param:String):String {
			var result:String;
			if (view.playlist[view.config['item']][prefix+'.'+param]){
				result = view.playlist[view.config['item']][prefix+'.'+param];
			} else if (view.config[prefix+'.'+param]) {
				result = view.config[prefix+'.'+param];
			} else {
				result = defaults[param];
			}
			return result;
		}
	}
}