package {

    import com.longtailvideo.jwplayer.utils.Stretcher;

    import flash.display.*;
    import flash.events.*;
    import flash.filters.DropShadowFilter;
    import flash.net.*;
    import flash.text.*;


    /** This plugin renders a single related thumb. **/
    public class RelatedThumb extends Sprite {


        /** Embedding the image assets. **/
        [Embed(source="../../assets/glow.png")]
        private const GlowSheet:Class;


        /** Background for the entire thumb. **/
        private var _back:Sprite;
        /** The handler to report clicks to. **/
        private var _click:Function;
        /** The related item that was loaded. **/
        private var _item:Object;
        /** Container that imports the preview thumbnail **/
        private var _loader:Loader;
        /** Mask for the loader. **/
        private var _mask:Sprite;
        /** Graphic over the image. **/
        private var _glow:DisplayObject;
        /** Height of the thumb. **/
        private var _height:Number;
        /** Overlay between the image and text. **/
        private var _overlay:Sprite;
        /** Textfield that displays the title. **/
        private var _field:TextField;
        /** TextField formatting. **/
        private var _format:TextFormat;
        /** Width of the thumb. **/
        private var _width:Number;


        /** Constructor. **/
        public function RelatedThumb(width:Number,height:Number,item:Object,click:Function) {
            _width = width;
            _height = height;
            _item = item;
            _click = click;
            _back = new Sprite();
            _back.graphics.beginFill(0,1);
            _back.graphics.drawRect(0,0,width,height);
            _back.filters = new Array(new DropShadowFilter(0));
            addChild(_back);
            _loader = new Loader();
            _loader.contentLoaderInfo.addEventListener(Event.COMPLETE,_loaderHandler);
            _loader.load(new URLRequest(item.image));
            addChild(_loader);
            _mask = new Sprite();
            _mask.graphics.beginFill(0,1);
            _mask.graphics.drawRect(1,1,width-2,height-2);
            addChild(_mask);
            _loader.mask = _mask;
            _glow = new GlowSheet();
            _glow.x = 1;
            _glow.y = 1;
            _glow.width = width-2;
            _glow.scaleY = _glow.scaleX;
            addChild(_glow);
            _overlay = new Sprite();
            _overlay.graphics.beginFill(0,0.8);
            _overlay.graphics.drawRect(1,height-27,width-2,26);
            addChild(_overlay);
            _field = new TextField();
            _field.height = 20;
            _field.width = width-10;
            _field.x = 5;
            _field.y = height-22;
            _format = new TextFormat('Arial', 12, 0xFFFFFF);
            _format.align = 'center';
            _field.defaultTextFormat = _format;
            _field.selectable = false;
            _field.text = item.title;
            addChild(_field);
            buttonMode = true;
            mouseChildren = false;
            addEventListener(MouseEvent.CLICK,_clickHandler);
            addEventListener(MouseEvent.MOUSE_OUT,_outHandler);
            addEventListener(MouseEvent.MOUSE_OVER,_overHandler);
        };


        /** Redirect on thumb click. **/
        private function _clickHandler(event:MouseEvent):void {
            _click(_item);
        };


        /** Fade the image when loaded. **/
        private function _loaderHandler(event:Event):void {
            try {
                Bitmap(_loader.content).smoothing = true;
            } catch(e:Error) {}
            Stretcher.stretch(_loader,_width,_height,Stretcher.FILL);
        };


        /** Redirect on thumb click. **/
        private function _outHandler(event:MouseEvent):void {
            _back.graphics.clear();
            _back.graphics.beginFill(0,1);
            _back.graphics.drawRect(0,0,_width,_height);
            _back.filters = new Array(new DropShadowFilter(0));
        };


        /** Redirect on thumb click. **/
        private function _overHandler(event:MouseEvent):void {
            _back.graphics.clear();
            _back.graphics.beginFill(0xFFFFFF,1);
            _back.graphics.drawRect(0,0,_width,_height);
            _back.filters = new Array(new DropShadowFilter(0,45,0xFFFFFF));
        };


    }


}
