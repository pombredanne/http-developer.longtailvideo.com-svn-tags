package {

    import com.longtailvideo.jwplayer.events.*;
    import com.longtailvideo.jwplayer.player.*;
    import com.longtailvideo.jwplayer.plugins.*;
    import com.longtailvideo.jwplayer.utils.*;
    import com.longtailvideo.jwplayer.parsers.*;

    import flash.display.*;
    import flash.events.*;
    import flash.filters.DropShadowFilter;
    import flash.net.*;
    import flash.text.*;
    import flash.utils.setTimeout;


    /** This plugin displays an overlay with related videos. **/
    public class Related extends Sprite implements IPlugin {


        /** Embedding the image assets. **/
        [Embed(source="../../assets/icon.png")]
        private const DockIcon:Class;
        [Embed(source="../../assets/sheet.png")]
        private const BackSheet:Class;
        [Embed(source="../../assets/replay.png")]
        private const ReplayButton:Class;
        [Embed(source="../../assets/close.png")]
        private const CloseButton:Class;


        /** Reference to the background sheet. **/
        private var _back:Sprite;
        /** Reference to the dock button. **/
        private var _button:MovieClip;
        /** The plugin configuration options.**/
        private var _config:Object;
        /** Clip with all graphics. **/
        private var _container:MovieClip;
        /** Default dimensions for the grid. **/
        private var _dimensions:Array;
        /** Link to the mRSS file with related videos. **/
        private var _file:String;
        /** The grid with all thumbs. **/
        private var _grid:Sprite;
        /** The CTA text heading. **/
        private var _heading:TextField;
        /** Reference to the dock icon. **/
        private var _icon:DisplayObject;
        /** Component that loads the related videos. **/
        private var _loader:URLLoader;
        /** Component that parses the related videos. **/
        private var _parser:RSSParser;
        /** Reference to the player. **/
        private var _player:IPlayer;
        /** Reference to the replay button. **/
        private var _replay:Sprite;
        /** Reference to the close button. **/
        private var _close:Sprite;


        /** The background screen was clicked. **/
        private function _backHandler(evt:MouseEvent):void { hide(); };


        /** A thumbnail is clicked. **/
        private function _clickHandler(item:Object):void {
            if(_config.onclick == 'play') {
                _player.load(item);
                _player.play();
            } else {
                navigateToURL(new URLRequest(item.link),'_top');
            }
        };

        /** Display the related items on complete. **/
        private function _completeHandler(evt:MediaEvent):void {
            if(_config.oncomplete !== false) {
                setTimeout(show,50);
            }
        };


        /** The controlbar/dock button was clicked. **/
        private function _dockHandler(evt:MouseEvent):void { show(); };


        /** Loading the RSS feed failed. **/
        private function _errorHandler(evt:ErrorEvent):void {
            Logger.log(evt.text,id);
            _file = undefined;
            if(_icon) {
                _icon.alpha = 0.5;
                _button.field.alpha = 0.5;
                _button.field.text = 'not set';
            }
        };


        /** Hide the list with related videos. **/
        public function hide():void {
            _back.alpha = 1;
            new Animations(_container).fade(0,0.2);
            // Only 5.7+...
            try {
                (_player.controls.display as Object).show();
                (_player.controls.dock as Object).show();
            } catch (error:Error) {}
        };


        /** Returns the plugin name. **/
        public function get id():String {
            return "related";
        };


        /** Called by the player to initialize; setup events and dock buttons.  */
        public function initPlugin(player:IPlayer, config:PluginConfig):void {
            _player = player;
            _config = config;
            _player.addEventListener(MediaEvent.JWPLAYER_MEDIA_COMPLETE, _completeHandler);
            _player.addEventListener(PlaylistEvent.JWPLAYER_PLAYLIST_ITEM, _itemHandler);
            _loader = new URLLoader();
            _loader.addEventListener(Event.COMPLETE,_loaderHandler);
            _loader.addEventListener(SecurityErrorEvent.SECURITY_ERROR, _errorHandler);
            _loader.addEventListener(IOErrorEvent.IO_ERROR, _errorHandler);
            _parser = new RSSParser();
            if(_config.usedock !== false) {
                _icon = new DockIcon()
                _button = _player.controls.dock.addButton(_icon, "related", _dockHandler);
            }
            // Add the background.
            _container = new MovieClip();
            _container.alpha = 0;
            _container.visible = false;
            addChild(_container);
            _back = new Sprite();
            _back.buttonMode = true;
            _back.addChild(new BackSheet());
            _back.addEventListener(MouseEvent.CLICK,_backHandler);
            _container.addChild(_back);
            // Add the replay and close buttons.
            _replay = new Sprite();
            _replay.buttonMode = true;
            _replay.addChild(new ReplayButton());
            _replay.addEventListener(MouseEvent.CLICK,_replayHandler);
            _container.addChild(_replay);
            _close = new Sprite();
            _close.buttonMode = true;
            _close.addChild(new CloseButton());
            _close.addEventListener(MouseEvent.CLICK,_backHandler);
            _container.addChild(_close);
            // Add the text heading.
            _heading = new TextField();
            _heading.height = 30;
            _heading.defaultTextFormat = new TextFormat('Arial', 16, 0xFFFFFF);
            _heading.autoSize = 'center';
            _heading.multiline = false;
            _heading.selectable = false;
            _heading.filters = new Array(new DropShadowFilter(1,45,0,1,1,1,1));
            if(_config.heading !== undefined) {
                _heading.htmlText = _config.heading;
            } else {
                _heading.htmlText = "Watch related videos";
            }
            _container.addChild(_heading);
            // Add the grid for thumbs
            _grid = new Sprite();
            _container.addChild(_grid);
        };


        /** Find the related thumbs feed when an item loads.  */
        public function _itemHandler(evt:PlaylistEvent):void {
            // Reset old data
            _file = undefined;
            while(_grid.numChildren > 0) {
                _grid.removeChildAt(0);
            }
            hide();
            // Check for new file
            if(_player.playlist.currentItem['related.file']) {
                _file = _player.playlist.currentItem['related.file'];
            } else if (_config['file']) {
                _file = _config['file'];
            }
            // Load the mRSS feed and set the dock icon
            if(_file) {
                _loader.load(new URLRequest(_file));
                if(_icon) { 
                    _icon.alpha = 1;
                    _button.field.alpha = 1;
                    _button.field.text = 'related';
                }
            } else {
                _errorHandler(new ErrorEvent(ErrorEvent.ERROR,false,false,
                    "This playlist entry has no related videos."));
            }
        };


        /** Loader has loaded the mRSS feed. **/
        private function _loaderHandler(evt:Event):void {
            try {
                var xml:XML = XML(evt.target.data);
                var rss:Array = _parser.parse(xml);
            } catch (error:Error) { 
                _errorHandler(new ErrorEvent(ErrorEvent.ERROR,false,false,"This feed is not valid XML and/or RSS."));
                return;
            }
            var related:Array = new Array();
            for (var i:Number = 0; i < rss.length; i++) {
                if(rss[i].image && rss[i].title && (
                    (_config.onclick == 'play' && rss[i].file) || 
                    (_config.onclick != 'play' && rss[i].link))) {
                    related.push(rss[i]);
                }
            }
            if(related.length) {
                var col:Number = 0;
                var row:Number = 0;
                for(var j:Number = 0; j < related.length; j++) {
                    var thumb:RelatedThumb = new RelatedThumb(
                        _dimensions[0],
                        _dimensions[1],
                        related[j],
                        _clickHandler
                    );
                    thumb.x = (_dimensions[0]+10) * col;
                    thumb.y = (_dimensions[1]+10) * row;
                    _grid.addChild(thumb);
                    if((_dimensions[0]+10)*(col+2) > _dimensions[2]) {
                        if((_dimensions[1]+10)*(row+2) > _dimensions[3]-80) {
                            break;
                        } else {
                            row++;
                            col = 0;
                        }
                    } else {
                        col++;
                    }
                }
                resize(_dimensions[2],_dimensions[3]);
            } else {
                _errorHandler(new ErrorEvent(ErrorEvent.ERROR,false,false,
                    "RSS feed has 0 entries that contain title,link and image."));
            }
        };


        /** The replay button was clicked. **/
        private function _replayHandler(evt:MouseEvent):void {
            hide();
            _player.seek(0);
        };


        /** Reposition the screens when the player resizes itself **/
        public function resize(width:Number, height:Number):void {
            // Do regular resize stuff
            _back.width = width;
            _back.height = height;
            _close.x = width - 50;
            _grid.x = Math.round(width/2 - _grid.width/2);
            _grid.y = Math.round(height/2 - _grid.height/2) + 15;
            _heading.y = _grid.y - 30;
            _heading.x = Math.round(width/2 - _heading.width/2);
            // Store thumb dimensions on first resize.
            _dimensions = [140,80,width,height];
            if(_config.dimensions) {
                var dim:Array = _config.dimensions.split('x');
                for(var i:Number=0; i<2; i++) {
                    _dimensions[i] = Number(dim[i]);
                }
            }
        };


        /** Show the list with related videos. **/
        public function show():void {
            if(_file) {
                new Animations(_container).fade(1,0.2);
                _player.pause();
                // Only 5.7+...
                try {
                    (_player.controls.display as Object).hide();
                    (_player.controls.dock as Object).hide();
                } catch (error:Error) {}
            }
        };


    }
}
