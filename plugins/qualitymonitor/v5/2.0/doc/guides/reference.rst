Introduction
============

The Qualitymonitor plugin for the JW Player is used to monitor the status of the bitrate switching / dynamic streaming functionalty of the player. This functionality is triggered by loading multiple quality levels of one video into the player. It will then monitor certain quality metrics and play the  optimal stream is served for every viewer.

For more info about this functionality, please refer to the dedicated sections in the HTTP resp. RTMP streaming guides:

*  `HTTP bitrate switching <http://www.longtailvideo.com/support/jw-player/jw-player-for-flash-v5/12534/video-delivery-http-pseudo-streaming#bitrateswitching>`_ 
* `RTMP dynamic streaming <http://www.longtailvideo.com/support/jw-player/jw-player-for-flash-v5/12535/video-delivery-rtmp-streaming#dynamicstreaming>`_ functionality of the player.

Screenshot
==========

Here's a screenshot of a JW Player with the qualitymonitor plugin:

 .. image:: ../assets/qualitymonitor.png
    :alt: Skinning examples.
    

The plugin consists of two sections. The biggest one is the overlay at the top of the video that lists and charts the current quality metrics. Additionally, the plugin contains a small messaging area in the middle of the screen, for printing player messages that relate to the video quality.


Metrics
=======

The Qualitymonitor plugin monitors the following metrics:

* The **bandwidth** of the current connection, in kilobits per second (green).
* The number of **frames dropped** per second (red).
* The **width** of the video screen (blue).
* The currently playing **quality level** (white). The required width and bandwidth of this level is shown between parentheses.

Messages
--------

In addition to these four metrics, the plugin displays two quality level related messages in the middle of the screen:

* The completion of a smooth transition for `RTMP dynamic streaming <http://www.longtailvideo.com/support/jw-player/jw-player-for-flash-v5/12535/video-delivery-rtmp-streaming#dynamicstreaming>`_ (in green text).
* The blacklisting and un-blacklisting of a quality level as a result of too many framedrops (in red text).

.. note::

  The framedrop detection and resulting (un)blacklisting of levels is only supported in JW Player 5.3+. For JW Player 5.0, 5.1 and 5.2, the framedrop chart will simply remain at 0.


Configuration Options
=====================

Embedding the qualitymonitor plugin is a matter of setting amending its name to the **plugins** player option. The  plugin contains no configuration options itself.

Here is a basic embed code example of a player using the qualitymonitor plugin. This example uses the SWFObject 2.0 embedding script:

.. code-block:: html

   <p id="container">The player will be placed here</p>
   
   <script type="text/javascript">
     swfobject.embedSWF("player.swf","container","480","270","9.0.115","false",{
       file: "/static/dynamic_stream.xml",
       plugins: "qualitymonitor"
     });
   </script>



Skinning
========

This plugin cannot be skinned.



Changelog
=========

Version 2.0
-----------

* Switched from the 4.x to the 5.x plugin API. As of now, the plugin cannot be loaded in the JW Player v4.
* Added support for monitoring framedrops in the chart, and level (un)blacklisting events in the center message.
* Colored the various quality metrics in the chart, to easily distinct between them.
* Made the chart stop drawing when the video is paused or stopped.



