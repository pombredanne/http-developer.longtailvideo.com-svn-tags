package com.jeroenwijering.plugins {


import com.jeroenwijering.events.*;
import com.jeroenwijering.utils.Sparkline;

import flash.display.*;
import flash.events.*;
import flash.geom.Rectangle;
import flash.text.*;
import flash.utils.*;


/**
* Show bitrate switching events.
**/
public class QualityMonitor extends MovieClip implements PluginInterface {


	/** Reference to the background clip. **/
	private var back:Sprite;
	/** Configuration variables. **/
	public var config:Object ={};
	/** Reference to the textfield. **/
	private var field:TextField;
	/** Reference to the checking interval. **/
	private var interval:Number;
	/** Reference to the view of the player. **/
	private var view:AbstractView;
	/** Reference to the bandwidth sparkline. **/
	private var lines:Array;


	/** Constructor. **/
	public function QualityMonitor():void {
		mouseEnabled = false;
		mouseChildren = false;
		back = new Sprite();
		back.graphics.beginFill(0x000000,0.8);
		back.graphics.drawRect(0,0,400,90);
		addChild(back);
		field = new TextField();
		field.width = 180;
		var fmt:TextFormat = new TextFormat("_sans",11,0xFFFFFF);
		fmt.leading = 10;
		field.defaultTextFormat = fmt;
		field.multiline = true;
		field.y = 10;
		field.x = 15;
		addChild(field);
		lines = new Array(
			addLine(3000,6),
			addLine(6,33),
			addLine(2000,58)
		);
	};


	/** Add sparklines to the plugin. **/
	private function addLine(max:Number,yps:Number):Sparkline { 
		var lne:Sparkline = new Sparkline(200,20,0,max);
		lne.x = 190;
		lne.y = yps;
		addChild(lne);
		return lne;
	};


	/** Update quality metrics **/
	private function check():void {
		var lvl:String = '1 of 1';
		if(view.playlist[view.config['item']] && view.playlist[view.config['item']]['levels']) {
			var arr:Array = view.playlist[view.config['item']]['levels'];
			var idx:Number = view.config['level'];
			lvl = (idx+1) + ' of ' + arr.length;
			lvl += ' (' + arr[idx]['bitrate'] + 'kbps, ' + arr[idx]['width'] + 'px)';
			field.htmlText = 
				'<b>bandwidth:</b> ' + view.config['bandwidth'] + ' kbps<br/>' +
				'<b>level:</b> ' + lvl + '<br/>' +
				'<b>width:</b> '+ view.config['width'] + ' pixels';
			lines[0].spark(view.config['bandwidth']);
			lines[1].spark(arr.length - view.config['level']);
			lines[2].spark(view.config['width']);
		} else { 
			field.htmlText = "Only one bitrate found.<br/>" +
				"You need a bitrate switching feed<br/>" +
				"and a 4.6+ / 5.1+ player.";
		} 
	};


	/** The initialize call is invoked by the player View. **/
	public function initializePlugin(vie:AbstractView):void {
		view = vie;
		view.addControllerListener(ControllerEvent.RESIZE,resizeHandler);
		interval = setInterval(check,100);
	};


	/** Handle a resize. **/
	private function resizeHandler(evt:ControllerEvent):void {
		x = config['x'];
		y = config['y'];
		back.width = config['width'];
		field.width = config['width']/2-10;
		lines[0].resize(config['width'] - 200);
		lines[1].resize(config['width'] - 200);
		lines[2].resize(config['width'] - 200);
	};


}


}