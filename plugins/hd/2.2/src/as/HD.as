package {


    import com.longtailvideo.jwplayer.events.*;
    import com.longtailvideo.jwplayer.model.*;
    import com.longtailvideo.jwplayer.player.*;
    import com.longtailvideo.jwplayer.plugins.*;
    import com.longtailvideo.jwplayer.utils.*;
    
    import flash.display.DisplayObject;
    import flash.display.MovieClip;
    import flash.events.MouseEvent;


    /** HD Plugin; implements an HD toggle. **/
    public class HD extends MovieClip implements IPlugin {


        [Embed(source="../../assets/controlbar.png")]
        private const ControlbarIcon:Class;
        [Embed(source="../../assets/dock.png")]
        private const DockIcon:Class;


        /** Reference to the dock/controlbar button. **/
        private var _button:MovieClip;
        /** Reference to the plugin's configuration **/
        private var _config:PluginConfig;
        /** Reference to dock/controlbar icon. **/
        private var _icon:DisplayObject;
        /** Is HD set for this playlistitem. **/
        private var _isset:Boolean;
        /** Item whose startposition to reset. **/
        private var _reset:Number;
        /** Reference to the player. **/
        private var _player:IPlayer;
        /** The current position inside a video. **/
        private var _position:Number;


        /** Constructor; nothing going on. **/
        public function HD():void {}


        /** Controlbar/dock button is clicked, so toggle the HD state. **/
        private function _clickHandler(event:MouseEvent):void {
            if(_isset) { 
                _config.state = !_config.state;
                Configger.saveCookie('hd.state',_config.state);
                _swap();
            }
        };


        /** Reset any start position on complete. **/
        private function _completeHandler(event:MediaEvent):void { 
            if(_reset > -1) {
                _player.playlist.getItemAt(_reset).start = 0;
                _reset = -1;
            }
        };


        /** Identifier of the plugin. **/
        public function get id():String {
            return 'hd';
        };


        /** The initialize call is invoked by the player. **/
        public function initPlugin(player:IPlayer, config:PluginConfig):void {
            _player = player;
            _config = config;
            // Add event listeners.
            _player.addEventListener(MediaEvent.JWPLAYER_MEDIA_COMPLETE,_completeHandler);
            _player.addEventListener(PlaylistEvent.JWPLAYER_PLAYLIST_ITEM, _itemHandler);
            _player.addEventListener(MediaEvent.JWPLAYER_MEDIA_META,_metaHandler);
            _player.addEventListener(PlaylistEvent.JWPLAYER_PLAYLIST_LOADED, _playlistHandler);
            _player.addEventListener(MediaEvent.JWPLAYER_MEDIA_TIME, _timeHandler);
            // Draw controlbar/dock button
            if (_player.config.dock) {
                _icon = new DockIcon();
                _button = _player.controls.dock.addButton(_icon, 'is on', _clickHandler);
            } else {
                _icon = new ControlbarIcon();
                _player.controls.controlbar.addButton(_icon, 'hd', _clickHandler);
            }
            // Set initial UI state.
            if(_config.state != true) { _config.state = false; }
            _redraw();
        };


        /** Updates the button for availability of item. **/
        private function _itemHandler(event:PlaylistEvent):void {
            if(_reset > -1) {
                _player.playlist.getItemAt(_reset).start = 0;
                _reset = -1;
            }
            var item:PlaylistItem = _player.playlist.getItemAt(_player.playlist.currentIndex);
            if(item['hd.file'] && !item['ova.hidden']) {
                _isset = true;
            } else {
                _isset = false;
            }
            _redraw();
        };


        /** Updates the playlist with either the HD or original video. **/
        private function _playlistHandler(event:PlaylistEvent):void {
            // Reset the reset.
            _reset = -1;
            // Which item should we apply the global 'hd.file' to?
            // We should skip ova.hidden items.
            var firstVisible:Number = 0;
            // Store all the original files into the playlist.
            for(var i:Number = 0; i < _player.playlist.length; i++) {
                var item:PlaylistItem = _player.playlist.getItemAt(i);
                if (item['ova.hidden']) {
                    // This item is hidden by OVA; we shouldn't set its hd.file property. 
                    firstVisible++;
                }
                // Set the hd.file flashvar to the first playlistitem.
                if(i == firstVisible && _config.file) {
                    item['hd.file'] = _config.file;
                }
                if(item.provider == 'youtube') {
                    item['youtube.quality'] = 'medium';
                } else {
                    item['hd.original'] = item.file;
                }
            }
            // Do the HD swap if applicable.
            if(_config.state) {
                _swap();
            }
        };


        /** Check metadata for youtube quality levels. **/
        private function _metaHandler(event:MediaEvent):void { 
            if(event.metadata.youtubequalitylevels && 
                event.metadata.youtubequalitylevels.indexOf('hd720') > -1) { 
                _isset = true;
                _redraw();
            }
        };


        /** Set the HD button state. **/
        private function _redraw():void {
            if(!_player.playlist.length || !_isset) {
                if (_button) {
                    _button.visible = false;
                } else { 
                    _icon.alpha = 0.1;
                }
            } else if (!_config.state) {
                if (_button) {
                    _button.field.text = 'is off';
                    _button.visible = true;
                } else {
                    _icon.alpha = 0.3;
                }
            } else {
                if (_button) {
                    _button.visible = true;
                    _button.field.text = 'is on';
                } else {
                    _icon.alpha = 1;
                }
            }
        };


        /** Enable HD if the fullscreen option is set. **/
        public function resize(width:Number, height:Number):void {
            if(_isset && _config.fullscreen && _player.config.fullscreen && !_config.state) {
                _config.state = true;
                _swap();
            }
        };


        /** Switch the currently playing file with a new one. **/
        private function _swap():void {
            // Swap HD state in every playlist item.
            for(var i:Number = 0; i<_player.playlist.length; i++) {
                var item:PlaylistItem = _player.playlist.getItemAt(i);
                if (!item['ova.hidden']) {
                    if (item.provider == 'youtube') {
                        if(_config.state) {
                            item['youtube.quality'] = 'hd720';
                        } else {
                            item['youtube.quality'] = 'medium';
                        }
                    } else if(item['hd.file']) {
                        if(_config.state) {
                            item.file = item['hd.file'];
                        } else {
                            item.file = item['hd.original'];
                        }
                    }
                }
            }
            // Restart player if not IDLE.
            if(_player.state != PlayerState.IDLE) {
                _reset = _player.playlist.currentIndex;
                _player.playlist.getItemAt(_reset).start = _position;
                _player.stop();
                _player.play();
            }
            _redraw();
        };


        /** Save the position inside a video. **/
        private function _timeHandler(event:MediaEvent):void {
            _position = event.position;
        };


    }


}