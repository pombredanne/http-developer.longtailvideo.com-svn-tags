\select@language {english}
\contentsline {chapter}{\numberline {1}Introduction}{1}{chapter.1}
\contentsline {chapter}{\numberline {2}Examples}{3}{chapter.2}
\contentsline {section}{\numberline {2.1}Basic Setup}{3}{section.2.1}
\contentsline {section}{\numberline {2.2}RTMP Stream with Fullscreen Toggle}{4}{section.2.2}
\contentsline {section}{\numberline {2.3}Playlist with HD Disabled by Default}{4}{section.2.3}
\contentsline {chapter}{\numberline {3}Configuration}{7}{chapter.3}
\contentsline {section}{\numberline {3.1}Using Playlists}{7}{section.3.1}
\contentsline {section}{\numberline {3.2}Skinning}{8}{section.3.2}
\contentsline {subsection}{\numberline {3.2.1}Example XML}{8}{subsection.3.2.1}
\contentsline {subsection}{\numberline {3.2.2}Configuration Options}{8}{subsection.3.2.2}
