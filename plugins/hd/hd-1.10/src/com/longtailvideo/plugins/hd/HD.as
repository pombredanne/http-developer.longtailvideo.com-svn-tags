package com.longtailvideo.plugins.hd {


    import com.longtailvideo.jwplayer.events.MediaEvent;
    import com.longtailvideo.jwplayer.events.PlayerStateEvent;
    import com.longtailvideo.jwplayer.events.PlaylistEvent;
    import com.longtailvideo.jwplayer.model.IPlaylist;
    import com.longtailvideo.jwplayer.model.PlaylistItem;
    import com.longtailvideo.jwplayer.player.IPlayer;
    import com.longtailvideo.jwplayer.player.PlayerState;
    import com.longtailvideo.jwplayer.plugins.IPlugin;
    import com.longtailvideo.jwplayer.plugins.PluginConfig;
    import com.longtailvideo.jwplayer.utils.Configger;

    import flash.display.DisplayObject;
    import flash.display.MovieClip;
    import flash.events.MouseEvent;


    /** HD Plugin; implements an HD toggle. **/
    public class HD extends MovieClip implements IPlugin {


        [Embed(source="../../../../../assets/controlbar.png")]
        private const ControlbarIcon:Class;
        [Embed(source="../../../../../assets/dock.png")]
        private const DockIcon:Class;


        /** Reference to the dock/controlbar button. **/
        private var _button:MovieClip;
        /** Reference to the plugin's configuration **/
        private var _config:PluginConfig;
        /** Reference to dock/controlbar icon. **/
        private var _icon:DisplayObject;
        /** Is HD set for this playlistitem. **/
        private var _isset:Boolean;
        /** Item whose startposition to reset. **/
        private var _reset:Number;
        /** Reference to the player. **/
        private var _player:IPlayer;
        /** The current position inside a video. **/
        private var _position:Number;


        /** Constructor; nothing going on. **/
        public function HD():void {}


        /** Controlbar/dock button is clicked, so toggle the HD state. **/
        private function _clickHandler(event:MouseEvent):void {
            if(_isset) { 
                _config.state = !_config.state;
                Configger.saveCookie('hd.state',_config.state);
                _swap();
            }
        };


        /** Reset any start position on complete. **/
        private function _completeHandler(event:MediaEvent):void { 
            if(_reset > -1) {
                _player.playlist.getItemAt(_reset).start = 0;
                _reset = -1;
            }
        };


        /** Identifier of the plugin. **/
        public function get id():String {
            return 'hd';
        };


        /** The initialize call is invoked by the player. **/
        public function initPlugin(player:IPlayer, config:PluginConfig):void {
            _player = player;
            _config = config;
            // Add event listeners.
            _player.addEventListener(MediaEvent.JWPLAYER_MEDIA_COMPLETE,_completeHandler);
            _player.addEventListener(PlaylistEvent.JWPLAYER_PLAYLIST_ITEM, _itemHandler);
            _player.addEventListener(MediaEvent.JWPLAYER_MEDIA_META,_metaHandler);
            _player.addEventListener(PlaylistEvent.JWPLAYER_PLAYLIST_LOADED, _playlistHandler);
            _player.addEventListener(MediaEvent.JWPLAYER_MEDIA_TIME, _timeHandler);
            // Draw controlbar/dock button
            if (_player.config.dock) {
                _icon = _player.skin.getSkinElement('hd', 'dockIcon');
                if(!_icon) { _icon = new DockIcon(); }
                _button = _player.controls.dock.addButton(_icon, 'is on', _clickHandler);
            } else {
                _icon = _player.skin.getSkinElement('hd', 'controlbarIcon');
                if(!_icon) { _icon = new ControlbarIcon(); }
                _player.controls.controlbar.addButton(_icon, 'hd', _clickHandler);
            }
            // Set initial UI state.
            if(_config.state != true) { _config.state = false; }
            _redraw();
        };


        /** Updates the button for availability of item. **/
        private function _itemHandler(event:PlaylistEvent):void {
            if(_reset > -1) {
                _player.playlist.getItemAt(_reset).start = 0;
                _reset = -1;
            }
            var item:PlaylistItem = _player.playlist.getItemAt(_player.playlist.currentIndex);
            if(!(item['ova.hidden']) && (item.provider == 'youtube' || item['hd.file'])) {
                _isset = true;
            } else {
                _isset = false;
            }
            _redraw();
        };


        /** Updates the playlist with either the HD or original video. **/
        private function _playlistHandler(event:PlaylistEvent):void {
            // Reset the reset.
            _reset = -1;
            // Set the file flashvar to the first playlistitem.
            if(_config.file) {
                _player.playlist.getItemAt(0)['hd.file'] = _config.file;
            }
            // Store all the original files into the playlist.
            for(var i:Number = 0; i < _player.playlist.length; i++) {
                var item:PlaylistItem = _player.playlist.getItemAt(i);
                if(item.provider == 'youtube') {
                    item['youtube.quality'] = 'medium';
                } else {
                    item['hd.original'] = item.file;
                }
            }
            // Do the HD swap if applicable.
            if(_config.state) {
                _swap();
            }
        };


        /** Check metadata for youtube quality levels. **/
        private function _metaHandler(event:MediaEvent):void { 
            if(event.metadata.youtubequalitylevels && 
                event.metadata.youtubequalitylevels.indexOf('hd720') == -1) { 
                _isset = false;
                _redraw();
            }
        };


        /** Set the HD button state. **/
        private function _redraw():void {
            if(!_player.playlist.length || !_isset) {
                _icon.alpha = 0.5;
                if (_button) { 
                    _button.field.alpha = 0.5;
                    _button.field.text = 'not set';
                }
            } else if (!_config.state) {
                if (_button) {
                    _button.field.text = 'is off';
                    _button.field.alpha = 1;
                    _icon.alpha = 1;
                } else {
                    _icon.alpha = 0.25;
                }
            } else {
                _icon.alpha = 1;
                if (_button) {
                    _button.field.alpha = 1;
                    _button.field.text = 'is on';
                }
            }
        };


        /** Enable HD if the fullscreen option is set. **/
        public function resize(width:Number, height:Number):void {
            if(_isset && _config.fullscreen && _player.config.fullscreen && !_config.state) {
                _config.state = true;
                _swap();
            }
        };


        /** Switch the currently playing file with a new one. **/
        private function _swap():void {
            // Swap HD state in every playlist item.
            for(var i:Number = 0; i<_player.playlist.length; i++) {
                var item:PlaylistItem = _player.playlist.getItemAt(i);
                 if (item.provider == 'youtube') {
                    if(_config.state) {
                        item['youtube.quality'] = 'hd720';
                    } else {
                        item['youtube.quality'] = 'medium';
                    }
                    
                } else if(item['hd.file']) {
                    if(_config.state) {
                        item.file = item['hd.file'];
                    } else {
                        item.file = item['hd.original'];
                    }
                }
            }
            // Restart player if not IDLE.
            if(_player.state != PlayerState.IDLE) {
                _reset = _player.playlist.currentIndex;
                _player.playlist.getItemAt(_reset).start = _position;
                _player.stop();
                _player.play();
            }
            _redraw();
        };


        /** Save the position inside a video. **/
        private function _timeHandler(event:MediaEvent):void {
            _position = event.position;
        };


    }


}