# This is a simple script that compiles the plugin using MXMLC (free & cross-platform).
# To use, make sure you have downloaded and installed the Flex SDK in the following directory:
FLEXPATH=/Developer/SDKs/flex_sdk_3


echo "Compiling with MXMLC..."
$FLEXPATH/bin/mxmlc ../src/com/longtailvideo/plugins/hd/HD.as -sp ../src -o ../bin-release/hd.swf -library-path+=../libs -use-network=false -optimize=true -incremental=false
