(function(jwplayer) {


/**
* Displays an HD/SD quality toggle.
**/
var template = function(_player, _options, _div) {


    /** Button for selecting HD on startup. **/
    var _buttonHD;
    /** Button for selecting SD on startup. **/
    var _buttonSD;
    /** Shared style for buttons. **/
    var _buttonStyle = {
        border: 'none',
        cursor: 'pointer',
        position: 'absolute',
        margin: '0 0 0 0',
        padding: '0 0 0 0',
        height: '80px',
        width: '120px'
    };
    /** Icon to display when HD is not available. **/
    var _iconNotset = '../assets/not_set.png';
    /** Icon to display when HD is on. **/
    var _iconOn = '../assets/is_on.png';
    /** Icon to display when HD is off. **/
    var _iconOff = '../assets/is_off.png';
    /** Currently active playlist item. **/
    var _item;
    /** Player just reloaded playlist. **/
    var _loaded;
    /** The current position in the video. **/
    var _position;
    /** Current HD state. **/
    var _state;


    /** For touch devices, hide buttons when buffering starts. **/
    function _bufferHandler(event) {
        _style(_div,{display:'none'});
        try { _player.getPlugin("display").show(); } catch (e) { /* Only 5.7+ */ }
    };


    /** Play the HD quality video. **/
    function _buttonHDHandler() {
        _startHD(true);
    };


    /** Play the SD quality video. **/
    function _buttonSDHandler() {
        _startSD(true);
    };


    /** Reset position when video completed. **/
    function _completeHandler() {
        _position = 0;
    };


    /** Copy the playlist, stripping out any non-string values. **/
    function _copyList(list,hd) {
        var copy = [];
        // Clean the playlist, since complex data is sometimes stored.
        for (var i=0; i<list.length; i++) {
            var entry = {};
            for(var key in list[i]) {
                if(typeof(list[i][key]) == 'string' || typeof(list[i][key]) == 'number') {
                    entry[key] = list[i][key];
                }
            }
            // Switch SD > HD or vice versa
            if(hd) {
                entry['hd.original'] = list[i].file;
                if(list[i]['hd.file']) {
                    entry['file'] = list[i]['hd.file'];
                }
            } else { 
                entry['file'] = list[i]['hd.original'];
            }
            copy.push(entry);
        }
        return copy;
    };


    /** Toggle HD setup. **/
    var dockHandler = function() {
        if(_player.getPlaylistItem(_item)['hd.file']) {
            if(_state) {
                _startSD(true);
            } else {
                _startHD(true);
            }
        }
    };


    /** Check if fullscreen option is used. **/
    function _fullscreenHandler(event) {
        if(event.fullscreen && _options.fullscreen && !_state) {
            _startHD(true);
        }
    };


    /** For touch devices, show elements when player is idle. **/
    function _idleHandler(event) {
        if(_player.getPlaylist().length == 1) {
            _style(_div,{display:'block'});
            try { 
                _player.getPlugin("display").hide(); 
                _player.getPlugin("controlbar").hide();
            } catch (e) { /* Only 5.7+ */ }
        }
    };


    /** Check if the device is touch or pointer. **/
    function _isTouch() {
        try {
            document.createEvent("TouchEvent");
            return true;
        } catch (e) {
            return false;
        }
    };


    /** Check if the new playlist item has an HD version. **/
    function _itemHandler(event) {
        _item = event.index;
        if(_player.getPlaylistItem(_item)['hd.file']) {
            if(_state) {
                _player.getPlugin("dock").setButton('hd',dockHandler,_iconOn);
            } else { 
                _player.getPlugin("dock").setButton('hd',dockHandler,_iconOff);
            }
        } else {
            _player.getPlugin("dock").setButton('hd',dockHandler,_iconNotset);
        }
    };


    /** Restart video if playlist loaded b/c of switch. **/
    function _playlistHandler(event) {
        if(_loaded) {
            if(_player.getPlaylist().length == 1) {
                _player.seek(_position);
            } else {
                _player.playlistItem(_item);
            }
            _loaded = false;
        }
    };


    /** Set dock buttons when player is ready. **/
    function _readyHandler() {
        if(_player.getRenderingMode() == 'flash') { return; }
        // Inject HD file when available.
        if(_options.file && !_player.getPlaylist()[0]['hd.file']) {
            _player.getPlaylist()[0]['hd.file'] = _options.file;
        }
        // Render large selector for touch devices and dock icon for mouse devices.
        if(_isTouch()) {
            _buttonSD = document.createElement("div");
            _buttonSD.onclick = _buttonSDHandler;
            _div.appendChild(_buttonSD);
            _buttonHD = document.createElement("div");
            _buttonHD.onclick = _buttonHDHandler;
            _div.appendChild(_buttonHD);
            try { 
                _player.getPlugin("display").hide();
                _player.getPlugin("controlbar").hide();
            } catch (e) { /* Only 5.7+ */ }
            _player.onBuffer(_bufferHandler);
            _player.onIdle(_idleHandler);
        } else {
            if(document.cookie.indexOf('jwplayerhdstate=true') > -1) {
                _startHD(false);
            } else  {
                _startSD(false);
            }
            _player.onPlaylistItem(_itemHandler);
        }
        // Subscribe to events.
        _player.onPlaylist(_playlistHandler);
        _player.onTime(_timeHandler);
        _player.onComplete(_completeHandler);
        _player.onFullscreen(_fullscreenHandler);
    };
    _player.onReady(_readyHandler);


    /** Reposition buttons upon a resize. **/
    this.resize = function(wid,hei) {
        if(_player.getRenderingMode() == 'flash') { return; }
        if(_isTouch()) {
            _style(_div,{
                position: 'relative'
            });
            _style(_buttonSD,_buttonStyle);
            _style(_buttonSD,{
                left: Math.floor(wid/2 - 130) + 'px',
                top: Math.floor(hei/2-40)+'px',
                background:'url(../assets/play_low.png)'
            });
            _style(_buttonHD,_buttonStyle);
            _style(_buttonHD,{
                left: Math.floor(wid/2 + 10) + 'px',
                top: Math.floor(hei/2-40)+'px',
                background:'url(../assets/play_high.png)'
            });
        }
    };


    /** Set cookie to store HD state. **/
    function _setCookie(state) {
        var cookie = 'jwplayerhdstate=' + state + '; ';
        cookie += 'expires=Wed, 1 Jan 2020 00:00:00 UTC; ';
        cookie += 'path=/';
        document.cookie = cookie;
    };


    /** Reload the playlist in HD mode. **/
    function _startHD(play) {
        if(!_isTouch()) {
            _player.getPlugin("dock").setButton('hd',dockHandler,_iconOn);
        }
        if(!_state) {
            var list = _copyList(_player.getPlaylist(), true);
            _state = true;
            if(play) { _loaded = true; }
            _player.load(list);
            _setCookie(true);
        } else if (play) {
            _player.play(true);
        }
    };


    /** Reload the playlist in SD mode. **/
    function _startSD(play) {
        if(!_isTouch()) {
            _player.getPlugin("dock").setButton('hd',dockHandler,_iconOff);
        }
        if(_state) {
            var list = _copyList(_player.getPlaylist(), false);
            _state = false;
            if(play) { _loaded = true; }
            _player.load(list);
            _setCookie(false);
        } else if (play) {
            _player.play(true);
        }
    };


    /** Apply CSS styles to elements. **/
    function _style(element,styles) {
        for(var property in styles) {
          element.style[property] = styles[property];
        }
     };


    /** Store the last position, for quick recovery. **/
    function _timeHandler(event) {
        _position = event.position;
    };


};


/** Register the plugin with JW Player. **/
jwplayer().registerPlugin('hd', template,'./hd.swf');


})(jwplayer);