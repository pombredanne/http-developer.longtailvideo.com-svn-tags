var settings = {
	/** Players to list. **/
	players: {
		'5.3':'players/5.3.swf',
		'5.2':'players/5.2.swf',
		'5.1':'players/5.1.swf',
		'5.0':'players/5.0.swf'
	},
	/** Plugins to list. **/
	plugins: {
		captions:'../captions.swf'
	},
	/** Skins to list. **/
	skins: {
		none:'',
		bekle:'skins/bekle.zip',
		glow:'skins/glow.zip',
		modieus:'skins/modieus.zip',
		stijl:'skins/stijl.zip',
	},
	/** All the setup examples with their flashvars. **/
	examples: {
		'':{},
		'TT plain captions': {
			plugins:'captions',
			file:'../assets/corrie.flv',
			image:'assets/corrie.jpg',
			'captions.file':'assets/plain.xml'
		},
		'SRT plain captions': {
			dock:'false',
			plugins:'captions',
			file:'../assets/corrie.flv',
			image:'assets/corrie.jpg',
			'captions.file':'assets/plain.srt'
		},
		'TT captions with styling': {
			plugins:'captions',
			file:'../assets/corrie.flv',
			image:'assets/corrie.jpg',
			'captions.file':'assets/styled.xml'
		},
		'SRT captions with styling': {
			plugins:'captions',
			file:'../assets/corrie.flv',
			image:'assets/corrie.jpg',
			'captions.file':'assets/styled.srt'
		},
		'MP4 timedtext captions': {
			plugins:'captions',
			file:'../assets/bunny.mp4',
			image:'assets/bunny.jpg'
		},
		' ':{},
		'Playlist with captions': {
			dock:false,
			file:'assets/playlist.xml',
			height:240,
			playlist:'right',
			playlistsize:240,
			plugins:'captions',
			width:720
		},
		'Multibitrate stream with captions': {
			plugins:'captions',
			file:'http://content.bitsontherun.com/jwp/nPripu9l.xml',
			captions:'assets/bunny.srt'
		}
	}
}
