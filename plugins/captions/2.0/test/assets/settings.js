var tests = {
	'load test ...':{},
	'TT captions with button in the dock': {
		dock:'true',
		plugins:'captions',
		file:'../../testing/files/corrie.flv',
		'captions.file':'files/corrie.xml'
	},
	'SRT captions with button in the controlbar': {
		dock:'false',
		plugins:'captions',
		file:'../../testing/files/corrie.flv',
		'captions.file':'files/corrie.srt'
	},
	'TT captions with styling': {
		plugins:'captions',
		file:'../../testing/files/bunny.flv',
		'captions.file':'files/captions.xml'
	},
	'SRT captions with styling': {
		plugins:'captions',
		file:'../../testing/files/bunny.flv',
		'captions.file':'files/captions.srt'
	},
	'Caption selector in the dock': {
		plugins:'captions',
		dock:'true',
		file:'../../testing/files/bunny.flv',
		'captions.file':'english:files/captions.srt,deutsch:files/captions.srt,nederlands:files/corrie.srt'
	},
	'Caption selector in the controlbar': {
		plugins:'captions',
		dock:'false',
		file:'../../testing/files/bunny.flv',
		'captions.file':'language 1:files/captions.srt,language 2:files/captions.srt,another one:files/corrie.srt'
	},
	'Javascript callback (console.log)': {
		plugins:'captions',
		file:'../../testing/files/corrie.flv',
		'captions.file':'files/corrie.xml',
		'captions.callback':'console.log'
	},
};
