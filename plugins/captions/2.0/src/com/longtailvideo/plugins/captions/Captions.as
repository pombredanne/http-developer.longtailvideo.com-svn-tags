package com.longtailvideo.plugins.captions {

	import com.longtailvideo.jwplayer.events.*;
	import com.longtailvideo.jwplayer.player.IPlayer;
	import com.longtailvideo.jwplayer.player.PlayerState;
	import com.longtailvideo.jwplayer.plugins.*;
	import com.longtailvideo.jwplayer.utils.Logger;
	import com.longtailvideo.jwplayer.view.interfaces.IControlbarComponent;
	import com.longtailvideo.jwplayer.view.interfaces.IDockComponent;

	import flash.display.*;
	import flash.events.*;
	import flash.filters.DropShadowFilter;
	import flash.net.*;
	import flash.text.*;


	/**
	* Plugin for playing closed captions with a video.
	**/
	public class Captions extends MovieClip implements IPlugin {


	[Embed(source="../../../../../assets/controlbar.png")]
	private const ControlbarButton:Class;
	[Embed(source="../../../../../assets/dock.png")]
	private const DockIcon:Class;


	/** Reference to the background graphic. **/
	private var _back:MovieClip;
	/* Reference to the bar (container for field & back)*/
	private var _bar:MovieClip;
	/** Reference to the controlbar button. **/
	private var _button:MovieClip;
	/** The array the captions are loaded into. **/
	private var _captions:Array;
	/** List with configuration settings. **/
	private var _config:Object = {
		back:false,
		file:undefined,
		fontsize:14,
		state:true
	};
	/** Currently active caption. **/
	private var _current:Number;
	/** Reference to the textfield. **/
	public var _field:TextField;
	/** Textformat entry for the captions. **/
	private var _format:TextFormat;
	/** Reference to the dock icon. **/
	private var _icon:MovieClip;
	/** XML connect and parse object. **/
	private var _loader:URLLoader;
	/* Reference to the JW Player. */
	private var _player:IPlayer;
	/** A map of known stylesheets. **/
	private var _stylesheets:Object;


	/** Constructor; solely inits the captions file loader. **/
	public function Captions() {
		_loader = new URLLoader();
		_loader.addEventListener(Event.COMPLETE,loaderHandler);
		_loader.addEventListener(IOErrorEvent.IO_ERROR,errorHandler);
		_loader.addEventListener(SecurityErrorEvent.SECURITY_ERROR,errorHandler);
	};


	/** Clicking the controlbar icon or dock button. **/
	private function clickHandler(evt:MouseEvent):void {
		setState(!_config['state']);
	};


	/** Draw the neccessary graphics to put the captions on stage. **/
	private function drawClip():void {
		_bar = new MovieClip();
		_bar.mouseChildren = false;
		_bar.mouseEnabled = false;
		addChild(_bar);
		_back = new MovieClip();
		_back.graphics.beginFill(0x0000,0.75);
		_back.graphics.drawRect(0,0,400,20);
		_bar.addChild(_back);
		_field = new TextField();
		_field.width = 400;
		_field.height = 10;
		_field.y = 5;
		_field.autoSize = "center";
		_field.multiline = true;
		_field.wordWrap = true;
		var fmt:TextFormat = new TextFormat();
		fmt.color = 0xFFFFFF;
		fmt.size = _config.fontsize;
		fmt.align = "center";
		fmt.font = "_sans";
		fmt.leading = 4;
		_field.defaultTextFormat = fmt;
		_bar.addChild(_field);
		if (!_config['back']) {
			_back.alpha = 0;
			_field.filters = new Array(new DropShadowFilter(0,45,0,1,2,2,10,3));
		}
	};


	/** The captions loader returns errors (file not found or security error. **/
	private function errorHandler(evt:ErrorEvent):void { 
		setCaption('<font color="#FF0000" size="-2">' + evt.text + '</font>');
	};


	/** Identifier string of this plugin in the JW Player. **/
	public function get id():String {
		return "captions";
	};


	/** Initialize as a JW Player 5 plugin. */
	public function initPlugin(ply:IPlayer, cfg:PluginConfig):void {
		_player = ply;
		for (var prp:String in _config) {
			if (cfg[prp] != undefined) { _config[prp] = cfg[prp]; }
		}
		_player.addEventListener(PlayerStateEvent.JWPLAYER_PLAYER_STATE,stateHandler);
		_player.addEventListener(PlaylistEvent.JWPLAYER_PLAYLIST_ITEM,itemHandler);
		_player.addEventListener(MediaEvent.JWPLAYER_MEDIA_TIME,timeHandler);
		_player.addEventListener(MediaEvent.JWPLAYER_MEDIA_META,metaHandler);
		if (_player.config.dock) {
			var icn:DisplayObject = _player.skin.getSkinElement("captions", "dockIcon");
			if (icn == null) { icn = new DockIcon(); }
			_icon = _player.controls.dock.addButton(icn,'is on',clickHandler);
		} else {
			var btn:DisplayObject = _player.skin.getSkinElement("captions", "controlbarButton");
			if (btn == null) { btn = new ControlbarButton; }
			_button = _player.controls.controlbar.addButton(btn,'captions',clickHandler);
		}
		drawClip();
		setState(_config['state']);
	};


	/** Check for captions with a new playlistItem. **/
	private function itemHandler(evt:PlaylistEvent):void {
		_current = 0;
		_captions = new Array();
		setCaption('');
		var fil:String = '';
		if (_player.playlist.currentItem['captions.file']) {
			fil = _player.playlist.currentItem['captions.file'];
		} else if (_player.playlist.currentItem['captions']) {
			fil = _player.playlist.currentItem['captions'];
		} else if (_config['file']) {
			fil = _config['file'];
		} else if (_player.config['captions']) {
			fil = _player.config['captions'];
		}
		if(fil != '') {
			_loader.load(new URLRequest(fil));
		}
	};


	/** Captions are loaded; now display them. **/
	private function loaderHandler(evt:Event):void {
		try {
			if(XML(evt.target.data).localName().toString().toLowerCase() == 'tt') {
				_captions = TTParser.parseCaptions(XML(evt.target.data));
				_stylesheets = TTParser.parseStylesheets(XML(evt.target.data));
			} else {
				_captions = SRTParser.parseCaptions(String(evt.target.data));
			}
		} catch (err:Error) { 
				_captions = SRTParser.parseCaptions(String(evt.target.data));
		}
		if (_captions.length < 2) {
			setCaption('<font color="#FF0000">No captions found.<br/>Probably not a valid SRT or DFXP file.</font>');
		}
	};


	/** Check for captions in metadata. **/
	private function metaHandler(evt:MediaEvent):void {
		var txt:String;
		var fnd:Boolean = false;
		if (evt.metadata.type == 'caption') {
			txt = evt.metadata.captions;
			fnd = true;
		} else if (evt.metadata.type == 'textdata') {
			txt = evt.metadata.text;
			fnd = true;
		}
		if (fnd == true) {
			setCaption(txt);
		}
	};


	/** Resize this plugin */
	public function resize(w:Number, h:Number):void {
		_back.height = _field.height + 10;
		_bar.width = w;
		_bar.scaleY = _bar.scaleX;
		if (_player.config.fullscreen || _player.config.controlbar == 'over') {
			_bar.y = h - _bar.height - 50;
		} else {
			_bar.y = h - _bar.height;
		}
		_bar.visible = _config['state'];
	};


	/** Set the current caption field text. */
	private function setCaption(text:String, style:String=null):void {
		var txt:String = TTParser.parseStyle(_stylesheets, text)
		if (txt) { text = txt; }
		if (style && _stylesheets[style]) {
			_field.styleSheet = _stylesheets[style]
			_field.htmlText = "<p>" + text + "</p>" ;
		} else {
			_field.htmlText = text;
		}
		if(_config['back'] && text != '') {
			_back.alpha = 1;
		} else {
			_back.alpha = 0;
		}
		resize(_player.config['width'], _player.config['height']);
	};


	/** Show/hide the captions, update the button, save state in cookie. **/
	public function setState(stt:Boolean):void {
		_config['state'] = stt;
		_bar.visible = stt;
		if (stt) {
			if (_icon) {
				_icon.field.text = "is on";
			} else {
				_button.alpha = 1;
			}
		} else {
			if (_icon) {
				_icon.field.text = "is off";
			} else {
				_button.alpha = 0.3;
			}
		}
		var cke:SharedObject = SharedObject.getLocal('com.jeroenwijering','/');
		cke.data['captions.state'] = stt;
		cke.flush();
	};


	/** Captions are only visible when playing / paused. **/
	private function stateHandler(evt:PlayerStateEvent):void {
		if (_config['state'] && (
			evt.newstate == PlayerState.PLAYING || 
			evt.newstate == PlayerState.PAUSED ||  
			evt.newstate == PlayerState.BUFFERING)) {
			_bar.visible = true;
		} else {
			setCaption('');
			_bar.visible = false;
		}
	};


	/** Check timing of the player to sync captions. **/
	private function timeHandler(evt:MediaEvent):void {
		var pos:Number = evt.position;
		if (_config['state'] == true && _captions.length && 
			(_captions[_current]['begin'] > pos || (_captions[_current+1] && _captions[_current+1]['begin'] < pos))) {
			updateCaption(pos);
		}
	};


	/** Set a caption on screen. **/
	private function updateCaption(pos:Number):void {
		for (var i:Number=0; i<_captions.length; i++) {
			if (_captions[i]['begin'] < pos && (i == _captions.length - 1 || _captions[i+1]['begin'] > pos)) {
				_current = i;
				setCaption(_captions[i]['text'], _captions[i]['style']);
				return;
			}
		}
	};


};


}