﻿package com.longtailvideo.plugins.captions {


import com.longtailvideo.jwplayer.utils.Strings;
import flash.text.StyleSheet;


public class TTParser {


	/** Parse captions from the TT XML, returning a list with {begin:Number,text:String} objects. **/
	public static function parseCaptions(dat:XML):Array {
		var arr:Array = new Array({begin:0,text:''});
		var styles:Object = {};
		for each (var i:XML in dat.children()) {
			if (i.localName() == "head") {
				for each (var styling:XML in i.children()) {
					for each (var node:XML in styling.children()) {
						if (node.localName() == 'style') {
							var st:Object = {};
							for each (var a:XML in node.attributes()) {
								var nm:String = a.name();
								st[nm] = node.attribute(nm);
							}
							if (st.id != null && st.id != undefined) {
								styles[st.id] = st;
							}
						}
					}
				}
			} else if (i.localName() == "body") {
				for each (var j:XML in i.children()) {
					for each (var k:XML in j.children()) {
						if (k.localName() == 'p') {
							var obj:Object = TTParser.parseCaption(k);
							arr.push(obj);
							if (obj['end']) {
								arr.push({begin:obj['end'],text:''});
								delete obj['end'];
							} else if (obj['dur']) {
								arr.push({begin:obj['begin']+obj['dur'],text:''});
								delete obj['dur'];
							}
						}
					}
				}
			}
		}
		return arr;
	};


	/** Parse a single captions entry. **/
	private static function parseCaption(dat:XML):Object {
		var ptn:RegExp = /(\n<br.*>\n)+/;
		var obj:Object = {
			begin:Strings.seconds(dat.@begin),
			dur:Strings.seconds(dat.@dur),
			end:Strings.seconds(dat.@end),
			style:dat.@style,
			text:dat.children().toString().replace(ptn,'\n').replace(/\n</,' <').replace(/>\n/, '> ')
		};
		return obj;
	};


	/** Parse stylesheets from the styling block. */
	public static function parseStylesheets(dat:XML):Object {
		var styles:Object = {};
		for each (var i:XML in dat.children()) {
			if (i.localName() == "head") {
				for each (var styling:XML in i.children()) {
					for each (var node:XML in styling.children()) {
						if (node.localName() == 'style') {
							var st:Object = {};
							for each (var a:XML in node.attributes()) {
								var val:String = a;
								var nm:String = a.name();
								if (nm.indexOf("::") > -1) {
									nm = nm.substring(nm.indexOf("::")+2);
								}
								st[nm] = val;
							}
							if (st.id != null && st.id != undefined) {
								try {
									if (st.style) {
										for each (var subid:String in st.style.split(" ")) {
											var ops:Object = styles[subid].getStyle("p");
											for (var opp:String in ops) {
												if (!st[opp]) st[opp] = ops[opp];
											}
										}
									}
									var ss:StyleSheet = new StyleSheet();
									ss.setStyle("p", st);
									styles[st.id] = ss;
								} catch (E:Error) {
								}
							}
						}
					}
				}
			}
		}
		return styles;
	}


	/** Attempt to parse a caption entry into HTML */
	public static function parseStyle(styles:Object, text:String):String {
		var html:String = "";
		if (text) {
			html = rewriteStyle(text);
		}
		return html;
	};


	private static function rewriteStyle(text:String):String {
		var out:String = text;
		if (text) {
			// supporting single un-nested spans only
			while (text.indexOf("<span") > -1) {
				var newText:String = replaceSpan(text);
				if (newText == text) break;
				text = newText;
			}
			out = text;
		}
		return out;
	};


	private static function replaceSpan(txt:String):String {
		var replaced:String = txt;
		// As dictated by Adobe supported HTML tags in TextFields
		// see http://livedocs.adobe.com/flash/9.0/ActionScriptLangRefV3/flash/text/TextField.html
		var font_mappings:Object = {
			"::color":"color",
			"::fontFamily":"face",
			"::fontSize":"size"
		};
		var tag_map:Object = {
			"::fontWeight":"b",
			"::fontStyle":"i",
			"::textDecoration":"u"
		};
		if (txt) {
			var left:int = txt.indexOf("<span ");
			var right:int = txt.indexOf("</span>", left);
			if (left > -1 && right > -1) {
				var span:XML = new XML(txt.substring(left,right+7));
				var styles:Object =	{};
				var tags:Array = [];
				for each (var a:XML in span.@*) {
					var nm:String = a.name();
					var val:String = a;
					var tnm:String = nm.substring(nm.indexOf("::"));
					if (tag_map[tnm]) {
						var tg:String = tag_map[tnm]
						tags.push(tg);
					}

					if (font_mappings[tnm]) {
						var FONT:String = "font";
						if (tags.indexOf(FONT) < 0) tags.push(FONT);
						if (!styles[FONT]) styles[FONT] = {};
						styles[FONT][font_mappings[tnm]] = val;
					}
				}
				replaced = span;
				for each (var tag:String in tags) {
					var newStyles:String = "";
					if (styles[tag]) {
						for (var sp:String in styles[tag]) {
							newStyles += " " + sp + "='" + styles[tag][sp] + "'";
						}
					}
					replaced = "<" + tag + newStyles + ">" + replaced + "</" + tag + ">";
				}
				replaced = txt.substring(0, left) + replaced + txt.substring(right+7);
			}
		}
		return replaced;
	};


}


}
