Introduction
============

The Captions plugin for JW Player supports the display of a closed captions or foreign language subtitle track with audio or video files. More creative use cases are karaoke, or the displaying of timed comments or footnotes with the video.

Captions are read from an external file in either the W3C recommended DFXP (TimedText) XML format or in the SubRip Text plain-text format. Alternatively, the Captions plugin can display captions embedded into MP4 files.

The plugin supports playlists and can be skinned using the JW Player PNG skinning model.



.. _options:

Configuration Options
=====================

Like the `JW Player overall <http://www.longtailvideo.com/support/jw-player/jw-player-for-flash-v5/12536/configuration-options>`_, the Captions plugin can be configured with options set in the embed code. The following configuration options (flashvars) are available:

.. describe:: captions.back

   When set to **true**, a semitransparant black background is drawn below the captions. This background makes the captions more readeable (nice for small texts), but does set the captions more apart from the video.
   
   By default, this black background is not shown. Instead, the player renders a thin black outline around the captions, similar to TV / DVD captions.

.. describe:: captions.file

   Location of the captions file(s) to display. Should be the URL to a valid :ref:`DFXP <dfxp>` or :ref:`SRT <srt>` captions file. 
   
   If your captions are embedded in your MP4 videos, or if you use a playlist, this option is not needed. The Captions plugin will automatically load and display captions found in either the video metadata or playlist contents.
   
   .. note::
   
      If your captions file(s) are located at another webserver than your JW Player (*player.swf*), you need to place a **crossdomain.xml** file in the root of that webserver. Without a *crossdomain.xml*, the Flash plugin will refuse to load the captions for security reasons. More info can be found `in this reference <http://www.longtailvideo.com/support/jw-player/jw-player-for-flash-v5/12541/crossdomain-file-loading-restrictions>`_.

.. describe:: captions.fontsize

   The base size of the captions text in pixels. Is **14** by default. This fits about 80 characters per line, just like with TV / DVD. The fontsize can be overridden with this option, but also with styling rules embedded in a  :ref:`DFXP <dfxp>` or :ref:`SRT <srt>` captions file.

.. describe:: captions.state

   Describes whether to show the captions on startup or not. The default is **true** (captions are shown).
   
   .. note::
   
      The *state* option is also saved in a cookie. When a user disables the captions on one video, they will be disabled by default on subsequent videos he watches on the same site. The configuration option overrides the cookie.


Dock Option
-----------

One option for `the player itself <http://www.longtailvideo.com/support/jw-player/jw-player-for-flash-v5/12536/configuration-options>`_ greatly influences the interface of the Captions plugin. This option is called **dock** and can be set to either *true* (the default) or *false*.

* When set *true*, the button to show/hide captions is displayed in the *dock*, an area in the top right of the display.
* When set *false* the button to show/hide captions is displayed in the controlbar, usually at the right side, before the mute button (this can be set in the skin).

Dock buttons are larger than controlbar buttons, which allows for clearer graphics to display. Dock buttons automatically fade out after a few seconds of playback.

Examples
--------

Here is an example embed code of a player using the captions plugin. This example uses the `SWFObject 2.0 <http://code.google.com/p/swfobject/>`_ embedding script:

.. code-block:: html

   <p id="container">The player will be placed here</p>
   
   <script type="text/javascript">
     swfobject.embedSWF("player.swf","container","480","270","9.0.115","false",{
       file: "/static/video.mp4",
       plugins: "captions",
      "captions.file": "/static/captions.xml"
     });
   </script>

Here is a second embed code, in which the *dock* is disabled (showing a controlbar button) and the *captions.back* is enabled (showing the black background):

.. code-block:: html

   <p id="container">The player will be placed here</p>
   
   <script type="text/javascript">
     swfobject.embedSWF("player.swf","container","480","270","9.0.115","false",{
       file: "/static/video.mp4",
       plugins: "captions",
      "captions.back": true,
      "captions.file": "/static/captions.xml",
      dock: false
     });
   </script>

Here's a screenshot of both examples:

.. image:: ../assets/examples.png
	:alt: Captions example.

.. note::

   The dock button will automatically fade away after 3 seconds of playback.




Supported Formats
=================

The plugin supports two distinct text formats for loading the captions. Additionally, the plugin can display texttracks embedded in MP4 files.

A great, easy to use tool for creating subtitles is `Subtitle Horse <http://www.subtitle-horse.com/>`_. It's an online editor that allows you to *live* insert captions into your video. The captions can be exported to both SRT and DFXP. It is also possible to edit already existing SRT or DFXP files ánd the tool can be downloaded, e.g. for inclusion in your own CMS.


.. _dfxp:

W3C TimedText (DFXP)
--------------------

The `W3C TimedText format <http://www.w3.org/TR/2010/PR-ttaf1-dfxp-20100914/>`_ is an XML format for storing closed captions or subtitles. Another name for this format is DFXP (Distribution Format Exchange Profile). Here is an example:

.. code-block:: xml

   <tt xmlns="http://www.w3.org/2006/10/ttaf1">
     <body>
       <div>
         <p begin="00:00:08" end="00:00:10">- Nothing is going on.</p>
         <p begin="00:00:10.5" end="00:00:12.5">You liar!</p>
         <p begin="00:00:13.5" end="00:00:15">Are you?</p>
         <p begin="00:00:17" end="00:00:20">Violet, please!<br/>- I am not your babe!</p>
         <p begin="00:00:34" end="00:00:36">Vi, please.<br/>- Leave me alone!</p>
       </div>
     </body>
   </tt>

The *<br/>* tags inside the paragraphs identify linebreaks in the captions. With the default fontsize, about 80 characters fit on a single line. This is similar to TV / DVD standards.

Though the format supports multiple languages (by using multiple *<div lang='xx'>* elements), only one language/div is supported by the Captions plugin for now.

.. note::

   Your DFXP files should use UTF8 encoding in order to correctly display special characters (accents, but also Chinese or Hebrew).

Styling
^^^^^^^

TimedText files can contain directions for styling the captions. These can be inserted in two ways, both supported by the Captions plugin:

* The *<tt>* element can contain a *<head>* tag with multiple style elements. These styles are all given an ID. The individual pragraphs in the *<body>* can be linked to the style rules using the *<p style="xx">* attribute.
* Each paragraph can contain *<span tts:xxx="yyy">* elements, to only style certain text snippets.

The following style rules are supported by the Captions plugin:

* **color**, e.g. *<span tts:color="#ffcc00">hello</span>*.
* **fontFamily**, e.g. *<span tts:fontFamily="Arial, sans-serif">hello</span>*.
* **fontSize**, e.g. *<span tts:fontSize="20">hello</span>*.
* **fontStyle**, e.g. *<span tts:fontStyle="italic">hello</span>*.
* **fontWeight**, e.g. *<span tts:fontWeight="bold">hello</span>*.
* **textDecoration**, e.g. *<span tts:textDecoration="underline">hello</span>*.

Here is the example DFXP file again, this time with styling rules in both the head and in inline spans. Don't forget the additional namespace declaration for the *tts* elements:

.. code-block:: xml

   <tt xmlns="http://www.w3.org/2006/10/ttaf1" xmlns:tts="http://www.w3.org/2006/04/ttaf1#styling">
     <head>
      <styling>
         <style id="normal" tts:fontSize="15" />
         <style id="yellow" tts:fontSize="15" tts:color="#FFFF00" />
         <style id="bigred" tts:color="#FF0000" tts:textDecoration="underline" tts:fontSize="20" />
      </styling>
     </head>
     <body>
       <div>
         <p begin="00:00:08" end="00:00:10" style="normal">- Nothing is going on.</p>
         <p begin="00:00:10.5" end="00:00:12.5" style="normal">
            You <span tts:fontSize="+10">liar</span>!
         </p>
         <p begin="00:00:13.5" end="00:00:15" style="yellow">Are you?</p>
         <p begin="00:00:17" end="00:00:20" style="bigred">
            Violet, please!<br/>- I am <span tts:fontSize="30">not</span> your babe!
         </p>
         <p begin="00:00:24" end="00:00:29" style="normal">
            You <span tts:fontFamily="Times, serif">stupid cow</span>,<br/>
            look what you gone and done now, ay.
         </p>
       </div>
     </body>
   </tt>

.. note::

   The captions plugin does not support either a *<span>* in *<span>* or a *<br/>* in *<span>* construction inside paragraphs. This would make the parsing of texts needlessly complex.

.. note::

   Fontsizes can be set both with absolute (*<font size="15">*) and with relative (*<font size="+5">*) values.

.. _srt:

SubRip (SRT)
------------

The `SubRip captions format <http://en.wikipedia.org/wiki/SubRip>`_ uses plain text files. Here's an example again:

.. code-block:: text

   1
   00:00:08,000 --> 00:00:10,000
   Nothing is going on.
   
   2
   00:00:10,500 --> 00:00:12,500
   Violet, please!
   - I am not your babe!
   
   3
   00:00:17,000 --> 00:00:20,000
   You stupid cow,
   look what you gone and done now, ay.

The formatting is easy to read, but also easy to break, especially in the timecode lines. Make sure to use the correct delimiters.

A double linebreak with enumeration is used to distinct between caption entries. Single linebreaks can be used to add breaks in the captions themselves. We recommend you stick to about 80 characters per line. This looks good with a default setup of the captions plugin. It is also the standard for TV / DVD setups.

.. note:: 

   Your SRT files should use UTF8 encoding in order to correctly display special characters (accents, but also Chinese or Hebrew).

Styling
^^^^^^^

The SRT format does not support any styling by itself. The Captions plugin does support styling though, by using a few basic HTML tags. In effect, the same set of styles as with :ref:`DFXP <dfxp>` is supported:

* *<b>* (bold), *<i>* (italic) and *<u>* (underline)
* *<font color="#ff0000" face="Courier, monospace" size="+2">* (font style)

Here is the same SRT example, this time with a number of styling tags:

.. code-block:: text

   1
   00:00:08,000 --> 00:00:10,000
   <b>Nothing</b> is going on.
   
   2
   00:00:10,500 --> 00:00:12,500
   Violet, <i>please</i>!
   - I am <font face="Courier, monospace" size="+10" color="#FF0000">not</font> your babe!

   3
   00:00:17,000 --> 00:00:20,000
   <font color="#00FF00">You <i><u>stupid cow</u></i>,
   <b>look</b> what you gone and done now, ay.</font>

.. note::

   Fontsizes can be set both with absolute (*<font size="15">*) and with relative (*<font size="+5">*) values.



MPEG-4 Timed Text
-----------------

The MP4 media container has the ability to `embed timed text tracks <http://en.wikipedia.org/wiki/MPEG-4_Part_17>`_, in addition to e.g. a video and an audio track. This text data, often referred to as MPEG-4 Timed Text, is automatically picked up and displayed by the Captions plugin.

The Captions plugin ignores the *trackid* of a Timed Text track. In other words, the plugin only supports a single text track, since all incoming textdata is simply printed in the captions area.

Since MPEG-4 Timed Text is a plain-text format, no styling options are supported (except the *captions.fontsize* and *captions.back* :ref:`configuration options <options>`).

.. note::

   There is little support for displaying MPEG-4 Timed Text across media players. Another player supporting this is the Quicktime player built into Apple's iOS (iPod/iPhone/iPad). 

Handbrake
^^^^^^^^^

A great tool for embedding captions in MP4 files is `Handbrake <http://handbrake.fr/>`_. This free, cross-platform encoding tool can:

* Embed captions from external SRT files into your transcodes.
* Migrate captions from DVDs it rips into your transcodes.

.. note:: 

   Handbrake's `documentation <http://trac.handbrake.fr/wiki/Subtitles>`_ uses the term *subtitles* instead of *captions*.




Playlist support
================

Captions can be assigned to one or more videos in a playlist. You can mix SRT, TT and MPEG-4 captions with videos that have no captions in a single feed. Since none of the XML playlist formats define elements for linking to captions files, the captions should be set using the `JWPlayer XML namespace <http://www.longtailvideo.com/support/jw-player/jw-player-for-flash-v5/12537/xml-playlist-support>`_. In practice, the namespace is enabled by:

* Setting an **xmlns:jwplayer** attribute in the main XML tag.
* Prefixing the XML elements with **jwplayer:**, e.g. *<jwplayer:sharing.code>*.

Example
-------

Here is an example embed code for captions with a playlist, using the `SWFObject 2.0 <http://code.google.com/p/swfobject/>`_ embedding script. Note the *captions.file* option is not set:

.. code-block:: html

   <p id="container">The player will be placed here</p>
   
   <script type="text/javascript">
     swfobject.embedSWF("player.swf","container","480","270","9.0.115","false",{
       file: "/static/playlist.xml",
       plugins: "captions"
     });
   </script>

And here is how the *playlist.xml* from this example can look like. The *captions.file* option is now set for each entry:

.. code-block:: xml

   <rss version="2.0" xmlns:jwplayer="http://developer.longtailvideo.com/">
     <channel>
       <title>Example RSS playlist with captions</title>
   
       <item>
         <title>Coronation Street</title>
         <description>This entry has external DFXP captions</description>
         <enclosure url="/static/corrie.flv" />
         <jwplayer:captions.file>/static/corrie.xml</jwplayer:captions.file>
       </item>
   
       <item>
         <title>Big Buck Bunny</title>
         <description>This entry has external SRT captions.</description>
         <enclosure url="/static/bunny.mp4" />
         <jwplayer:captions.file>/static/bunny.srt</jwplayer:captions.file>
       </item>
   
     </channel>
   </rss>

.. note::
   
   The same `crossdomain file restrictions  <http://www.longtailvideo.com/support/jw-player/jw-player-for-flash-v5/12541/crossdomain-file-loading-restrictions>`_ that apply to captions also apply to playlists. Therefore make sure you have placed a *crossdomain.xml* on the webserver that hosts your captions and playlist, if they are different from the *player.swf*'s webserver.




Skinning
========

The Captions plugin includes support for styling its controlbar or dock button through the JW Player `PNG Skinning Model <http://www.longtailvideo.com/support/jw-player/jw-player-for-flash-v5/14/skinning-the-jw-player-5>`_. Styling of the captions themselves must be included in the DFXP or SRT captions file.

Due to current limitations in the PNG skinning model, not all desired results can be achieved:

* Rollovers cannot be set, for neither the *dock* nor the *controlbar* button.
* Alternative icons (for the **off** state) cannot be set, for neither the *dock* nor the *controlbar* button.
* Contrary to regular controlbar buttons, the captions controlbar button is given a margin of 5px to its left and to its right. You should take this into account, removing any margins you have in the button graphics.

Here's an example of a skin that includes custom Captions graphics:

.. image:: ../assets/stijlskin.png
	:alt: Skinning example.

This skin, called **Stijl**, can be `freely downloaded here <http://www.longtailvideo.com/addons/skins/25/Stijl>`_.

XML Block
---------

Here is the XML code block you should include in your PNG skin to style the Captions plugin:

.. code-block:: html

   ...
   <component name="captions">
     <elements>
       <element name="controlbarButton" src="controlbar.png" />
       <element name="dockIcon" src="dock.png" />
     </elements>
   </component>

The two PNG images (*controlbar.png* and *dock.png*) should be placed in a **captions** subdirectory of the main skin directory.




Changelog
=========

An annoying known bug is the fact the Captions area is *stealing* mouse focus from the video screen. The plugin contains the neccessary *.mouseChildren=false* and *.mouseEnabled=false* settings, but the mouse doesn't pass nontheless. Suggestions are welcome!

Version 2.0
-----------

* Migrated plugin to V5 API; new Captions.as implements IPlugin and works natively with JW 5+. As of 2.0, the plugin doesn't work with the 4.x player anymore.
* Added support for styling using TimedText Styles (color, fontFamily, fontSize, fontWeight, fontStyle, fontWeight). Styles can be defined both inline and in the *<head>* of a TimedText file.
* Added besic support for PNG skinning, by allowing a custom controlbar and dock icon.
* Added support for error handling. 404 not founds, crossdomain security errors and file parsing errors are caught.
* Added support for controlbars placed over the display, by moving the captions further up.
* Fixed an issue that left the captions 'back' element visible as a thin bar when there where no captions.
* Fixed an issue where captions loaded though the configuration options were ignored when using a single-track playlist.