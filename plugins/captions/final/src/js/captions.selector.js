(function(jwplayer) {


    /** Component that renders a selection menu. **/
    jwplayer.captions.selector = function(_callback,_div) {

        /** Currently active item. **/
        var _active;
        /** Reference to the container div. **/
        var _container;
        /** Reference to the close button. **/
        var _close;
        /** Dimensions of the display. **/
        var _dimensions;
        /** Reference to the track listing. **/
        var _list;
        /** The list with options. **/
        var _options = [];


        /** Set backgrounds on a menu entry. **/
        function _background(index,over) {
            var backgrounds = [];
            if(index > 0) {
                backgrounds.push('url(../assets/divider.png) no-repeat 0 0');
            }
            if(index == _active) {
                backgrounds.push('url(../assets/active.png) no-repeat 20px 0');
            }
            if(over) {
                backgrounds.push('url(../assets/back.png) no-repeat 0 1px');
            }
            _style(_options[index].button,{
                background: backgrounds.join(',')
            });
        };


        /** One of the buttons was clicked. **/
        function _clickHandler(event) {
            var old = _active;
            var index = _index(event);
            _active = index;
            _background(old);
            _background(index);
            _callback(index);
        };


        /** Close button or container are clicked. **/
        function _closeHandler(event) {
            if(event.target == _container || event.target == _close) { 
                _callback(_active);
            }
        };


        /** Hide the language selector. **/
        this.hide = function() {
            _container.style.opacity = 0;
            setTimeout(function(){
                _container.style.display = 'none';
            },200);
        };


        /** Retrieve the index for a button. **/
        function _index(event) { 
            for(var i=0; i<_options.length; i++) {
                if(event.target == _options[i].button) { 
                    return i;
                }
            }
        };


        /** Swap background on mouse over. **/
        function _overHandler(event) {
            _background(_index(event),true);
        };


        /** Restore background on mouse out. **/
        function _outHandler(event) {
            _background(_index(event),false);
        };


        /** Polulate the selector with a list of options. **/
        this.populate = function(options, active) {
            _options = options;
            _active = active;
            _list.innerHTML = '';
            for(var i=0; i<options.length; i++) { 
                var button = document.createElement('li');
                button.innerHTML = options[i].label;
                _options[i].button = button;
                _list.appendChild(button);
                _style(button,{
                    color: '#FFF',
                    cursor: 'pointer',
                    display:'block',
                    font: '12px/25px Arial,sans-serif',
                    fontWeight: 'bold',
                    height: '25px',
                    textShadow: '1px 1px 0 #000',
                    textAlign: 'center',
                    width: '180px'
                });
                button.onclick = _clickHandler;
                button.onmouseover = _overHandler;
                button.onmouseout = _outHandler;
                _background(i);
            }
            _resize();
        };


        /** Resize the selector to fit the display. **/
        this.resize = function(width, height) {
            _dimensions = [width,height];
            _resize();
        };


        /** Resize the selector. **/
        function _resize() {
            _style(_container,{
                height: _dimensions[1]+'px',
                width: _dimensions[0]+'px'
            });
            _style(_close,{
                left: (_dimensions[0]-50)+'px'
            });
            _style(_list,{
                left: Math.round(_dimensions[0]/2 - 90)+'px',
                top: Math.round(_dimensions[1]/2 - _options.length*12.5)+'px'
            });
        };


        /** Draw the initial selector graphics. **/
        function _setup() {
            _container = document.createElement('div');
            _container.onclick = _closeHandler;
            _div.appendChild(_container);
            _style(_container,{
                backgroundImage: 'url(../assets/sheet.png)',
                backgroundRepeat: 'no-repeat',
                backgroundSize: '100% 100%',
                cursor: 'pointer',
                display: 'none',
                opacity: 0,
                position: 'relative',
                webkitTransition: 'opacity 150ms linear',
                MozTransition: 'opacity 150ms linear',
                oTransition: 'opacity 150ms linear',
                transition: 'opacity 150ms linear'
            });
            _close = document.createElement("div");
            _close.onclick = _closeHandler;
            _container.appendChild(_close);
            _style(_close,{
                background: 'transparent url(../assets/close.png)',
                cursor: 'pointer',
                position: 'absolute',
                left: '0px',
                top: '0px',
                margin: '0px',
                padding: '0px',
                display: 'block',
                width: '50px',
                height: '50px'
            });
            _list = document.createElement("ul");
            _container.appendChild(_list);
            _style(_list,{
                display: 'block',
                height: 'auto',
                listStyle: 'none',
                margin: '0px',
                padding: '0px',
                position: 'absolute',
                width: '180px'
            });
        };
        _setup();


        /** Show the language selector. **/
        this.show = function() {
            _container.style.display = 'block';
            setTimeout(function(){
                _container.style.opacity =  1;
            },20);
        };


        /** Apply CSS styles to elements. **/
        function _style(element,styles) {
            for(var property in styles) {
              element.style[property] = styles[property];
            }
        };


    };


})(jwplayer);