:: This is a simple script that compiles the plugin using the free Flex SDK on Windows.
:: Learn more at http://developer.longtailvideo.com/trac/wiki/PluginsCompiling

SET FLEXPATH="C:\Program Files\Adobe\Adobe Flash Builder Beta 2\sdks\3.4.1"

echo "Compiling player 5 plugin..."

%FLEXPATH%\bin\mxmlc ..\src\as\com\longtailvideo\plugins\captions\Captions.as -sp ..\src\as -o ..\captions.swf -library-path+=..\lib -load-externs=..\lib\jwplayer-5-classes.xml -use-network=false