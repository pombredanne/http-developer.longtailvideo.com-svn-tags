# This is a simple script that compiles the plugin using MXMLC (free & cross-platform).
# To use, make sure you have downloaded and installed the Flex SDK in the following directory:

FLEXPATH=/Developer/SDKs/flex_sdk_3

echo "Compiling with MXMLC..."

$FLEXPATH/bin/mxmlc ../src/as/com/longtailvideo/plugins/captions/Captions.as -sp ../src/as -o ../captions.swf -library-path+=../lib -load-externs=../lib/jwplayer-5-classes.xml -use-network=false