<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<title>Captions Plugin Reference</title>
<style>
    body { padding: 50px 100px; width: 660px; font: 13px/20px Arial; background: #FFF; }
    a , h1, h2{ color: #369; }
    h1 { font-size: 32px; }
    h2 { margin-top: 50px; }
    h3 { margin-top: 25px; }
    pre { font-size: 12px; background: #E5F3C8; padding:5px 10px; border: 1px solid #D3EAA4; }
    dd { color: #333; font-style: italic; }
</style>
</head><body>


<h1>Captions Plugin Reference</h1>

<h2>Purpose</h2>

<p>The purpose of this guide is providing a feature overview of the <a href="http://www.longtailvideo.com/addons/plugins/84/Captions">JW Player Captions Plugin</a>.</p>



<h2>Introduction</h2>

<p>The Captions plugin for JW Player supports the display of closed captions or subtitles at the bottom of a video. Captions can be shown or hidden with a toggle:</p>

<p><img src="assets/captions_example.png" alt="A screenshot of example captions implementation" style="margin-left:15px"/></p>

<p>Captions are read from external files, in the SRT (SubRip) text format or the DFXP (W3C TimedText) XML format. Captions are also read from MP4 videos (3GPP Timed Text). The plugin works in both Flash and HTML5, but there are certain format/device restrictions (see below).</p>

<p>The plugin can load multiple subtitle tracks per video, in which case a selection menu is presented. It also supports <a href="http://www.longtailvideo.com/support/jw-player/jw-player-for-flash-v5/12537/xml-playlist-support">playlists</a> and styling of the captions with CSS properties.</p>



<h2>Configuration Options</h2>

<p>Like the <a href="http://www.longtailvideo.com/support/jw-player/jw-player-for-flash-v5/12536/configuration-options">JW Player </a> itself, the Captions plugin can be configured with options that are set in the embed code. The following configuration options are available:</p>

<dl>
<dt><b>back</b> (<em>false</em>)</dt>
<dd>By default, the player renders a thin black outline around the captions, similar to TV / DVD captions. When setting this option <b>true</b>, a black box is drawn around the captions. This background makes the captions more readeable (nice for small texts), but does set them more apart from the video.</dd>
<dt><b>file</b> (<em>undefined</em>)</dt>
<dd>Location of the captions file to display. Should be the URL to a valid <a href="#dfxp">DFXP</a> or <a href="#srt">SRT</a> captions file. If your captions are embedded in your MP4 videos, or if you use a playlist, this option is not needed.</dd>
<dt><b>state</b> (<em>true</em>)</dt>
<dd>Describes whether to show the captions on startup or not. The default is <b>true</b> (captions are shown). When a viewer changes the state, the value is saved in a cookie, so users won't have to disable the captions on every video again if they don't want them.</dd>
</dl>

<h3>Example</h3>

<p>Here is a basic embed code of a player with the captions plugin, using the <a href="http://www.longtailvideo.com/support/jw-player/jw-player-for-flash-v5/15995/jw-embedder-reference-guide">JW Embedder</a>. The <strong>file</strong> option is used to load SRT captions:</p>

<pre>
&lt;script type="text/javascript" src="/jwplayer/jwplayer.min.js"&gt;&lt;/script&gt;

&lt;p id="container"&gt;The player will be placed here&lt;/p&gt;

&lt;script type="text/javascript"&gt;
 jwplayer("container").setup({
   file: "/assets/video.mp4",
   flashplayer: "/jwplayer/player.swf",
   height: 360,
   plugins: {
     "captions-3": {
       file: "/assets/captions.srt"
     }
   },
   width: 640
 });
&lt;/script&gt;
</pre>

<h3>Crossdomain loading</h3>

<p>An important issue to keep in mind with captions is that they cannot be loaded cross-domain. In other words, if your player is embedded at <em>http://somesite.com</em>, you cannot load SRT or XML captions from <em>http://othersite.com</em>. This restriction applies to all browsers and devices, in Flash and HTML5. There are workarounds though:</p>

<ul>
<li>You can place a small (PHP) script on the server that hosts your player to <em>proxy</em> the external playlist. This is the easiest solution which works for both Flash and HTML5.</li>
<li>If you're using only the Flash mode of JW Player (no HTML5), you can place a <a href="http://www.longtailvideo.com/support/jw-player/jw-player-for-flash-v5/12541/crossdomain-file-loading-restrictions">crossdomain.xml file</a> on the server that hosts the captions.</li>
<li>A very technical solution for HTML5 is to configure the server that hosts your captions for <a href="https://developer.mozilla.org/En/HTTP_Access_Control">Cross-Origin Resource Sharing</a>.</li>
</ul>



<h2>Formats and Devices</h2>

<p>This section lists the 3 formats the plugin supports, as well as the modes (Flash, HTML5)  and devices (iPad, iPhone, Android) in which these formats work.</p>

<h3>SRT (SubRip)</h3>

<p>The <a href="http://en.wikipedia.org/wiki/SubRip">SRT format</a> is a widely used and easy to understand plain text captioning format. It is supported in both Flash and HTML5 mode on all desktop browsers. SRT is not supported on Android and the iPad/iPhone. On those devices, it is not possible to render custom graphics during (fullscreen) video playback.</p>

<p>In SRT, a double linebreak is used to distinct between entries. Single linebreaks are used to add breaks in the texts themselves. Lines should be restricted to about 80 characters per line, which fits the default plugin setup. Here's an example file:</p>

<pre>
1
00:00:08,000 --&gt; 00:00:10,000
Nothing is going on.

2
00:00:10,500 --&gt; 00:00:12,500
Violet, please!
- I am not your babe!

3
00:00:17,000 --&gt; 00:00:20,000
You stupid cow,
look what you gone and done now, ay.
</pre>

<p><em>Note your SRT files should be saved using <strong>UTF8</strong> encoding in order to correctly display special characters (accents, but also e.g. Arab, Chinese, Russian).</em></p>

<h3>DFXP (W3C TimedText)</h3>

<p>The <a href="http://www.w3.org/TR/2010/PR-ttaf1-dfxp-20100914/">DFXP format</a> is an XML captioning format popular amongst Flash and Silverlight players. The Captions plugin also supports it in Flash mode, but not in HTML5. DFXP is therefore also not supported on the iPad, Android and the iPhone.</p>
    
<p>DFXP is a fairly complicated and structured XML format. The actual captions entries are found inside &lt;p&gt; tags inside the &lt;body&gt;, with &lt;br/&gt; tags used for line breaks. Here is an example:</p>

<pre>
&lt;tt xmlns="http://www.w3.org/2006/10/ttaf1"&gt;
 &lt;body&gt;
   &lt;div&gt;
     &lt;p begin="00:00:08" end="00:00:10"&gt;- Nothing is going on.&lt;/p&gt;
     &lt;p begin="00:00:10.5" end="00:00:12.5"&gt;You liar!&lt;/p&gt;
     &lt;p begin="00:00:13.5" end="00:00:15"&gt;Are you?&lt;/p&gt;
     &lt;p begin="00:00:17" end="00:00:20"&gt;Violet, please!&lt;br/&gt;- I am not your babe!&lt;/p&gt;
     &lt;p begin="00:00:34" end="00:00:36"&gt;Vi, please.&lt;br/&gt;- Leave me alone!&lt;/p&gt;
   &lt;/div&gt;
 &lt;/body&gt;
&lt;/tt&gt;
</pre>

<p><em>Note your DFXP files should be saved using <strong>UTF8</strong> encoding in order to correctly display special characters (accents, but also e.g. Arab, Chinese, Russian).</em></p>

<h3>MP4 (3GPP Text Tracks)</h3>

<p>The MP4 media container has the ability to <a href="http://en.wikipedia.org/wiki/MPEG-4_Part_17">embed timed text tracks</a>, in addition to e.g. a video and an audio track. This text data, often referred to as 3GPP Timed Text, is automatically picked up and displayed by the Captions plugin in Flash mode.</p>

<p>On the iPad and iPhone, the video element controller itself detect and renders the closed captions. It also displays a language selection menu in case multiple tracks are shown:</p>

<p><img src="assets/captions_iphone.png" alt="Embedded MP4 captions showing on the iPhone" style="margin-left:60px"/></p>

<p>In HTML5 mode on desktop browsers and on Android, embedded MP4 captions are not supported.</p>



<h2>Multiple Tracks</h2>

<p>It is possible to assign multiple captions tracks (for multiple languages) to one video. The button that used to toggle the captions then pops up a language selection menu:</p>

<p><img src="assets/captions_multitrack.png" alt="An example with multiple captions tracks" style="margin-left:20px"/></p>

<p>For MP4 files with multiple tracks, the plugin automatically detects the languages and renders the menu. For SRT or DFXP files, there's two configuration options to set:</p>

<dl>
<dt><b>files</b> (<em>undefined</em>)</dt>
<dd>When you have multiple captions, use the <b>files</b> option instead of <b>file</b>. Set it to a comma-separated list of URLs. Each URL in this list should link to a valid SRT or DFXP captions file. If your captions are embedded in your MP4 videos, this option is not needed.</dd>
<dt><b>labels</b> (<em>undefined</em>)</dt>
<dd>Set this value to a second comma-separated list, defining the labels for each language that should pop up in the selection menu (e.g. <em>English,Deutsch,Francais</em>). The amount and order of these labels should be the same as the amount and order of entries in the <b>files</b> option.</dd>
</dl>

<p>When a viewer changes the captions track, the value is saved in a cookie. That way the viewer won't have to re-set the track with every new video or page reload. You can override this cookied value by setting another option called <strong>label</strong>. Set it to the label of the track you want pre-selected.<p>

<p><em>Note you can also set the <strong>labels</strong> option to override the default MP4 track labels.</em></p>

<h3>Example</h3>

<p>This example embed code loads a video with 3 different SRT files:</p>

<pre>
&lt;script type="text/javascript" src="/jwplayer/jwplayer.min.js"&gt;&lt;/script&gt;

&lt;p id="container"&gt;The player will be placed here&lt;/p&gt;

&lt;script type="text/javascript"&gt;
 jwplayer("container").setup({
   file: "/assets/video.mp4",
   flashplayer: "/jwplayer/player.swf",
   height: 360,
   plugins: {
     "captions-3": {
       files: "/assets/deu.srt,/assets/fra.srt,/assets/ita.srt",
       labels: "Deutsch,Français,Italiano"
     }
   },
   width: 640
 });
&lt;/script&gt;
</pre>

<p><em>Note the selection menu cannot scroll (yet) if there are too many languages. Work around this issue by offering a language selection outside the player or use browser info for pre-selecting a few languages.</em></p>



<h2>Playlist Support</h2>

<p>Captions can be assigned to one or more videos in a playlist. The functionality is available for both inline and RSS playlists, in both Flash and HTML5. You can mix videos with and videos without captions in a single feed.</p>

<p>Since RSS playlist format does not define an element for linking to captions files, captions should be set using the <a href="http://www.longtailvideo.com/support/jw-player/jw-player-for-flash-v5/12537/xml-playlist-support">JWPlayer XML namespace</a>. In practice, the namespace is enabled by:</p>

<ul>
<li>Setting an <b>xmlns:jwplayer</b> attribute in the main XML tag.</li>
<li>Prefixing the XML elements with <b>jwplayer:</b>, e.g. <em>&lt;jwplayer:captions.file&gt;</em></li>
</ul>

<p>You can set either the <b>captions.file</b> (single track) or the <b>captions.files</b> &amp; <b>captions.labels</b> (multiple tracks) options for each playlist entry.</p>

<h3>Example</h3>

<p>Here is an example RSS playlist. The <em>captions.file</em> option is set for both entries:</p>

<pre>
&lt;rss version="2.0" xmlns:jwplayer="http://developer.longtailvideo.com/"&gt;
  &lt;channel&gt;
    &lt;title&gt;Example RSS playlist with captions&lt;/title&gt;

    &lt;item&gt;
      &lt;title&gt;Coronation Street&lt;/title&gt;
      &lt;description&gt;A episode clip, with captions.&lt;/description&gt;
      &lt;enclosure url="/static/corrie.mp4" /&gt;
      &lt;jwplayer:captions.file&gt;/static/corrie.srt&lt;/jwplayer:captions.file&gt;
    &lt;/item&gt;

    &lt;item&gt;
      &lt;title&gt;Big Buck Bunny&lt;/title&gt;
      &lt;description&gt;The official trailer, with captions.&lt;/description&gt;
      &lt;enclosure url="/static/bunny.mp4" /&gt;
      &lt;jwplayer:captions.file&gt;/static/bunny.srt&lt;/jwplayer:captions.file&gt;
    &lt;/item&gt;

  &lt;/channel&gt;
&lt;/rss&gt;
</pre>

<p><em>Note the same crossdomain loading restrictions that apply to captions also <a href="http://www.longtailvideo.com/support/jw-player/jw-player-for-flash-v5/19824/playlist-support-in-the-jw-player#XML">apply to playlists</a>.</em></p>



<h2>Styling the Captions</h2>

<p>It is possible to change the styling of the captions with a couple of configuration options. Here's a screenshot of a video with styled captions:</p>

<p><img src="assets/captions_styling.png" alt="A screenshot of player with styled captions" style="margin-left:20px"/></p>

<p>The following six style properties can be set. Add them to the player embed code, just like the <em>back</em> and <em>state</em> options:</p>

<dl>
<dt><b>color</b> ( <em>#FFFFFF</em> )</dt>
<dd>Can be any hexadecimal color value (e.g. <strong>#FFCC00</strong>).</dd>
<dt><b>fontFamily</b> ( <em>Arian,sans-serif</em> )</dt>
<dd>Can be any font installed <a href="http://www.fonttester.com/web_safe_fonts.html">on a user's computer</a> (e.g. <strong>Georgia,serif</strong>).</dd>
<dt><b>fontSize</b> ( <em>15</em> )</dt>
<dd>Can be any size in pixels (e.g. <strong>20</strong>). Note the captions are scaled to cover the video, with the actual pixel size used at a video width of 400px.</dd>
<dt><b>fontStyle</b> ( <em>normal</em> )</dt>
<dd>Can be set to <strong>italic</strong> for making the text italic.</dd>
<dt><b>fontWeight</b> ( <em>normal</em> )</dt>
<dd>Can be set to <strong>bold</strong> for boldening the text.</dd>
<dt><b>textDecoration</b> ( <em>none</em> )</dt>
<dd>Can be set to <strong>underline</strong> to add a line below the text.</dd>
</dl>

<p><em>Note these styling options do <strong>not</strong> work for MP4 captions on the iPad/iPhone, since these devices offer no control over rendering of the captions.</em></p>

<h3>DFXP Styling</h3>

<p>The DFXP format contains two different mechanisms for styling captions. Both are supported by the player, though solely in Flash mode:<p>

<ul>
<li>The <em>&lt;head&gt;</em> of a DFXP file can contain one or more style elements. These elements are all given an ID. The individual captions paragraphs can be linked to the style rules using the <em>style="xx"</em> attribute.</li>
<li>Inside captions paragraphs, text snippets can be wrapped in <em>&lt;span&gt;</em> elements. These spans can contain a <em>style="xx"</em> attribute, or even list individual style rules (like <em>fontWeight="bold"</em>).</li>
</ul>

<p>Here is the example DFXP file, containing both styling methods. Note the individual rules (color, fontSize, etc.) need to be prefixed with a <b>tts:</b> namespace identifier. The according namespace declaration (<em>xmlns:tts</em>) needs to be set in the main XML element to make it a valid file:</p>

<pre>
&lt;tt xmlns="http://www.w3.org/2006/10/ttaf1" 
  xmlns:tts="http://www.w3.org/2006/04/ttaf1#styling"&gt;
  &lt;head&gt;
   &lt;styling&gt;
      &lt;style id="normal" tts:fontSize="15" /&gt;
      &lt;style id="warning" tts:color="#FF0000" tts:fontWeight="bold" tts:fontSize="20" /&gt;
   &lt;/styling&gt;
  &lt;/head&gt;
  &lt;body&gt;
    &lt;div&gt;
      &lt;p begin="00:00:08" end="00:00:10" style="normal"&gt;- Nothing is going on.&lt;/p&gt;
      &lt;p begin="00:00:10.5" end="00:00:12.5" style="warning"&gt;You liar!&lt;/p&gt;
      &lt;p begin="00:00:17" end="00:00:20" style="normal"&gt;Violet, please!&lt;br/&gt;
          - I am &lt;span style="warning"&gt;not&lt;/span&gt; your babe!
      &lt;/p&gt;
      &lt;p begin="00:00:24" end="00:00:29" style="normal"&gt;
          You &lt;span tts:fontStyle="italic"&gt;stupid cow&lt;/span&gt;, look what you did.&lt;/p&gt;
    &lt;/div&gt;
  &lt;/body&gt;
&lt;/tt&gt;
</pre>

<p><em>Note the captions plugin does not support &lt;span&gt; tags inside &lt;span&gt; tags.</em></p>

<h3>SRT Styling</h3>

<p>The SRT file format does not support any styling, but this can be forces by inserting HTML tags. <em>&lt;b&gt;</em>, <em>&lt;i&gt;</em> and <em>&lt;u&gt;</em> can be used to set weight, style and decoration and  <em>&lt;font color="#ff0000" face="Courier" size="18"&gt;</em> can be used to set color, family and size. Use this as last resort, since compatibility with many tools <strong>will likely break</strong>.</p>



<h2>Changelog</h2>

<h3>Version 3.0</h3>

<ul>
<li>Added support for HTML5 mode: back/file/state options, SRT/MP4 formats, multiple tracks, captions styling and playlists.</li>
</ul>
<ul>
<li>Enhanced support for dock button language shortcode in multitrack setups. The full label is simply shown.</li>
<li>Made captions aware of controlbar in the "over" state. For player 5.7+, the captions will now scroll up if the controlbar shows and scroll down again if the controlbar hides.</li>
<li>Added a timed transition to showing/hiding of the language selector.</li>
<li>Added support for overriding the MP4 language labels with the "labels" option.</li>
<li>Added a close button to the language selector menu.</li>
<li>Added support for displaying filenames in language selector if labels are not shown.</li>
<li>Added support for forcing a pre-set language through the "label" option.</li>
<li>Enhanced scaling of the captions. For larger dimensions, they're now scaled relatively smaller.</li>
</ul>
<ul>
<li>Fixed an issue with language selector tabbing that required two tabs for advancing one entry.</li>
<li>Fixed an issue with OVA advertising that caused displaying captions on the preroll.</li>
</ul>
<ul>
<li>Deprecated skinning the captions toggle (no skinning in HTML5). The feature still works, but only in Flash.</li>
<li>Deprecated placing the toggle in the controlbar (no custom controlbar buttons in HTML5). The feature still works, but only in Flash.</li>
</ul>



</body></html>