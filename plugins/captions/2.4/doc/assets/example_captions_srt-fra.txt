1
00:01:47,025 --> 00:01:50,050
Cette lame a un sombre passé.

2
00:01:51,079 --> 00:01:55,079
Elle a fait couler bien du sang innocent.

3
00:01:58,000 --> 00:02:01,045
Tu es bien idiote de voyager seule
sans la moindre préparation.

4
00:02:01,075 --> 00:02:04,079
Tu as de la chance que ton sang coule encore
dans tes veines.

5
00:02:05,025 --> 00:02:06,029
Merci.

6
00:02:07,050 --> 00:02:09,000
Alors...

7
00:02:09,040 --> 00:02:13,080
Qu'est-ce qui t'amène
sur la terre des gardiens ?

8
00:02:15,000 --> 00:02:17,050
Je recherche quelqu'un.

9
00:02:18,000 --> 00:02:22,019
Quelqu'un de très cher ?
Une âme sœur ?

10
00:02:23,040 --> 00:02:25,000
Un dragon.

11
00:02:28,084 --> 00:02:31,075
Une quête dangereuse pour une chasseresse solitaire.

12
00:02:32,094 --> 00:02:35,087
J'ai été seule
aussi longtemps que je m'en souvienne.

13
00:03:27,025 --> 00:03:30,050
C'est bientôt fini. Chut...

14
00:03:30,075 --> 00:03:33,050
Ne bouge pas !

15
00:03:48,025 --> 00:03:52,025
Bonne nuit, Scales.

16
00:04:10,034 --> 00:04:13,084
Attrape-la, Scales ! Vas-y !

17
00:04:25,025 --> 00:04:28,025
Scales?

18
00:05:04,000 --> 00:05:07,050
Ouais ! Vas-y !

19
00:05:38,075 --> 00:05:42,000
Scales !

20
00:07:25,085 --> 00:07:27,050
J'ai échoué.

21
00:07:32,080 --> 00:07:36,050
Tu n'as juste pas su ouvrir les yeux...

22
00:07:37,080 --> 00:07:40,050
Ce sont des terres à dragons, Sintel.

23
00:07:40,085 --> 00:07:44,000
Tu es plus proche que tu ne le crois.

24
00:09:17,060 --> 00:09:19,050
Scales!

25
00:10:21,060 --> 00:10:24,000
Scales?

26
00:10:26,020 --> 00:10:29,079
Scales...
