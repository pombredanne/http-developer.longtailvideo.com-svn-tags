package com.longtailvideo.plugins.captions {


    import com.longtailvideo.jwplayer.utils.Logger;

    import flash.display.*;
    import flash.events.*;


    /** Component that renders a select box. **/
    public class Selector extends MovieClip {


        /** Compiles of the default graphics. **/
        [Embed(source="../../../../../../assets/divider.png")]
        private const DividerRow:Class;
        [Embed(source="../../../../../../assets/sheet.png")]
        private const BackSheet:Class;
        [Embed(source="../../../../../../assets/close.png")]
        private const CloseButton:Class;


        /** currently active option. **/
        private var _active:Number;
        /** Background graphic. **/
        private var _back:Sprite;
        /** Close button. **/
        private var _close:Sprite;
        /** Container for the buttons. **/
        private var _container:Sprite;
        /** Function to call when a selection is made. **/
        private var _callback:Function;
        /** List with options from the selector. **/
        private var _options:Array;


        /** Constructor. **/
        public function Selector(callback:Function) {
            _callback = callback;
            _back = new Sprite();
            _back.addChild(new BackSheet());
            addChild(_back);
            _back.buttonMode = true;
            _back.addEventListener(MouseEvent.CLICK,_backHandler);
            _close = new Sprite();
            _close.addChild(new CloseButton());
            addChild(_close);
            _close.mouseEnabled = false;
            _container = new Sprite();
            addChild(_container);
            _container.buttonMode = true;
        };


        /** Background was clicked. **/
        private function _backHandler(event:MouseEvent):void {
            _buttonHandler(_active);
        }; 


        /** Button was clicked. **/
        private function _buttonHandler(index:Number):void {
            _options[_active].button.activate(false);
            _options[index].button.activate(true);
            _active = index;
            _callback(index);
        };


        /** Populate the selector with a number of items. **/
        public function populate(options:Array,active:Number=0):void {
            _options = options;
            _active = active;
            // Remove existing buttons.
            while(_container.numChildren > 0) {
                _container.removeChildAt(0);
            }
            // Draw new buttons.
            for(var i:Number = 0; i<_options.length; i++) {
                _options[i].button = new SelectorButton(_buttonHandler,i,_options[i].label);
                _container.addChild(_options[i].button);
                _options[i].button.y = i*25;
                if(i < _options.length - 1) {
                    var divider:DisplayObject = new DividerRow();
                    divider.y = i*25 + 24;
                    _container.addChild(divider);
                }
            }
            // Set active button and center the list
            _options[_active].button.activate(true);
            resize(_back.width,_back.height);
        };


        /** Resize the selector screen. **/
        public function resize(width:Number,height:Number):void {
            _back.height = height;
            _back.width = width;
            _close.x = width - 50;
            if(_options) {
                _container.x = Math.round(_back.width/2 - _container.width/2);
                _container.y = Math.round(_back.height/2 - _container.height/2);
            }
        };


};


}
