package com.longtailvideo.plugins.captions {


    import flash.accessibility.AccessibilityProperties;
    import flash.display.*;
    import flash.events.*;
    import flash.filters.DropShadowFilter;
    import flash.text.*;


    /** A single selector button. **/
    public class SelectorButton extends Sprite {


        /** Embeds of the default graphics. **/
        [Embed(source="../../../../../../assets/back.png")]
        private const ButtonBack:Class;
        [Embed(source="../../../../../../assets/active.png")]
        private const ActiveIcon:Class;


        /** The icon that indicates the item is active. **/
        private var _active:DisplayObject;
        /** Background of the button. **/
        private var _back:DisplayObject;
        /** Textfield of the button. **/
        private var _field:TextField;
        /** Label of the textfield. **/
        private var _label:String;
        /** Formatting of the textfield. **/
        private var _format:TextFormat;
        /** Handler to call when the button is clicked. **/
        private var _handler:Function;
        /** Index of the button. **/
        private var _index:Number;


        /** Constructor. **/
        public function SelectorButton(handler:Function,index:Number,label:String) {
            _handler = handler;
            _index = index;
            _label = label;
            _back = new ButtonBack();
            _back.alpha = 0;
            addChild(_back);
            _field = new TextField();
            _field.filters = new Array(new DropShadowFilter(1,45,0,1,0,0));
            addChild(_field);
            // Create the text formatting.
            _format = new TextFormat();
            _format.color = 0xFFFFFF;
            _format.size = 12;
            _format.bold = true;
            _format.align = TextFormatAlign.CENTER;
            _format.font = "_sans";
            _field.defaultTextFormat = _format;
            // Set other textfield props.
            _field.selectable = false;
            _field.height = 20;
            _field.width = 180;
            _field.y = 2;
            _field.text = _label;
            // Add active indicator (small dot).
            _active = new ActiveIcon();
            _active.x = 30;
            _active.y = 0;
            _active.visible = false;
            addChild(_active);
            // Insert accessibility options (tabbing / screenreader label).
            var acs:AccessibilityProperties = new AccessibilityProperties();
            acs.name = label;
            accessibilityProperties = acs;
            buttonMode = true;
            mouseChildren = false;
            tabEnabled = true;
            tabChildren = false;
            tabIndex = index + 501;
            // Set event handlers.
            addEventListener(MouseEvent.CLICK,_clickHandler);
            addEventListener(MouseEvent.MOUSE_OUT,_outHandler);
            addEventListener(MouseEvent.MOUSE_OVER,_overHandler);
        };


        /** (de)activate the button. **/
        public function activate(state:Boolean):void {
            if(state) {
                _active.visible = true;
            } else {
                _active.visible = false;
            }
        };


        /** Register the click. **/
        private function _clickHandler(event:MouseEvent):void { 
            _handler(_index);
        };


        /** Fade the back on rollover. **/
        private function _outHandler(event:MouseEvent):void { 
            _back.alpha = 0;
        };


        /** Fade the back on rollover. **/
        private function _overHandler(event:MouseEvent):void {
            _back.alpha = 1;
        };

    };


}
