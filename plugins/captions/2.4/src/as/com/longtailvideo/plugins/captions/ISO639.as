package com.longtailvideo.plugins.captions {


    /** Mapping table for MP4 TimedText ISO639-2 language codec. **/
    public class ISO639 {


        /** Most common codes (hat tip to Google Translate). **/
        private static const MAP:Object = {
            afr: 'Afrikaans',
            alb: 'Albanian',
            ara: 'Arabic',
            aze: 'Azerbaijani',
            baq: 'Basque',
            bel: 'Belarusian',
            bul: 'Bulgarian',
            cat: 'Catalan',
            chi: 'Chinese',
            cze: 'Czech',
            dan: 'Danish',
            deu: 'German',
            dut: 'Dutch',
            eng: 'English',
            esp: 'Spanish',
            est: 'Estonian',
            fil: 'Filipino',
            fin: 'Finnish',
            fra: 'French',
            fre: 'French',
            geo: 'Georgian',
            ger: 'Deutsch',
            gle: 'Irish',
            glg: 'Galician',
            gre: 'Greek',
            hat: 'Haitian',
            heb: 'Hebrew',
            hin: 'Hindi',
            hrv: 'Croatian',
            hun: 'Hungarian',
            ice: 'Icelandic',
            ind: 'Indonesian',
            ita: 'Italian',
            jpn: 'Japanese',
            kor: 'Korean',
            lat: 'Latin',
            lav: 'Latvian',
            lit: 'Lithuanian',
            mac: 'Macedonian',
            may: 'Malay',
            mlt: 'Maltese',
            nor: 'Norwegian',
            per: 'Persian',
            pol: 'Polish',
            por: 'Portuguese',
            rum: 'Romanian',
            rus: 'Russian',
            slo: 'Slovak',
            slv: 'Slovenian',
            spa: 'Spanish',
            srp: 'Serbian',
            swa: 'Swahili',
            swe: 'Swedish',
            tha: 'Thai',
            tur: 'Turkish',
            ukr: 'Ukrainian',
            urd: 'Urdu',
            vie: 'Vietnamese'
        };


        /** Get the name of a specific language code. **/
        public static function label(code:String):String { 
            if(ISO639.MAP[code]) { 
                return ISO639.MAP[code];
            } else {
                return "Captions on";
            }
        };


    };


}
