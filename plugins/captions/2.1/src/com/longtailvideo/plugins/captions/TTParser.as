﻿package com.longtailvideo.plugins.captions {


import com.longtailvideo.jwplayer.utils.Strings;


public class TTParser {


	/** Parse stylesheets from the head. */
	public static function parseStyles(data:XML,defaults:Object):Object {
		var styles:Object = {};
		for each (var i:XML in data.children()) {
			if (i.localName() == "head") {
				for each (var styling:XML in i.children()) {
					for each (var node:XML in styling.children()) {
						if (node.localName() == 'style') {
						    // Set the defaults.
							var rules:Object = {};
							for(var rule:String in defaults) {
							    rules[rule] = defaults[rule];
							}
							// Loop through all attributes for overrides.
							for each (var attrib:XML in node.attributes()) {
								var name:String = attrib.name();
								if (name.indexOf("::") > -1) {
									name = name.substring(name.indexOf("::") + 2);
								}
								rules[name] = attrib.toString();
							}
							// Save to listing
							if (node.@id) {
								styles[node.@id] = rules;
							}
						}
					}
				}
			}
		}
		return styles;
	};


	/** Parse captions from the TT XML, returning a list with {begin:Number,text:String} objects. **/
	public static function parseCaptions(data:XML):Array {
		var arr:Array = new Array({begin:0,text:''});
		for each (var i:XML in data.children()) {
			if (i.localName() == "body") {
				for each (var j:XML in i.children()) {
					for each (var k:XML in j.children()) {
					    // Paragraphs are single captions. They live inside dividers.
						if (k.localName() == 'p') {
							var obj:Object = TTParser.parseCaption(k);
							arr.push(obj);
							// End with a new, empty caption, accontig for duration or end set.
							if (obj['end']) {
								arr.push({begin:obj['end'],text:''});
								delete obj['end'];
							} else if (obj['dur']) {
								arr.push({begin:obj['begin']+obj['dur'],text:''});
								delete obj['dur'];
							}
						}
					}
				}
			}
		}
		return arr;
	};


	/** Parse a single captions entry. **/
	private static function parseCaption(dat:XML):Object {
		var ptn:RegExp = /(\n<br.*>\n)+/;
		var obj:Object = {
			begin:Strings.seconds(dat.@begin),
			dur:Strings.seconds(dat.@dur),
			end:Strings.seconds(dat.@end),
			style:dat.@style,
			// Not sure what this does anymore, but looks like it should be cleaned up.
			text:dat.children().toString().replace(ptn,'<br/>').replace(/\n</,' <').replace(/>\n/, '> ')
		};
		return obj;
	};


	/** Convert inline caption styling into HTML. */
	public static function parseSpans(text:String,styles:Object,defaults:Object):String {
		while (text.indexOf("<span") > -1) {
			text = parseSpan(text,styles,defaults);
		}
		return text;
	};


	/** Convert a span entry into HTML. **/
	private static function parseSpan(text:String,styles:Object,defaults:Object):String {
		var rules:Object = {};
		var newtext:String = '';
		// Find the span bounds and convert to XML.
		var left:Number = text.indexOf("<span ");
		var right:Number = text.indexOf("</span>",left);
		if (left > -1 && right > -1) {
			var span:XML = new XML(text.substring(left,right+7));
			// Use style if defined, else set defaults.
			var style:String = span.@style;
			if(style && styles[style]) {
			    for(var i:String in styles[style]) { rules[i] = styles[style][i]; }
			} else {
			    for(var j:String in defaults) { rules[j] = defaults[j]; }
			}
			// Override style with inline declarations
			for each (var attrib:XML in span.@*) {
				var name:String = attrib.localName().toString();
				if (rules[name]) {
				    rules[name] = attrib.toString();
				}
			}
			// Wrap plain text with font and b/i/u tags.
			newtext = '<font family="'+rules.fontFamily+'" size="'+rules.fontSize+'" color="'+rules.color+'">';
			newtext +=  text.substring(text.indexOf('>',left)+1, right);
			newtext += "</font>";
			if(rules.fontWeight == 'bold') { newtext = '<b>'+newtext+'</b>'; }
			if(rules.fontStyle == 'italic') { newtext = '<i>'+newtext+'</i>'; }
			if(rules.textDecoration == 'underline') { newtext = '<u>'+newtext+'</u>'; }
		}
		return text.substr(0,left)+newtext+text.substr(right+7);
	};


}


}
