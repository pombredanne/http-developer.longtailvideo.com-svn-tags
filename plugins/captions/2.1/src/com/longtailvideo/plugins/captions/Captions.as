package com.longtailvideo.plugins.captions {

	import com.longtailvideo.jwplayer.events.*;
	import com.longtailvideo.jwplayer.player.IPlayer;
	import com.longtailvideo.jwplayer.player.PlayerState;
	import com.longtailvideo.jwplayer.plugins.*;
	import com.longtailvideo.jwplayer.utils.Logger;
	import com.longtailvideo.jwplayer.view.interfaces.IControlbarComponent;
	import com.longtailvideo.jwplayer.view.interfaces.IDockComponent;

	import flash.display.*;
	import flash.events.*;
	import flash.filters.DropShadowFilter;
	import flash.net.*;
	import flash.text.*;
	import flash.utils.*;


	/** Plugin for playing closed captions with a video. **/
	public class Captions extends MovieClip implements IPlugin {


	[Embed(source="../../../../../assets/controlbar.png")]
	private const ControlbarButton:Class;
	[Embed(source="../../../../../assets/dock.png")]
	private const DockIcon:Class;


	/* Reference to the bar (container for field & back)*/
	private var _bar:Sprite;
	/** Reference to the controlbar button. **/
	private var _button:MovieClip;
	/** Reference to the controlbar button icon **/
	private var _buttonIcon:DisplayObject;
	/** The array the captions are loaded into. **/
	private var _captions:Array;
	/** List with configuration settings. **/
	private var _config:Object = {
		back:false,
		file:undefined,
		state:true
	};
	/** Currently active caption. **/
	private var _current:Number;
	/** Default style properties. **/
	private var _defaults:Object = {
		color: '#FFFFFF',
		fontFamily: 'Arial, sans-serif',
		fontSize: 14,
		fontStyle: 'normal',
		fontWeight: 'normal',
		leading: 5,
		textAlign: 'center',
		textDecoration: 'none'
	};
	/** Reference to the textfield. **/
	public var _field:TextField;
	/** Currently used captions file. **/
	private var _file:String;
	/** Textformat entry for the captions. **/
	private var _format:TextFormat;
	/** Reference to the dock icon. **/
	private var _icon:MovieClip;
	/** XML connect and parse object. **/
	private var _loader:URLLoader;
	/** Reference to the outline graphic. **/
	private var _outline:Sprite;
	/* Reference to the JW Player. */
	private var _player:IPlayer;
	/** The default stylesheet. **/
	private var _sheet:StyleSheet;
	/** A map of TT stylesheets. **/
	private var _styles:Object;


	/** Constructor; solely inits the captions file loader. **/
	public function Captions() {
		_sheet = new StyleSheet();
		_loader = new URLLoader();
		_loader.addEventListener(Event.COMPLETE,loaderHandler);
		_loader.addEventListener(IOErrorEvent.IO_ERROR,errorHandler);
		_loader.addEventListener(SecurityErrorEvent.SECURITY_ERROR,errorHandler);
	};


	/** Clicking the controlbar icon or dock button. **/
	private function clickHandler(evt:MouseEvent):void {
		setState(!_config['state']);
	};


	/** Draw the neccessary graphics to put the captions on stage. **/
	private function drawClip():void {
		_bar = new Sprite();
		addChild(_bar);
		_outline = new Sprite();
		_bar.addChild(_outline);
		_field = new TextField();
		_field.width = 400;
		_field.height = 10;
		_field.autoSize = "center";
		_field.multiline = true;
		_field.selectable = false;
		_field.wordWrap = true;
		_field.styleSheet = _sheet;
		_bar.addChild(_field);
		if (!_config['back']) {
			_field.filters = new Array(new DropShadowFilter(0,45,0,1,2,2,10,3));
			_outline.alpha = 0;
		}
	};


	/** The captions loader returns errors (file not found or security error. **/
	private function errorHandler(evt:ErrorEvent):void { 
		setCaption('<font color="#FF0000">' + evt.text + '</font>');
	};


	/** Identifier string of this plugin in the JW Player. **/
	public function get id():String {
		return "captions";
	};


	/** Initialize as a JW Player 5 plugin. */
	public function initPlugin(ply:IPlayer, cfg:PluginConfig):void {
		_player = ply;
		for (var prp:String in _config) {
			if (cfg[prp] != undefined) { _config[prp] = cfg[prp]; }
		}
		for (var rule:String in _defaults) {
			if (cfg[rule.toLowerCase()] != undefined) { 
			    // Fix for colors, since the player automatically converts to HEX.
			    if(rule == 'color') {
			        _defaults['color'] = '#'+String(cfg[rule.toLowerCase()]).substr(-6);
		        } else {
		            _defaults[rule] = cfg[rule.toLowerCase()];
		        }
			}
		}
		_player.addEventListener(PlayerStateEvent.JWPLAYER_PLAYER_STATE,stateHandler);
		_player.addEventListener(PlaylistEvent.JWPLAYER_PLAYLIST_ITEM,itemHandler);
		_player.addEventListener(MediaEvent.JWPLAYER_MEDIA_TIME,timeHandler);
		_player.addEventListener(MediaEvent.JWPLAYER_MEDIA_META,metaHandler);
		if (_player.config.dock) {
			var icn:DisplayObject = _player.skin.getSkinElement("captions", "dockIcon");
			if (icn == null) { icn = new DockIcon(); }
			_icon = _player.controls.dock.addButton(icn,'is on',clickHandler);
		} else {
			_buttonIcon = _player.skin.getSkinElement("captions", "controlbarButton");
			if (_buttonIcon == null) { _buttonIcon = new ControlbarButton(); }
			_button = _player.controls.controlbar.addButton(_buttonIcon,'captions',clickHandler);
		}
		drawClip();
		setState(_config['state']);
		_bar.visible = false;
	};


	/** Check for captions with a new playlistItem. **/
	private function itemHandler(evt:PlaylistEvent):void {
		_current = 0;
		_captions = new Array();
		setCaption('');
		_file = undefined;
		if (_player.playlist.currentItem['captions.file']) {
			_file = _player.playlist.currentItem['captions.file'];
		} else if (_player.playlist.currentItem['captions']) {
			_file = _player.playlist.currentItem['captions'];
		} else if (_config['file']) {
			_file = _config['file'];
		} else if (_player.config['captions']) {
			_file = _player.config['captions'];
		}
		if(_file) {
			load();
		}
	};


	/** Load the captions file. **/
	private function load():void {
		_loader.load(new URLRequest(_file));
	};


	/** Captions are loaded; now display them. **/
	private function loaderHandler(evt:Event):void {
		try {
			if(XML(evt.target.data).localName().toString().toLowerCase() == 'tt') {
				_styles = TTParser.parseStyles(XML(evt.target.data),_defaults);
				_captions = TTParser.parseCaptions(XML(evt.target.data));
			} else {
				_captions = SRTParser.parseCaptions(String(evt.target.data));
			}
		} catch (err:Error) {
			_captions = SRTParser.parseCaptions(String(evt.target.data));
		}
		if (_captions.length < 1) {
			setCaption('<font color="#FF0000">No captions found.<br/>Probably not a valid SRT or DFXP file.</font>');
		}
	};


	/** Check for captions in metadata. **/
	private function metaHandler(evt:MediaEvent):void {
		var txt:String;
		var fnd:Boolean = false;
		if (evt.metadata.type == 'caption') {
			txt = evt.metadata.captions;
			fnd = true;
		} else if (evt.metadata.type == 'textdata') {
			txt = evt.metadata.text;
			if(txt.substr(-1) == '\n') { txt = txt.substr(0,txt.length-1); }
			fnd = true;
		}
		if (fnd == true) {
			setCaption(txt);
		}
	};


	/** Resize this plugin */
	public function resize(w:Number, h:Number):void {
		_bar.width = w;
		_bar.scaleY = _bar.scaleX;
		if (_player.config.fullscreen || _player.config.controlbar == 'over') {
			_bar.y = h - _bar.height - 45;
		} else {
			_bar.y = h - _bar.height - 5;
		}
	};


	/** Set the current caption field text. */
	private function setCaption(text:String, style:String=null):void {
		if (style && _styles[style]) {
		    _sheet.setStyle("p",_styles[style]);
		    text = TTParser.parseSpans(text,_styles,_styles[style]);
		} else { 
			_sheet.setStyle("p",_defaults);
			text = TTParser.parseSpans(text,_styles,_defaults);
		}
		_field.htmlText = "<p>" + text + "</p>" ;
		setOutline();
		resize(_player.config['width'], _player.config['height']);
	};


    /** Draw the black boxes around the textlines. **/
    private function setOutline():void {
        _outline.graphics.clear();
        for (var i:Number=0; i < _field.numLines; i++) {
            var metrics:TextLineMetrics = _field.getLineMetrics(i);
            if(metrics.width > 16) {
                _outline.graphics.beginFill(0x000000,0.85);
                _outline.graphics.drawRect(
                    metrics.x - 8,
                    i*metrics.height,
                    metrics.width + 16,
                    metrics.height
                );
                _outline.graphics.endFill();
            }
        }
    };


	/** Show/hide the captions, update the button, save state in cookie. **/
	public function setState(stt:Boolean):void {
		_config['state'] = stt;
		_bar.visible = stt;
		if (stt) {
			if (_icon) {
				_icon.field.text = "is on";
			} else {
				_buttonIcon.alpha = 1;
			}
		} else {
			if (_icon) {
				_icon.field.text = "is off";
			} else {
				_buttonIcon.alpha = 0.3;
			}
		}
		var cke:SharedObject = SharedObject.getLocal('com.jeroenwijering','/');
		cke.data['captions.state'] = stt;
		cke.flush();
	};


	/** Captions are only visible when playing / paused. **/
	private function stateHandler(evt:PlayerStateEvent):void {
		if (_config['state'] && (
			evt.newstate == PlayerState.PLAYING || 
			evt.newstate == PlayerState.PAUSED || 
			evt.newstate == PlayerState.BUFFERING)) {
			_bar.visible = true;
		} else {
			setCaption('');
			_bar.visible = false;
		}
	};


	/** Check timing of the player to sync captions. **/
	private function timeHandler(evt:MediaEvent):void {
		var pos:Number = evt.position;
		if (_config['state'] == true && _captions.length && 
			(_captions[_current]['begin'] > pos || (_captions[_current+1] && _captions[_current+1]['begin'] < pos))) {
			updateCaption(pos);
		}
	};


	/** Set a caption on screen. **/
	private function updateCaption(pos:Number):void {
		for (var i:Number=0; i<_captions.length; i++) {
			if (_captions[i]['begin'] < pos && (i == _captions.length - 1 || _captions[i+1]['begin'] > pos)) {
				_current = i;
				setCaption(_captions[i]['text'], _captions[i]['style']);
				return;
			}
		}
	};


};


}
