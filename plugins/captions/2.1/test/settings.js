var settings = {
	/** Players to list. **/
	players: {
		'5.3':'players/5.3.swf',
		'5.2':'players/5.2.swf',
		'5.1':'players/5.1.swf',
		'5.0':'players/5.0.swf'
	},
	/** Plugins to list. **/
	plugins: {
		captions:'../captions.swf'
	},
	/** Skins to list. **/
	skins: {
		none:'',
		bekle:'skins/bekle.zip',
		glow:'skins/glow.zip',
		modieus:'skins/modieus.zip',
		stijl:'skins/stijl.zip',
	},
	/** All the setup examples with their flashvars. **/
	examples: {
		'':{},
		'DFXP captions': {
			plugins:'captions',
			file:'../assets/corrie.flv',
			image:'assets/corrie.jpg',
			'captions.file':'assets/plain.xml'
		},
		'SRT captions': {
			dock:'false',
			plugins:'captions',
			file:'../assets/corrie.flv',
			image:'assets/corrie.jpg',
			'captions.file':'assets/plain.srt'
		},
		'MP4 captions': {
			plugins:'captions',
			file:'../assets/bunny.mp4',
			image:'assets/bunny.jpg',
			'captions.back':true
		},
		' ':{},
		'Playlist with captions': {
			dock:false,
			file:'assets/playlist.xml',
			height:240,
			playlist:'right',
			playlistsize:240,
			plugins:'captions',
			width:720
		},
		'Multibitrate stream with captions': {
			plugins:'captions',
			file:'http://content.bitsontherun.com/jwp/nPripu9l.xml',
			captions:'assets/bunny.srt'
		},
		'  ':{},
		'Config styling': {
			plugins:'captions',
			file:'../assets/corrie.flv',
			image: 'assets/corrie.jpg',
			'captions.file': 'assets/plain.xml',
			'captions.color': 'FFCC00',
			'captions.fontFamily': 'Georgia',
			'captions.fontSize': 18,
			'captions.fontStyle': 'italic',
			'captions.fontWeight': 'bold',
			'captions.textDecoration': 'underline'
		},
		'DFXP styling': {
			plugins:'captions',
			file:'../assets/corrie.flv',
			image:'assets/corrie.jpg',
			'captions.file':'assets/styled.xml'
		},
		'SRT styling': {
			plugins:'captions',
			file:'../assets/corrie.flv',
			image:'assets/corrie.jpg',
			'captions.file':'assets/styled.srt'
		},
		'Config + DFXP styling': {
			plugins:'captions',
			file:'../assets/corrie.flv',
			image: 'assets/corrie.jpg',
			'captions.file': '../doc/assets/captions_styled.xml',
			'captions.fontFamily': 'Georgia',
			'captions.fontSize': 18,
			'captions.fontStyle': 'italic',
			'captions.fontWeight': 'bold'
		}
	}
}
