# This is a simple script that compiles the plugin using MXMLC (free & cross-platform).
# Learn more at http://developer.longtailvideo.com/trac/wiki/PluginsCompiling
# To use, make sure you have downloaded and installed the Flex SDK in the following directory:

FLEXPATH=/Developer/SDKs/flex_sdk_3

echo "Compiling with MXMLC..."

$FLEXPATH/bin/mxmlc ../src/com/longtailvideo/plugins/captions/Captions.as -sp ../src -o ../captions.swf -library-path+=../lib -load-externs=../lib/jwplayer-5-classes.xml -use-network=false -optimize=true -incremental=false

# $FLEXPATH/bin/compc -source-path ../src -output ../captions.swc -compiler.library-path $FLEXPATH/frameworks/libs -compiler.library-path ../lib -include-classes com.longtailvideo.plugins.captions.Captions -link-report='../captions-classes.xml'
