<h2>Purpose</h2>

<p>The purpose of this guide is to be a reference for you as you get started with the latest version of the <a href="http://www.longtailvideo.com/addons/plugins/84/Captions">Captions Plugin for the JW Player</a>.



<h2>Introduction</h2>

<p>The Captions plugin for JW Player supports the display of a closed captions or foreign language subtitle track with audio or video files. More creative use cases are karaoke, or the displaying of timed comments or footnotes with the video.</p>

<p>Captions are read from an external file in either the W3C recommended DFXP (TimedText) XML format or in the SubRip Text plain-text format. Alternatively, the Captions plugin can display 3GPP Text Tracks embedded into MP4 files.</p>

<p>The plugin supports <a href="http://www.longtailvideo.com/support/jw-player/jw-player-for-flash-v5/12537/xml-playlist-support">playlists</a> and can be skinned using the <a href="http://www.longtailvideo.com/support/jw-player/jw-player-for-flash-v5/12197/installing-xmlpng-skins">JW Player skinning model</a>.</p>



<h2>Configuration Options</h2>

<p>Like the <a href="http://www.longtailvideo.com/support/jw-player/jw-player-for-flash-v5/12536/configuration-options">JW Player overall</a>, the Captions plugin can be configured with options set in the embed code. The following configuration options (flashvars) are available:</p>

<dl>
<dt><b>back</b> (<em>false</em>)</dt>
<dd>By default, the player renders a thin black outline around the captions, similar to TV / DVD captions. When setting this option <b>true</b>, a black box is drawn around the captions. This background makes the captions more readeable (nice for small texts), but does set them more apart from the video.</dd>
<dt><b>file</b> (<em>undefined</em>)</dt>
<dd>Location of the captions file(s) to display. Should be the URL to a valid <a href="#dfxp">DFXP</a> or <a href="#srt">SRT</a> captions file. If your captions are embedded in your MP4 videos or if you use a playlist, this option is not needed.</dd>
<dt><b>state</b> (<em>true</em>)</dt>
<dd>Describes whether to show the captions on startup or not. The default is <b>true</b> (captions are shown). When changed, the value is saved in a cookie, so users won't have to disable the captions on every video again if they don't want them.</dd>
</dl>

<p><b>Be warned:</b> if your captions file(s) are located at another webserver than your JW Player (player.swf), you need to place a <b>crossdomain.xml</b> file in the root of that webserver. Without this crossdomain.xml, the Flash plugin will refuse to load the captions for security reasons. More info can be found <a href="http://www.longtailvideo.com/support/jw-player/jw-player-for-flash-v5/12541/crossdomain-file-loading-restrictions">in this guide</a></p>

<h4>Dock Option</h4>

<p>One option for <a href="http://www.longtailvideo.com/support/jw-player/jw-player-for-flash-v5/12536/configuration-options">the player itself</a> influences the interface of the Captions plugin. This option is called <b>dock</b> and can be set to either <em>true</em> (the default) or <em>false</em>.</p>

<ul>
<li>When set <em>true</em>, the button to show/hide captions is displayed in the *dock*, an area in the top right of the display.</li>
<li>When set <em>false</em> the button to show/hide captions is displayed in the controlbar, usually at the right side, before the mute button (this can be set in the skin).</li>
<li>Dock buttons are larger than controlbar buttons, which allows for clearer graphics to display. Dock buttons automatically fade out after a few seconds of playback.</li>
</ul>

<h4>Example</h4>

<p>Here is an example embed code of a player with the captions plugin, using the <a href="http://www.longtailvideo.com/support/jw-player/jw-player-for-flash-v5/15995/jw-embedder-reference-guide">JW Embedder </a>. The button is displayed in the dock and the captions background is not rendered:</p>

<pre>
&lt;p id="container"&gt;The player will be placed here&lt;/p&gt;
&lt;script type="text/javascript"&gt;
 jwplayer("container").setup({
   controlbar: "bottom",
   dock: true,
   file: "/static/video.mp4",
   flashplayer: "/static/player.swf",
   height: 270,
   plugins: {
     "captions-2": {
       back: false,
       file: "/static/captions.xml"
     }
   },
   width: 480
 });
&lt;/script&gt;
</pre>

<p>Here's a screenshot of this example, plus a screenshot that shows how the player looks with the dock disabled and the captions background enabled:</p>

<p><img src="http://www.longtailvideo.com/support/sites/default/files/captions_examples.png" alt="Caption plugin example setups" width="760" /></p>



<h2>Supported Formats</h2>

<p>The plugin supports two distinct text formats for loading the captions. Additionally, the plugin can display 3GPP Text Tracks embedded in MP4 files.</p>

<p>A great, easy to use tool for creating subtitles is <a href="http://www.universalsubtitles.org/">Universal Subtitles</a>. It's an online editor that allows you to create and download captions for video. The captions can be exported to both SRT and DFXP. It is also possible to edit already existing SRT or DFXP files and to quickly create translations of captions.</p>


<a name="srt"/><h4>SubRip (SRT)</h4>

<p>The <a href="http://en.wikipedia.org/wiki/SubRip">SubRip captions format</a> uses plain text files and an easy to understand formatting. Here's an example:</p>

<pre>
1
00:00:08,000 --&gt; 00:00:10,000
Nothing is going on.

2
00:00:10,500 --&gt; 00:00:12,500
Violet, please!
- I am not your babe!

3
00:00:17,000 --&gt; 00:00:20,000
You stupid cow,
look what you gone and done now, ay.
</pre>

<p>A double linebreak with enumeration is used to distinct between caption entries. Single linebreaks can be used to add breaks in the captions themselves. We recommend you stick to about 80 characters per line. This looks good with a default setup of the captions plugin. It is also the standard for TV / DVD setups.</p>

<p><em>Note your SRT files should be saved using UTF8 encoding in order to correctly display special characters (accents, but also Chinese or Hebrew).</em></p>

<a name="dfxp"/><h4>DFXP (TimedText)</h4>

<p>The <a href="http://www.w3.org/TR/2010/PR-ttaf1-dfxp-20100914/">DFXP format</a> (Distribution Format Exchange Profile) is an XML format for storing closed captions or subtitles. Another name for this format is TTML (Timed Text Markup Language). Here is an example:</p>

<pre>
&lt;tt xmlns="http://www.w3.org/2006/10/ttaf1"&gt;
 &lt;body&gt;
   &lt;div&gt;
     &lt;p begin="00:00:08" end="00:00:10"&gt;- Nothing is going on.&lt;/p&gt;
     &lt;p begin="00:00:10.5" end="00:00:12.5"&gt;You liar!&lt;/p&gt;
     &lt;p begin="00:00:13.5" end="00:00:15"&gt;Are you?&lt;/p&gt;
     &lt;p begin="00:00:17" end="00:00:20"&gt;Violet, please!&lt;br/&gt;- I am not your babe!&lt;/p&gt;
     &lt;p begin="00:00:34" end="00:00:36"&gt;Vi, please.&lt;br/&gt;- Leave me alone!&lt;/p&gt;
   &lt;/div&gt;
 &lt;/body&gt;
&lt;/tt&gt;
</pre>

<p>The <b>&lt;br/&gt;</b> tags inside the paragraphs identify linebreaks in the captions. With the default fontsize, about 80 characters fit on a single line. This is similar to TV / DVD standards.</p>

<p><em>Note your DFXP files should use UTF8 encoding in order to correctly display special characters (accents, but also Chinese or Hebrew).</em></p>
<p><em>Note that, though the format supports multiple languages (by using multiple <em>&lt;div lang='xx'&gt;</em> elements), only one language/div is supported by the Captions plugin for now.</em></p>

<a name="mp4"><h4>MP4 Text Tracks</h4>

<p>The MP4 media container has the ability to <a href="http://en.wikipedia.org/wiki/MPEG-4_Part_17">embed timed text tracks</a>, in addition to e.g. a video and an audio track. This text data, often referred to as MPEG-4 Timed Text (or 3GPP Timed Text), is automatically picked up and displayed by the Captions plugin. Other notable players that support these MP4 text tracks are the iPad/iPhone, Quicktime and VLC.</p>

<p>A great tool for embedding captions in MP4 files is <a href="http://handbrake.fr/">Handbrake </a>. This free, cross-platform encoding tool can encode captions from external SRT files to MP4 and migrate captions from DVD to MP4. See <a href="http://trac.handbrake.fr/wiki/Subtitles">its documentation</a> for more info.</p>

<p><em>Note the Captions plugin ignores the <b>trackid</b> of a Timed Text track. In other words, the plugin only supports a single text track, since all incoming textdata is simply printed in the captions area.</em></p>




<h2>Styling the Captions</h2>

<p>It is possible to change the styling of the captions overall, per line and even per text snippet. The following six style properties can be changed (CSS developers will be familiar with them):</p>

<ul>
<li><b>color</b>: can be any hexadecimal color value (e.g. <em>#FFCC00</em>).</li>
<li><b>fontFamily</b>: can be any font installed <a href="http://www.fonttester.com/web_safe_fonts.html">on a user's computer</a> (e.g. <em>Georgia,serif</em>).</li>
<li><b>fontSize</b>: can be any size in pixels (e.g. <em>18</em>). Note the default fontsize is <em>14</em>.</li>
<li><b>fontStyle</b>: can be set to <em>normal</em> (the default) or <em>italic</em>.</li>
<li><b>fontWeight</b>: can be set to <em>normal</em> (the default) or <em>bold</em>.</li>
<li><b>textDecoration</b>: can be set to <em>none</em> (the default) or <em>underline</em>.</li>
</ul>

<h4>Configuration options</h4>

<p>The easiest way to style the captions is through configuration options. Like the <b>back</b>, <b>file</b> and <b>state</b> options, each of the six style rules can be added to the player embed code. This functionality works for styling both SRT, DFXP and MP4 captions.</p>

<h4>DFXP Styling</h4>

<p>DFXP files contain two different mechanisms for styling:<p>

<ol>
<li>The <em>&lt;head&gt;</em> of a DFXP file can contain one or more style elements. These elements are all given an ID. The individual captions paragraphs can be linked to the style rules using the <em>style="xx"</em> attribute.</li>
<li>Inside captions paragraphs, text snippets can be wrapped in <em>&lt;span&gt;</em> elements. These spans can contain a <em>style="xx"</em> attribute, or even list individual style rules (like <em>fontWeight="bold"</em>).</li>
</ol>

<p>Here is the example DFXP file, containing all possible styling methods. Note the individual rules (color, fontSize,  ...) need to be prefixed with a <b>tts:</b> namespace identifier. The according namespace declaration (<em>xmlns:tts</em>) needs to be set in the header of the file to make it a valid DFXP file with TTS (Timed Text Styling) rules:</p>

<pre>
&lt;tt xmlns="http://www.w3.org/2006/10/ttaf1" xmlns:tts="http://www.w3.org/2006/04/ttaf1#styling"&gt;
  &lt;head&gt;
   &lt;styling&gt;
      &lt;style id="normal" tts:fontSize="15" /&gt;
      &lt;style id="warning" tts:color="#FF0000" tts:fontWeight="bold" tts:fontSize="20" /&gt;
   &lt;/styling&gt;
  &lt;/head&gt;
  &lt;body&gt;
    &lt;div&gt;
      &lt;p begin="00:00:08" end="00:00:10" style="normal"&gt;- Nothing is going on.&lt;/p&gt;
      &lt;p begin="00:00:10.5" end="00:00:12.5" style="warning"&gt;You liar!&lt;/p&gt;
      &lt;p begin="00:00:17" end="00:00:20" style="normal"&gt;Violet, please!&lt;br/&gt;
          - I am &lt;span style="warning"&gt;not&lt;/span&gt; your babe!
      &lt;/p&gt;
      &lt;p begin="00:00:24" end="00:00:29" style="normal"&gt;
          You &lt;span tts:fontStyle="italic"&gt;stupid cow&lt;/span&gt;, look what you did.&lt;/p&gt;
    &lt;/div&gt;
  &lt;/body&gt;
&lt;/tt&gt;
</pre>

<p><em>Note the captions plugin does not support nested &lt;span&gt; tags.</em></p>

<h4>SRT Styling</h4>

<p>Officially, the SRT format does not support any styling by itself. The Captions plugin does support styling of SRT though, by using a few basic HTML tags. Please use this sparsely, since <b>compatibility with other tools will likely break</b>. When you intend to do a lot of styling, the DFXP format is a much better option.</p>

<p>That said, the tags &lt;b&gt;, &lt;i&gt; and &lt;u&gt; can be used for setting the weight, style and decoration. The tag &lt;font color="#ff0000" face="Courier" size="18"&gt; can be used to set the color, family and size. Here is a small SRT snippet with some inline HTML for styling:</p>

<pre>
1
00:00:08,000 --&gt; 00:00:10,000
&lt;b&gt;Nothing&lt;/b&gt; is going on.

2
00:00:10,500 --&gt; 00:00:12,500
Violet, &lt;i&gt;&lt;u&gt;please&lt;/u&gt;&lt;/i&gt;!
&lt;font face="Courier" size="20" color="#FF0000"&gt;I am not your babe!&lt;/font&gt;
</pre>



<h2>Playlist support</h2>

<p>Captions can be assigned to one or more videos in a playlist. You can mix SRT, DFXP and MP4 captions with videos that have no captions in a single feed. Since none of the XML playlist formats define elements for linking to captions files, the captions should be set using the <a href="http://www.longtailvideo.com/support/jw-player/jw-player-for-flash-v5/12537/xml-playlist-support">JWPlayer XML namespace</a>. In practice, the namespace is enabled by:</p>


<ul>
<li>Setting an <b>xmlns:jwplayer</b> attribute in the main XML tag.</li>
<li>Prefixing the XML elements with <b>jwplayer:</b>, e.g. <em>&lt;jwplayer:sharing.code&gt;</em></li>
</ul>


<h4>Example</h4>

<p>Here is an example playlist. The <em>captions.file</em> option is set for each entry:</p>

<pre>
&lt;rss version="2.0" xmlns:jwplayer="http://developer.longtailvideo.com/"&gt;
  &lt;channel&gt;
    &lt;title&gt;Example RSS playlist with captions&lt;/title&gt;

    &lt;item&gt;
      &lt;title&gt;Coronation Street&lt;/title&gt;
      &lt;description&gt;This entry has external DFXP captions&lt;/description&gt;
      &lt;enclosure url="/static/corrie.flv" /&gt;
      &lt;jwplayer:captions.file&gt;/static/corrie.xml&lt;/jwplayer:captions.file&gt;
    &lt;/item&gt;

    &lt;item&gt;
      &lt;title&gt;Big Buck Bunny&lt;/title&gt;
      &lt;description&gt;This entry has external SRT captions.&lt;/description&gt;
      &lt;enclosure url="/static/bunny.mp4" /&gt;
      &lt;jwplayer:captions.file&gt;/static/bunny.srt&lt;/jwplayer:captions.file&gt;
    &lt;/item&gt;

  &lt;/channel&gt;
&lt;/rss&gt;
</pre>

<p>Note the same <a href="http://www.longtailvideo.com/support/jw-player/jw-player-for-flash-v5/12541/crossdomain-file-loading-restrictions">crossdomain file restrictions</a> that apply to captions also apply to playlists. Therefore make sure you have placed a <em>crossdomain.xml</em> on the webserver that hosts your captions and playlist, if they are different from the player's webserver.</p>




<h2>Skinning</h2>

<p>The Captions plugin includes support for styling its controlbar or dock button through the <a href="http://www.longtailvideo.com/support/jw-player/jw-player-for-flash-v5/14/skinning-the-jw-player-5">JW Player Skinning Model</a>. Styling of the captions themselves must be included in the DFXP or SRT captions file.</p>

<p>Due to current limitations in the PNG skinning model, not all desired results can be achieved:</p>

<ul>
<li>Rollovers cannot be set, for neither the <em>dock</em> nor the <em>controlbar</em> button.</li>
<li>Alternative icons (for the <em>off</em> state) cannot be set. Instead, the plugin fades the icon when disabled.</li>
<li>Contrary to regular controlbar buttons, the captions controlbar button is given a margin of 5px to its left and to its right. You should take this into account, removing any margins you have in the button graphics.</li>
</ul>

<p>Here's an example of a skin that includes custom Captions graphics, using the <a href="http://www.longtailvideo.com/addons/skins/25/Stijl">freely available Stijl skin</a>:</p>

<p><img src="http://www.longtailvideo.com/support/sites/default/files/stijlskin.png" alt="Skinned captions plugin example" width="760"/></p>


<h4>XML Block</h4>

<p>Here is the XML code block you should include in your PNG skin to style the Captions plugin:</p>

<pre>
&lt;component name="captions"&gt;
  &lt;elements&gt;
    &lt;element name="controlbarButton" src="controlbar.png" /&gt;
    &lt;element name="dockIcon" src="dock.png" /&gt;
  &lt;/elements&gt;
&lt;/component&gt;
</pre>

<p>The two PNG images (<em>controlbar.png</em> and <em>dock.png</em>) should be placed in a <b>captions</b> subdirectory of the main skin directory.</p>




<h2>Changelog</h2>


<h4>Version 2.0</h4>

<ul>
<li>Migrated plugin to V5 API; new Captions.as implements IPlugin and works natively with JW 5+. As of 2.0, the plugin doesn't work with the 4.x player anymore.</li>
<li>Added support for styling using TimedText Styles (color, fontFamily, fontSize, fontWeight, fontStyle, fontWeight). Styles can be defined both inline and in the &lt;head&gt; of a TimedText file.</li>
<li>Added besic support for PNG skinning, by allowing a custom controlbar and dock icon.</li>
<li>Added support for error handling. 404 not founds, crossdomain security errors and file parsing errors are caught.</li>
<li>Added support for controlbars placed over the display, by moving the captions further up.</li>
<li>Fixed an issue that left the captions 'back' element visible as a thin bar when there where no captions.</li>
<li>Fixed an issue where captions loaded though the configuration options were ignored when using a single-track playlist.</li>
</ul>

<h4>Version 2.1</h4>

<ul>
<li>Added support for styling through configuration options.</li>
<li>Added support for linking spans to styles in DFXP.</li>
<li>Enhanced the <em>back</em> option, drawing the black box around the text instead of around the entire video bottom.</li>
<li>Fixed the opacity toggle in the controlbar button. Now, only the icon is toggled instead of the whole button.</li>
<li>Fixed an issue in which selection of the caption text would change the styling.</li>
<li>Fixed an issue in which captions would sit on top of controlbars in the <em>over</em> state.</li>
</ul>

<p>An annoying known bug is the fact the Captions area is stealing mouse focus from the video screen. The plugin contains the neccessary <em>mouseChildren=false</em> and <em>mouseEnabled=false</em> settings, but the mouse doesn't pass nontheless. Suggestions are welcome!</p>
