package com.jeroenwijering.plugins {


import com.jeroenwijering.events.*;

import flash.display.*;
import flash.events.*;
import flash.geom.Rectangle;
import flash.text.*;

/**
* A simple plugin that displays a search bar.
**/
public class InfoBox extends MovieClip implements PluginInterface {


	/** Reference to the background clip. **/
	private var back:Sprite;
	/** Default colors. **/
	private var colors:Array = new Array('FFFFFF','000000');
	/** Reference to the graphics. **/
	public var config:Object = {
		color:undefined,
		size:85,
		position:'bottom'
	};
	/** Reference to the title textfield. **/
	private var title:TextField;
	/** Reference to the description textfield. **/
	private var description:TextField;
	/** Reference to the author textfield. **/
	private var author:TextField;
	/** Reference to the date textfield. **/
	private var date:TextField;
	/** Reference to the View of the player. **/
	private var view:AbstractView;


	/** Constructor. **/
	public function InfoBox():void {};


	/** Build the clip graphics. **/
	private function build():void {
		back = new Sprite();
		back.graphics.beginFill(uint('0x'+colors[0]));
		back.graphics.drawRect(0,0,400,40);
		addChild(back);
		title = new TextField();
		title.defaultTextFormat = new TextFormat("_sans",13,uint('0x'+colors[1]),true);
		title.x = title.y = 5;
		addChild(title);
		description = new TextField();
		description.defaultTextFormat = new TextFormat("_sans",11,uint('0x'+colors[1]));
		description.multiline = description.wordWrap = true;
		description.x = 5;
		description.y = 25;
		addChild(description);
		author = new TextField();
		author.defaultTextFormat = new TextFormat("_sans",10,uint('0x'+colors[1]));
		author.x = 5;
		addChild(author);
		date = new TextField();
		date.defaultTextFormat = new TextFormat("_sans",10,uint('0x'+colors[1]));
		addChild(date);
	};



	/** The initialize call is invoked by the player View. **/
	public function initializePlugin(vie:AbstractView):void {
		view = vie;
		view.addControllerListener(ControllerEvent.RESIZE,resizeHandler);
		if(view.config['backcolor']) { colors[0] = view.config['backcolor']; }
		if(view.config['frontcolor']) { colors[1] = colors[2] = view.config['frontcolor']; }
		if(config['color']) { colors[1] = config['color']; }
		view.addControllerListener(ControllerEvent.PLAYLIST,itemHandler);
		view.addControllerListener(ControllerEvent.ITEM,itemHandler);
		build();
		resizeHandler();
		if(config['position'] == 'over') {
			back.alpha = 0.8;
			mouseEnabled = false;
			mouseChildren = false;
			view.addModelListener(ModelEvent.STATE,stateHandler);
		}
	};


	/** Repopulate the texts on item change. **/
	private function itemHandler(evt:ControllerEvent):void {
		var itm:Object = view.playlist[view.config['item']];
		title.text = itm['title'] + ' ';
		description.text = itm['description'] + ' ';
		author.htmlText = '<b>Source:</b> ' + itm['author'] + ' ';
		date.htmlText = '<b>Added:</b> ' + itm['date'] + ' ';
	};


	/** Handle a resize. **/
	private function resizeHandler(evt:ControllerEvent=undefined):void {
		back.width = view.config['width'];
		if(config['width']) {
			visible = config['visible'];
			x = config['x'];
			y = config['y'] + config['height'] - config['size'];
			back.width = config['width'];
			back.height = config['size'];
		}
		title.width = description.width = back.width - 10;
		description.height = back.height - 45;
		author.y = date.y = back.height - 25;
		author.width = date.width = back.width/2 - 5;
		date.x = back.width/2;
	};


	/** When the box position is "over", the box is hidden when the video plays. **/
	private function stateHandler(evt:ModelEvent):void {
		switch (evt.data.newstate) { 
			case ModelStates.BUFFERING:
			case ModelStates.PLAYING:
				visible = false;
				break;
			default:
				visible = true;
				break;
		}
	};


}


}