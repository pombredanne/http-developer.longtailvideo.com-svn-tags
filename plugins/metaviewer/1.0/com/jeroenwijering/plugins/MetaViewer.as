/**
* Prints the metadata enclosed in videos onscreen, which is useful for metadata checking. 
**/
package com.jeroenwijering.plugins {


import com.jeroenwijering.events.*;
import com.jeroenwijering.utils.Scrollbar;

import flash.display.*;
import flash.text.*;


public class MetaViewer extends MovieClip implements PluginInterface {


	/** Reference to the solid backcolor. **/
	public var back:Sprite;
	/** Object with configuration values. **/
	public var config:Object = {
		size:200
	};
	/** Reference to the field that prints all params. **/
	private var field:TextField;
	/** Scrollbar clip for the metadata. **/
	private var scrollbar:Scrollbar;
	/** Reference to the View of the player. **/
	private var view:AbstractView;


	/** Constructor; nothing going on. **/
	public function MetaViewer() {
		buildStage();
	};


	/** Build all stage graphics. **/
	private function buildStage():void {
		back = new Sprite();
		back.graphics.beginFill(0x000000,0.8);
		back.graphics.drawRect(0,0,400,300);
		addChild(back);
		field = new TextField();
		field.defaultTextFormat = new TextFormat('_sans',11,0xFFFFFF);
		field.multiline = true;
		field.wordWrap = true;
		field.autoSize = TextFieldAutoSize.LEFT;
		field.x = field.y = 20;
		field.width = 360;
		field.height = 260;
		field.text = "No metadata received yet...";
		addChild(field);
		scrollbar = new Scrollbar(field);
		visible = false;
	};


	/** Print the metadata in a list. **/
	private function dataHandler(evt:ModelEvent):void {
		if(evt.data.type == 'metadata' || evt.data.type == 'id3') {
			var arr:Array = new Array();
			for (var itm:String in evt.data) {
				switch(itm) {
					case 'type':
					case 'id':
					case 'version':
					case 'client':
						break;
					case 'keyframes':
						var frs:String = '» '+itm+' ('+evt.data[itm]['times'].length+' entries):';
						for (var i:Number=0; i<evt.data[itm]['times'].length; i++) {
							frs += '\n   '+evt.data[itm]['times'][i]+'s - '+evt.data[itm]['filepositions'][i]+'b';
						}
						arr.push(frs);
						break;
					case 'seekpoints':
						var spt:String = '» '+itm+' ('+evt.data[itm].length+' entries):';
						for (var j:Number=0; j<evt.data[itm].length; j++) {
							spt += '\n   '+evt.data[itm][j]['time']+'s - '+evt.data[itm][j]['offset']+'b';
						}
						arr.push(spt);
						break;
					case 'trackinfo':
						arr.push('» '+itm+': '+evt.data[itm].length+' entries');
						break;
					default:
						if(evt.data[itm] != '') {
							arr.push('» '+itm+': '+evt.data[itm]);
						}
						break;
				}
			}
			arr.sort();
			field.text = arr.join('\n');
			resizeHandler();
			visible = true;
		}
	};


	/** The initialize call is invoked by the player View. **/
	public function initializePlugin(vie:AbstractView):void {
		view = vie;
		view.addControllerListener(ControllerEvent.RESIZE,resizeHandler);
		view.addModelListener(ModelEvent.META,dataHandler);
		resizeHandler();
	};


	/** Handle a resize. **/
	private function resizeHandler(evt:ControllerEvent=null):void {
		var wid:Number = view.config['width'];
		var hei:Number = view.config['height'];
		if(config['width']) {
			x = config['x'];
			y = config['y'];
			wid = config['width'];
			hei = config['height'];
		}
		back.width = wid;
		back.height = hei;
		field.width = wid-50;
		scrollbar.draw(hei-40);
	};


};


}