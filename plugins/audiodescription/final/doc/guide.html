<h2>Purpose</h2>

<p>The purpose of this guide is to be a reference for you as you get started with the latest version of the <a href="http://www.longtailvideo.com/addons/plugins/85/Audio-Description">Audio Description Plugin</a> for the JW Player.</p>



<h2>Introduction</h2>

<p>The Audiodescription plugin for JW Player supports playback of a closed audiodescription track (a separate MP3 file,  kept in sync with the video). Audiodescriptions can be used for accessibility purposes (people who have bad sight or are blind), for educational purposes (explanations of scenes or situations) or for <em>making of</em> style comments (from e.g. the director of a movie).</p>

<p>The plugin supports playlists. Its on/off toggle can be skinned using the JW Player PNG skinning model. The plugin also offers a setting that ducks (80% mutes) the original audio when there's audiodescription data. This is useful in situations where voices from the original and the audiodescription tracks overlap.</p>



<h2>Configuration Options</h2>

<p>Like the <a href="http://www.longtailvideo.com/support/jw-player/jw-player-for-flash-v5/12536/configuration-options">JW Player at large</a>, the Audiodescription plugin can be configured with options set in the embed code. The following configuration options (flashvars) are available:</p>

<dl>
<dt>file</dt>
<dd>Location of the audiodescription file to playback. This should be the URL to an MP3 file with the same length as the video. Such an MP3 file generally contains both narrative and blank (no sound) sections.</dd>
</dl>

<p><em>Note the MP3 file should be in a constant bitrate, 44.1 kHz or 22.05 kHz, mono or stereo format. Other values (especially variable bitrates) may lead Flash to playback the sound chipmunked.</em></p>

<dl>
<dt>state</dt>
<dd>Describes whether to play the audiodescription on startup or not. The default is **true** (audiodescription is played). Can be set **false** as well.</dd>
</dl>

<p><em>Note the state option is also saved in a cookie. When a user disables the audiodescription on one video, they will be disabled by default on subsequent videos he watches on the same site. The configuration option overrides the cookie.</em></p>

<dl>
<dt>volume</dt>
<dd>The audiodescription does not respect the overall player volume setting. Instead, you can use this volume option to independently set the audiodescription's volume (0 to 100). Set it high (100) if the AD is relatively soft, or set it low (10) if the AD is relatively loud.</dd>
</dl>

<p>If the issue is not volume, but overlapping narrations, check out the ducking functionality.</p>

<h4>Dock Option</h4>

<p>One option for <a href="http://www.longtailvideo.com/support/jw-player/jw-player-for-flash-v5/12536/configuration-options">the player itself</a> greatly influences the interface of the Audiodescription plugin. This option is called <b>dock</b> and can be set to either <em>true</em> (the default) or <em>false</em>.</p>

<ul>
<li>When set <em>true</em>, the button to play/mute audiodescriptions is displayed in the <b>dock</b>, an area in the top right of the display.</li>
<li>When set <em>false</em> the button to play/mute audiodescriptions is displayed in the <b>controlbar</b>, usually at the right side, before the mute button (this can be set in the skin).</li>
</ul>

<p>Dock buttons are larger than controlbar buttons, which allows for clearer graphics to display. Dock buttons automatically fade out after a few seconds of playback.</p>

<h4>Example</h4>

<p>Here is an example embed code of a player using the audiodescriptions plugin. This example uses the <a href="http://www.longtailvideo.com/support/jw-player/jw-player-for-flash-v5/15995/jw-embedder-reference-guide">JW Embedder</a>:</p>

<pre>
&lt;p id="container"&gt;The player will be placed here&lt;/p&gt;

&lt;script type="text/javascript"&gt;
 jwplayer("container").setup({
   file: "/static/video.mp4",
   flashplayer: "/static/player.swf",
   height: 270,
   plugins: { 
     "audiodescription-2": { 
       file: "/static/audiodescription.mp3"
     }
  },
  width: 480
 });
&lt;/script&gt;
</pre>




<h2>Ducking</h2>

<p>Version 2 of the Audiodescription plugin introduced a sophisticated feature to avoid overlapping narrations of the original and the audiodescription tracks. With ducking enabled, the audiodescription plugin will analyze the waveform of the audiodescription MP3. It then <b>ducks</b> (lowers) the volume of the original audio when it detects sound in the audiodescription. When there's no sound in the audiodescription, the volume of the original track is upped again.</p>

<p>The ducking feature takes over the volume control of the player. If there's no audiodescription wavedata, the original audio is kept at the original volume. If there is wavedata, the original audio is faded down (<em>80%</em>). This back-and-forth goes on until the end of the video is reached.</p>

<p>Ducking can be enabled with the following option:</p>

<dl>
<dt>ducking</dt>
<dd>Set this option <b>true</b> to enable the ducking functionality.</dd>
<dt>debug</dt>
<dd>Set this separate option to <b>true</b> to have the plugin render a visualization of the ducking process (for debugging). It can be seen in the image below. The green line shows the waveform of the audiodescription and the red line shows the forced volume level of the original audio.</dd>
</dl>

<p><img src="http://www.longtailvideo.com/support/sites/default/files/ducking.png" alt="Audio Description ducking visualization." /></p>
   
<p>Since calculating the waveform is pretty CPU-intensive, it is not recommended to use ducking for videos that are over a few minutes in length. Expect a few hundred milliseconds of calculation for every minute of video.</p>




<h2>Supported Formats</h2>

<p>The audiodescription plugin solely supports a single MP3 file per video. The plugin does not support other formats (like WAV or AAC). It also doesn't support compositions of MP3 files (e.g. using SMIL).</p>

<p>Moreover, Flash is pretty picky when it comes to correctly playing back MP3 files. Only a small subset of MP3 is supported. For your audiodescription to correctly playback, we recommend the following settings:</p>

<ul>
<li><b>bitrate</b>: 64, 96, 128, 160 or 192 kbps constant bit rate. 96 kbps seems to be a good filesize/quality tradeoff.</li>
<li><b>channels</b>: mono or joint stereo.</li>
<li><b>sample frequency</b>: 44.1kHz or 22.05kHz.</li>
</ul>

<p>When your MP3 file is not supported by Flash, you'll be hearing the infamous <b>chipmunks</b> effect: the pitch of the audio is too high (or too low).</p>

<h4>Creating audiodescriptions</h4>

<p>Audiodescriptions are best created with a simple audio/video editor that supports recording of audio through a line-in or microphone. For example:</p>

<ul>
<li><b>Windows Movie Maker</b> on Windows.</li>
<li><b>iMovie</b> on the MAC.</li>
</ul>

<p>Take the following steps to create the audiodescription MP3:</p>

<ol>
<li>Load the original video into your editor.</li>
<li>Using e.g. a cheap headset, record the voiceovers you want to insert.</li>
<li>Position the voiceovers in such a way it overlaps as little as possible with the original audio.</li>
<li><b>Mute</b> the original audio.</li>
<li>Export the video, e.g. to an MP4 or WMV or MOV video.</li>
<li>Use the (simple, free and crossplatform) Miro Video Converter tool to extract the audio as an MP3 file.</li>
</ol>

<p>When done, you have an audiodescription MP3 of your video that can be directly loaded into the JW Player.</p>

<p><em>Note you should not keep your microphone running when you don't speak. Background noise will also trigger the ducking filter, resulting in the video's sound getting muted because of the background noise.</em></p>



<h2>Playlist support</h2>

<p>Audio descriptions can be assigned to one or more videos in a playlist. You can even combine videos with and videos without an audiodescription in a single feed.</p>
<p>Since none of the existing XML playlist formats define elements for linking to audiodescription files, they should be set using the <a href="http://www.longtailvideo.com/support/jw-player/jw-player-for-flash-v5/12537/xml-playlist-support">JWPlayer XML namespace</a>. In practice, the namespace is enabled by:</p>

<ul>
<li>Setting an <b>xmlns:jwplayer</b> attribute in the main XML tag.</li>
<li>Prefixing the XML elements with <b>jwplayer:</b>, e.g. <em>&lt;jwplayer:audiodescription.file&gt;</em>.</li>
</ul>

<h4>Example</h4>

<p>Here is an example playlist that includes audiodescription links. The <em>audiodescription.file</em> option is now set for each entry:</p>

<pre>
&lt;rss version="2.0" xmlns:jwplayer="http://developer.longtailvideo.com/"&gt;
 &lt;channel&gt;
   &lt;title&gt;Example RSS playlist with captions&lt;/title&gt;

   &lt;item&gt;
     &lt;title&gt;Coronation Street&lt;/title&gt;
     &lt;description&gt;This entry has an audiodescription&lt;/description&gt;
     &lt;enclosure url="/static/corrie.flv" /&gt;
     &lt;jwplayer:audiodescription.file&gt;/static/corrie.mp3&lt;/jwplayer:audiodescription.file&gt;
   &lt;/item&gt;

   &lt;item&gt;
     &lt;title&gt;Big Buck Bunny&lt;/title&gt;
     &lt;description&gt;This entry has another audiodescription&lt;/description&gt;
     &lt;enclosure url="/static/bunny.mp4" /&gt;
     &lt;jwplayer:audiodescription.file&gt;/static/bunny.mp3&lt;/jwplayer:audiodescription.file&gt;
   &lt;/item&gt;

 &lt;/channel&gt;
&lt;/rss&gt;
</pre>

<p>For security purposes, so-called <a href="http://www.longtailvideo.com/support/jw-player/jw-player-for-flash-v5/12541/crossdomain-file-loading-restrictions">crossdomain file restrictions</a> apply to playlists. Therefore make sure you have placed a <em>crossdomain.xml</em> on the webserver that hosts your playlist, if they are different from the <em>player.swf</em>'s webserver.</p>



<h2>Skinning</h2>

<p>The Audiodescription plugin includes support for styling its controlbar or dock button through the JW Player <a href="http://www.longtailvideo.com/support/jw-player/jw-player-for-flash-v5/14/skinning-the-jw-player-5">PNG Skinning Model</a>. Styling of the ducking debug is not possible (end users should not see it).</p>

<p>Due to current limitations in the PNG skinning model, not all desired results can be achieved:</p>

<ul>
<li>Rollovers cannot be set, for neither the dock nor the controlbar button.</li>
<li>Alternative icons (for the <b>off</b> state) cannot be set, for neither the dock nor the controlbar button.</li>
<li>Contrary to regular controlbar buttons, the audiodescription controlbar button is given a margin of 5px to its left and to its right. You should take this into account, removing any margins you have in the button graphics.</li>
</ul>

<p>Here's an example of a skin that includes custom Audiodescription graphics:</p>

<p><img src="http://www.longtailvideo.com/support/sites/default/files/skinning_0.png" alt="Skinned Audio Description Example" /></p>

<p>This skin, called <b>Stijl</b>, can be <a href="http://www.longtailvideo.com/addons/skins/25/Stijl">freely downloaded here</a>.</p>

<h4>XML Block</h4>

<p>Here is the XML code block you should include in your PNG skin to style the Audiodescription plugin:</p>

<pre>
&lt;component name="audiodescription"&gt;
 &lt;elements&gt;
   &lt;element name="controlbarButton" src="controlbar.png" /&gt;
   &lt;element name="dockIcon" src="dock.png" /&gt;
 &lt;/elements&gt;
&lt;/component&gt;
</pre>

<p>The two PNG images (<em>controlbar.png</em> and <em>dock.png</em>) should be placed in an <b>audiodescription</b> subdirectory of the main skin directory.</p>



<h2>Changelog</h2>

<h4>Version 2.0</h4>

<ul>
<li>Migrated plugin to V5 API; new Audiodesription.as implements IPlugin and works natively with JW 5+. As of 2.0, the plugin doesn't work with the 4.x player anymore.</li>
<li>Added support for ducking of the original audio. A fairly sophisticated feature that might ease the authoring of high volumes of audio descriptions. Do note it's pretty CPU-heavy and should only be used for short clips.</li>
<li>Added basic support for PNG skinning, by allowing a custom controlbar and dock icon.</li>
<li>Fixed an issue that threw an error when the playlistitem was unloaded while the MP3 was still transferring.</li>
<li>Fixed an issue where the audiodescription of a previous video would still be in the player config after a LOAD event.</li>
</ul>

<h4>Version 2.1</h4>

<ul>
<li>Fixed the opacity toggle in the controlbar button. Now, only the icon is toggled instead of the whole button.</li>
<li>The plugin now resets the player volume to its original state after a ducking session.</li>
<li>When ducking is enabled, the MP3 is loaded and examined on player load, which speeds up processing.</li>
</ul>