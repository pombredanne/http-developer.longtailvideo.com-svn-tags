var settings = {
	/** Players to list. **/
	players: {
		'5.3':'players/5.3.swf',
		'5.2':'players/5.2.swf',
		'5.1':'players/5.1.swf',
		'5.0':'players/5.0.swf'
	},
	/** Plugins to list. **/
	plugins: {
		audiodescription:'../audiodescription.swf'
	},
	/** Skins to list. **/
	skins: {
		none:'',
		bekle:'skins/bekle.zip',
		glow:'skins/glow.zip',
		modieus:'skins/modieus.zip',
		stijl:'skins/stijl.zip',
	},
	/** All the setup examples with their flashvars. **/
	examples: {
		'':{},
		'Local audiodescription': {
			plugins:'audiodescription',
			file:'../assets/corrie.flv',
			image:'assets/corrie.jpg',
			'audiodescription.file':'assets/corrie.mp3'
		},
		'Online audiodescription': {
			dock:'false',
			plugins:'audiodescription',
			file:'../assets/ns.mp4',
			image:'assets/ns.jpg',
			'audiodescription.file':'http://content.bitsontherun.com/videos/RCJfkBlp-393434.mp3'
		},
		'Audiodescription with ducking': {
			dock:'false',
			plugins:'audiodescription',
			file:'../assets/ns.mp4',
			image:'assets/ns.jpg',
			'audiodescription.file':'http://content.bitsontherun.com/videos/RCJfkBlp-393434.mp3',
			'audiodescription.ducking':true,
			'audiodescription.debug':true
		},
		' ':{},
		'Playlist with audiodescriptions': {
			file:'assets/audiodescriptions.xml',
			height:240,
			playlist:'right',
			'playlist.thumbs':false,
			playlistsize:240,
			plugins:'audiodescription',
			width:720
		},
		'Playlist with global audiodescription': {
			plugins:'audiodescription',
			file:'http://content.bitsontherun.com/jwp/AKIUnF9G.xml',
			'audiodescription.file':'assets/ns.mp3'
		},
		'Playlist with inline audiodescription (try LOAD of title)': {
			plugins:'audiodescription',
			file:'assets/single.xml',
			title:'http://content.bitsontherun.com/jwp/yj1shGJB.xml'
		},
		'  ':{},
		'Skinned audiodescription': {
			skin:'stijl',
			plugins:'audiodescription',
			file:'../assets/corrie.flv',
			image:'assets/corrie.jpg',
			'audiodescription.file':'assets/corrie.mp3'
		}
	}
}
