/**
* Plugin for playing a closed audiodescription with a video.
**/
package com.jeroenwijering.plugins {


import com.jeroenwijering.events.*;
import com.jeroenwijering.utils.Logger;

import flash.display.*;
import flash.events.*;
import flash.media.*;
import flash.net.*;


public class Audiodescription extends MovieClip implements PluginInterface {


	[Embed(source="../../../controlbar.png")]
	private const ControlbarIcon:Class;
	[Embed(source="../../../dock.png")]
	private const DockIcon:Class;


	/** Reference to the dock button. **/
	private var button:MovieClip;
	/** List with configuration settings. **/
	public var config:Object = {
		file:undefined,
		state:true,
		volume:90
	}
	/** Reference to the MVC view. **/
	private var view:AbstractView;
	/** Reference to the icon. **/
	private var icon:Bitmap;
	/** sound object to be instantiated. **/
	private var sound:Sound;
	/** Sound channel object. **/
	private var channel:SoundChannel;


	/** Constructor; not much going on. **/
	public function Audiodescription():void {};


	/** Initing the plugin. **/
	public function initializePlugin(vie:AbstractView):void {
		view = vie;
		view.addControllerListener(ControllerEvent.ITEM,itemHandler);
		view.addModelListener(ModelEvent.TIME,timeHandler);
		view.addModelListener(ModelEvent.STATE,stateHandler);
		if(view.config['dock']) {
			button = view.getPlugin('dock').addButton(DisplayObject(new DockIcon()),'is on',clickHandler);
		} else if(view.getPlugin('controlbar')) {
			icon = new ControlbarIcon();
			view.getPlugin('controlbar').addButton(icon,'audiodescription',clickHandler);
		}
		setState(config['state']);
	};


	/** Clicking the  hide button. **/
	private function clickHandler(evt:MouseEvent):void {
		setState(!config['state']);
	};


	/** Check for captions with a new item. **/
	private function itemHandler(evt:ControllerEvent=null):void {
		var file:String;
		if (view.playlist[view.config['item']]['audiodescription.file']){
			file = view.playlist[view.config['item']]['audiodescription.file'];
		} else if (view.playlist[view.config['item']]['audiodescription']){
			file = view.playlist[view.config['item']]['audiodescription']; 
		} else if(view.config['audiodescription.file']) {
			file = view.config['audiodescription.file'];
		} else if(view.config['audio']) {
			// Legacy support
			file = view.config['audio'];
		}
		if(file) {
			config['file'] = file;
		}
	};


	/** Turn the audiodescription on/off. **/
	public function setState(stt:Boolean):void {
		config['state'] = stt;
		var cke:SharedObject = SharedObject.getLocal('com.jeroenwijering','/');
		cke.data['audiodescription.state'] = stt;
		cke.flush();
		setVolume();
		if(stt) { 
			if(button) { 
				button.field.text = 'is on'; 
			} else {
				icon.alpha = 1;
			}
		} else {
			if(button) { 
				button.field.text = 'is off'; 
			} else {
				icon.alpha = 0.3;
			}
		}
	};


	/** Set the volume level. **/
	private function setVolume():void {
		var trf:SoundTransform = new SoundTransform(config['volume']/100);
		if(!config['state']) { trf.volume = 0; }
		if(channel) { channel.soundTransform = trf; }
	};


	/** The statehandler manages audio pauses. **/
	private function stateHandler(evt:ModelEvent):void {
		switch(evt.data.newstate) {
			case ModelStates.PAUSED:
			case ModelStates.COMPLETED:
			case ModelStates.IDLE:
				if(channel) {
					channel.stop();
				}
				break;
			case ModelStates.PLAYING:
				if(config['file'] && !channel) {
					try {
						sound = new Sound(new URLRequest(config['file']));
						channel = sound.play();
						setVolume();
					} catch (err:Error) {
						Logger.log(err.message,'audiodescription');
					}
				}
				break;
		}
	};


	/** Check timing of the player to sync audio if needed. **/
	private function timeHandler(evt:ModelEvent):void {
		var pos:Number = evt.data.position;
		if(channel && view.config['state'] == ModelStates.PLAYING && Math.abs(pos-channel.position/1000) > 0.5) {
			channel.stop();
			channel = sound.play(pos*1000);
			setVolume();
		}
	};


};


}