package com.longtailvideo.plugins.audiodescription {

	import com.longtailvideo.jwplayer.events.*;
	import com.longtailvideo.jwplayer.player.*;
	import com.longtailvideo.jwplayer.plugins.*;
	import com.longtailvideo.jwplayer.view.interfaces.*;
	import com.longtailvideo.jwplayer.utils.Logger;

	import flash.display.*;
	import flash.events.*;
	import flash.media.*;
	import flash.net.*;
	import flash.utils.ByteArray;
	import flash.utils.getTimer;


	/**
	* Plugin for playing closed captions with a video.
	**/
	public class Audiodescription extends MovieClip implements IPlugin {


	[Embed(source="../../../../../assets/controlbar.png")]
	private const ControlbarButton:Class;
	[Embed(source="../../../../../assets/dock.png")]
	private const DockIcon:Class;


	/** Reference to the controlbar button. **/
	private var _button:MovieClip;
	/** Sound channel object. **/
	private var _channel:SoundChannel;
	/** A bunch of clips drawn for the debug menu. **/
	private var _clips:Object;
	/** List with configuration settings. **/
	private var _config:Object = {
		debug:false,
		ducking:false,
		file:undefined,
		state:true,
		volume:90
	};
	/** Currently active file. **/
	private var _current:String;
	/** Array with ducking samples. **/
	private var _ducks:Array;
	/** Reference to the dock icon. **/
	private var _icon:MovieClip;
	/** Is the sound already loaded. **/
	private var _loaded:Boolean;
	/* Reference to the JW Player. */
	private var _player:IPlayer;
	/** sound object to be instantiated. **/
	private var _sound:Sound;
	/** Current player state. **/
	private var _state:String;
	/** Volume of the main player. **/
	private var _volume:Number;
	/** Array with waveform samples. **/
	private var _waves:Array;


	/** Constructor; nothing there. **/
	public function Audiodescription() {};


	/** Clicking the controlbar icon or dock button. **/
	private function clickHandler(evt:MouseEvent):void {
		setState(!_config['state']);
	};


	/** 
	* Extract a waveform of the sound when it's completed. Optimizations todo:
	* 
	* - Don't reinitialyze arr but reload
	* - try forcing gc()
	* - use vectors instead of arrays
	* - try small buckets (onProgress) instead of at once (onComplete)
	* 
	**/
	private function completeHandler(evt:Event):void {
		if(!_config['ducking']) { return; }
		var stt:Number = getTimer();
		var arr:ByteArray = new ByteArray();
		var bts:Number = 4410;
		var pos:Number = 0;
		// load a sound sample every .1 second.
		while(bts == 4410) {
			bts = _sound.extract(arr,4410);
			arr.position = pos;
			if(arr.bytesAvailable > 7) {
				_waves.push(arr.readFloat());
			}
			pos += 8;
		}
		drawWave();
		// Calculate the ducking and add some margin.
		var i:Number = 0;
		while(i<_waves.length) {
			if(_waves[i] == 0) {
				_ducks.push(0);
				i++;
			} else {
				_ducks[i-2] = _ducks[i-1] = 1;
				_ducks.push(1,1,1);
				i += 3;
			}
		}
		// Add fades to the ducking.
		var blr:Number = 5;
		var tmp:Array = new Array();
		for(i=0; i <= blr; i++) { tmp.push(_ducks[i]); }
		for(i=blr; i < _ducks.length-blr; i++) {
			var ent:Number = 0;
			for(var j:Number = i - blr; j <= i + blr; j++) { 
				ent += _ducks[j];
			}
			tmp.push( ent / (blr*2+1) );
		}
		for(i =_ducks.length-blr; i < _ducks.length; i++) {
			tmp.push(_ducks[i]); 
		}
		_ducks = tmp;
		drawDucks();
		Logger.log("Calculated ducking in "+(getTimer()-stt)+" ms",id);
	};


	/** Draw the overall clips. **/
	private function drawClips():void {
		var back:Shape = new Shape();
		back.graphics.beginFill(0x000000,0.8);
		back.graphics.drawRect(0,0,480,100);
		addChild(back);
		var wave:Shape = new Shape();
		wave.y = 50;
		addChild(wave);
		var duck:Shape = new Shape();
		addChild(duck);
		var line:Shape = new Shape();
		line.graphics.lineStyle(0,0x3333FF);
		line.graphics.lineTo(0,100);
		addChild(line);
		_clips = {back:back,wave:wave,duck:duck,line:line};
	};


	/** Draw the audiodescription waveform. **/
	private function drawDucks():void {
		_clips.duck.graphics.moveTo(0,10);
		_clips.duck.graphics.lineStyle(0,0xFF0000);
		for(var i:Number=0; i < _ducks.length; i++) {
			_clips.duck.graphics.lineTo(i,10 + _ducks[i]*80);
		}
		_clips.duck.width = 480;
	};


	/** Draw the audiodescription waveform. **/
	private function drawWave():void {
		_clips.wave.graphics.moveTo(0,0);
		_clips.wave.graphics.lineStyle(0,0x00FF00);
		for(var i:Number=0; i < _waves.length; i++) {
			_clips.wave.graphics.lineTo(i, _waves[i]*50);
		}
		_clips.wave.width = 480;
	};


	/** Identifier string of this plugin in the JW Player. **/
	public function get id():String {
		return "audiodescription";
	};


	/** Initialize as a JW Player 5 plugin. */
	public function initPlugin(ply:IPlayer, cfg:PluginConfig):void {
		_player = ply;
		for (var prp:String in _config) {
			if (cfg[prp] != undefined) { _config[prp] = cfg[prp]; }
		}
		_player.addEventListener(PlayerStateEvent.JWPLAYER_PLAYER_STATE,stateHandler);
		_player.addEventListener(PlaylistEvent.JWPLAYER_PLAYLIST_ITEM,itemHandler);
		_player.addEventListener(MediaEvent.JWPLAYER_MEDIA_TIME,timeHandler);
		if (_player.config.dock) {
			var icn:DisplayObject = _player.skin.getSkinElement(id, "dockIcon");
			if (icn == null) { icn = new DockIcon(); }
			_icon = _player.controls.dock.addButton(icn,'is on',clickHandler);
		} else {
			var btn:DisplayObject = _player.skin.getSkinElement(id, "controlbarButton");
			if (btn == null) { btn = new ControlbarButton; }
			_button = _player.controls.controlbar.addButton(btn,id,clickHandler);
		}
		setState(_config['state']);
		drawClips();
	};


	/** Check for captions with a new playlistItem. **/
	private function itemHandler(evt:PlaylistEvent):void {
		_current = undefined;
		_loaded = false;
		if (_player.playlist.currentItem[id+'.file']) {
			_current = _player.playlist.currentItem[id+'.file'];
		} else if (_player.playlist.currentItem[id]) {
			_current = _player.playlist.currentItem[id];
		} else if (_config['file']) {
			_current = _config['file'];
		} else if (_player.config[id]) {
			_current = _player.config[id];
		}
		_ducks = new Array();
		_waves = new Array();
		_clips.duck.graphics.clear();
		_clips.wave.graphics.clear();
		if(_config['ducking']) {
			_player.volume(90);
		}
	};


	/** Resize this plugin */
	public function resize(wid:Number, hei:Number):void {
		if(_config['debug']) { 
			visible = true;
			width = wid;
		} else { 
			visible = false;
		}
	};


	/** Show/hide the captions, update the button, save state in cookie. **/
	public function setState(stt:Boolean):void {
		_config['state'] = stt;
		setVolume(_config['volume']);
		if (stt) {
			if (_icon) {
				_icon.field.text = "is on";
			} else {
				_button.alpha = 1;
			}
		} else {
			if (_icon) {
				_icon.field.text = "is off";
			} else {
				_button.alpha = 0.3;
			}
		}
		var cke:SharedObject = SharedObject.getLocal('com.jeroenwijering','/');
		cke.data[id+'.state'] = stt;
		cke.flush();
	};


	/** Set the volume level. **/
	private function setVolume(vol:Number):void {
		var trf:SoundTransform = new SoundTransform(vol/100);
		if(!_config['state']) { trf.volume = 0; }
		if(_channel) { _channel.soundTransform = trf; }
	};


	/** Captions are only visible when playing / paused. **/
	private function stateHandler(evt:PlayerStateEvent):void {
		_state = evt.newstate;
		switch(_state) {
			case PlayerState.PAUSED:
			case PlayerState.BUFFERING:
			case PlayerState.IDLE:
				if(_loaded) {
					_channel.stop();
				}
				break;
			case PlayerState.PLAYING:
				if(_current && !_loaded) {
					_loaded = true;
					_sound = new Sound(new URLRequest(_current));
					_sound.addEventListener(SecurityErrorEvent.SECURITY_ERROR, errorHandler);
					_sound.addEventListener(IOErrorEvent.IO_ERROR, errorHandler);
					_sound.addEventListener(Event.COMPLETE,completeHandler);
					_channel = _sound.play();
					setVolume(_config['volume']);
				}
				break;
		}
	};

	/** Sound cannotbe loaded. **/
	private function errorHandler(evt:ErrorEvent):void {
		Logger.log(evt.text,id);
		_loaded = false;
	};

	/** Check timing of the player to sync captions. **/
	private function timeHandler(evt:MediaEvent):void {
		var pos:Number = evt.position;
		if(_loaded && _state == PlayerState.PLAYING && 
			Math.abs(pos - _channel.position/1000) > 0.25) {
			Logger.log("Shifting audio "+ Math.round(pos*1000 - _channel.position) +" ms for syncing",id);
			_channel.stop();
			_channel = _sound.play(pos*1000);
			setVolume(_config['volume']);
		}
		if(_config['ducking'] && _config['state'] && _ducks.length > 0 && 
			_ducks[Math.round(pos*10)] != _volume) {
			_volume = _ducks[Math.round(pos*10)];
			_player.volume(90 - 80*_ducks[Math.round(pos*10)]);
		}
		_clips.line.x = Math.round(pos / evt.duration * 480);
	};


};


}