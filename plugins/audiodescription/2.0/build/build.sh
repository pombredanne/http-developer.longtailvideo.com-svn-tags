# This is a simple script that compiles the plugin using MXMLC (free & cross-platform).
# Learn more at http://developer.longtailvideo.com/trac/wiki/PluginsCompiling
# To use, make sure you have downloaded and installed the Flex SDK in the following directory:

FLEXPATH=/Developer/SDKs/flex_sdk_3

echo "Compiling with MXMLC..."

$FLEXPATH/bin/mxmlc ../src/com/longtailvideo/plugins/audiodescription/Audiodescription.as -sp ../src -o ../audiodescription.swf -library-path+=../lib -use-network=false -optimize=true -incremental=false -target-player="10.0.0"

# $FLEXPATH/bin/compc -source-path ../src -output ../captions.swc -compiler.library-path $FLEXPATH/frameworks/libs -compiler.library-path ../lib -include-classes com.longtailvideo.plugins.captions.Captions -link-report='../captions-classes.xml'