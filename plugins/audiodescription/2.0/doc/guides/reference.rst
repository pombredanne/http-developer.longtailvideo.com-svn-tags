Introduction
============

The Audiodescription plugin for JW Player supports playback of a closed audiodescription track (a separate MP3 file,  kept in sync with the video). Audiodescriptions can be used for accessibility purposes (people who have bad sight or are blind), for educational purposes (explanations of scenes or situations) or for *making of* style comments (from e.g. the director of a movie). 

The plugin supports playlists. Its on/off toggle can be skinned using the JW Player PNG skinning model. The plugin also offers a setting that ducks (80% mutes) the original audio when there's audiodescription data. This is useful in situations where voices from the original and the audiodescription tracks overlap.



.. _options:

Configuration Options
=====================

Like the `JW Player overall <http://www.longtailvideo.com/support/jw-player/jw-player-for-flash-v5/12536/configuration-options>`_, the Audiodescription plugin can be configured with options set in the embed code. The following configuration options (flashvars) are available:

.. describe:: audiodescription.file

   Location of the audiodescription file to playback. This should be the URL to an MP3 file with the same length as the video. Such an MP3 file generally contains both narrative and blank (no sound) sections.
   
   .. note::
   
      The MP3 file should be in a constant bitrate, 44.1 kHz or 22.05 kHz, mono or stereo format. Other values (especially variable bitrates) may lead Flash to playback the sound *chipmunked*.

.. describe:: audiodescription.state

   Describes whether to play the audiodescription on startup or not. The default is **true** (audiodescription is played). Can be set **false** as well.
   
   .. note::
   
      The *state* option is also saved in a cookie. When a user disables the audiodescription on one video, they will be disabled by default on subsequent videos he watches on the same site. The configuration option overrides the cookie.

.. describe:: audiodescription.volume

   The audiodescription does not respect the overall player volume setting. Instead, you can use this *audiodescription.volume* option to independently set the audiodescription's volume (**0** to **100**). Set it high (*100*) if the AD is relatively soft, or set it low (*20*) if the AD is relatively loud.
   
   .. note::
   
      If the issue is not volume, but overlapping narrations, check out the ducking option instead.

Ducking
-------

Version 2 of the Audiodescription plugin introduced a sophisticated feature to avoid overlapping narrations of the original and the audiodescription tracks. With ducking enabled, the audiodescription plugin will analyze the waveform of the audiodescription MP3. It then *ducks* (lowers) the volume of the original audio when it detects sound in the audiodescription. When there's no sound in the audiodescription, the volume of the original track is upped again.

The *ducking* feature takes over the volume control of the player. If there's no audiodescription wavedata, the original audio is always at a volume of *90*. If there is wavedata, the original audio is faded down to *10* and the AD is boosted up to *90*. This back-and-forth goes on until the end of the video is reached.

Ducking can be enabled with the following option:

.. describe:: audiodescription.ducking

   Set this option **true** to enable the ducking functionality. 
   
   .. note::
   
      When set, both the *audiodescription.volume* and the overall *volume* option are ignored.

The ducking process can be visualized with the following option:

.. describe:: audiodescription.debug
   
   Set this separate option to *true* to have the plugin render a visualization of the ducking process. It can be seen in the image below. The green line shows the waveform of the audiodescription and the red line shows the forced volume level of the original audio.

   .. image:: ../assets/ducking.png
      :alt: Skinning example.
   
.. note::
      
      Since calculating the waveform is pretty CPU-intensive, it is not recommended to use ducking for videos that are over a few minutes in length. Expect a few hundred milliseconds of calculation for every minute of video.


Dock Option
-----------

One option for `the player itself <http://www.longtailvideo.com/support/jw-player/jw-player-for-flash-v5/12536/configuration-options>`_ greatly influences the interface of the Audiodescription plugin. This option is called **dock** and can be set to either *true* (the default) or *false*.

* When set *true*, the button to play/mute audiodescriptions is displayed in the *dock*, an area in the top right of the display.
* When set *false* the button to play/mute audiodescriptions is displayed in the controlbar, usually at the right side, before the mute button (this can be set in the skin).

Dock buttons are larger than controlbar buttons, which allows for clearer graphics to display. Dock buttons automatically fade out after a few seconds of playback.

Examples
--------

Here is an example embed code of a player using the audiodescriptions plugin. This example uses the `SWFObject 2.0 <http://code.google.com/p/swfobject/>`_ embedding script:

.. code-block:: html

   <p id="container">The player will be placed here</p>
   
   <script type="text/javascript">
     swfobject.embedSWF("player.swf","container","480","270","10","false",{
       file: "/static/video.mp4",
       plugins: "audiodescription",
      "audiodescription.file": "/static/audiodescription.mp3"
     });
   </script>

Here is a second embed code, in which the *dock* is disabled (showing a controlbar button) and the *audiodescription.ducking* is enabled (ducking the original sound when there's AD narration):

.. code-block:: html

   <p id="container">The player will be placed here</p>
   
   <script type="text/javascript">
     swfobject.embedSWF("player.swf","container","480","270","10","false",{
       file: "/static/video.mp4",
       plugins: "audiodescription",
      "audiodescription.ducking": true,
      "audiodescription.file": "/static/audiodescription.mp3",
      dock: false
     });
   </script>


Supported Formats
=================

The audiodescription plugin solely supports a single MP3 file per video. The plugin does not support other formats (like WAV or AAC). It also doesn't support compositions of MP3 files (e.g. using SMIL).

Moreover, Flash is pretty picky when it comes to correctly playing back MP3 files. Only a small subset of MP3 is supported. For your audiodescription to correctly playback, we recommend the following settings:

* *bitrate*: 64, 96, 128, 160 or 192 kbps constant bit rate. 96 kbps seems to be a good filesize/quality tradeoff.
* *channels*: mono or joint stereo.
* *sample frequency*: 44.1kHz or 22.05kHz.


When your MP3 file is not supported by Flash, you'll be hearing the infamous *chipmunks* effect: the pitch of the audio is too high (or too low).


Creating audiodescriptions
--------------------------

Audiodescriptions are best created with a simple audio/video editor that supports recording of audio through a line-in or microphone. For example:

* **Windows Movie Maker** on Windows.
* **iMovie** on the MAC.

Take the following steps to create the audiodescription MP3:

1. Load the original video into your editor
2. Using e.g. a cheap headset, record the voiceovers you want to insert.
3. Position the voiceovers in such a way it overlaps as little as possible with the original audio.
4. **MUTE** the original audio.
5. Export the video, e.g. to an MP4 or WMV or MOV video. 
6. Use the (simple, free and crossplatform) Miro Video Converter tool to extract the audio as an MP3 file.

When done, you have an audiodescription MP3 of your video that can be directly loaded into the JW Player.

.. note:: 

   Do not keep your microphone running when you don't speak. Background noise will also trigger the ducking filter, resulting in the video's sound getting muted because of the background noise.


Playlist support
================

Audiodescriptions can be assigned to one or more videos in a playlist. You can videos with and videos without an audiodescription in a single feed. Since none of the XML playlist formats define elements for linking to audiodescription files, they should be set using the `JWPlayer XML namespace <http://www.longtailvideo.com/support/jw-player/jw-player-for-flash-v5/12537/xml-playlist-support>`_. In practice, the namespace is enabled by:

* Setting an **xmlns:jwplayer** attribute in the main XML tag.
* Prefixing the XML elements with **jwplayer:**, e.g. *<jwplayer:audiodescription.file>*.

Example
-------

Here is an example embed code for audiodescriptions with a playlist, using the `SWFObject 2.0 <http://code.google.com/p/swfobject/>`_ embedding script. Note the *audiodescription.file* option is not set:

.. code-block:: html

   <p id="container">The player will be placed here</p>
   
   <script type="text/javascript">
     swfobject.embedSWF("player.swf","container","480","270","10","false",{
       file: "/static/playlist.xml",
       plugins: "audiodescription",
       "audiodescription.ducking":true
     });
   </script>

And here is how the *playlist.xml* from this example can look like. The *audiodescription.file* option is now set for each entry:

.. code-block:: xml

   <rss version="2.0" xmlns:jwplayer="http://developer.longtailvideo.com/">
     <channel>
       <title>Example RSS playlist with captions</title>
   
       <item>
         <title>Coronation Street</title>
         <description>This entry has an audiodescription</description>
         <enclosure url="/static/corrie.flv" />
         <jwplayer:audiodescription.file>/static/corrie.mp3</jwplayer:audiodescription.file>
       </item>
   
       <item>
         <title>Big Buck Bunny</title>
         <description>This entry has another audiodescription</description>
         <enclosure url="/static/bunny.mp4" />
         <jwplayer:audiodescription.file>/static/bunny.mp3</jwplayer:audiodescription.file>
       </item>
   
     </channel>
   </rss>

.. note::
   
   For security purposes, so-called `crossdomain file restrictions  <http://www.longtailvideo.com/support/jw-player/jw-player-for-flash-v5/12541/crossdomain-file-loading-restrictions>`_ apply to playlists. Therefore make sure you have placed a *crossdomain.xml* on the webserver that hosts your playlist, if they are different from the *player.swf*'s webserver.




Skinning
========

The Audiodescription plugin includes support for styling its controlbar or dock button through the JW Player `PNG Skinning Model <http://www.longtailvideo.com/support/jw-player/jw-player-for-flash-v5/14/skinning-the-jw-player-5>`_. Styling of the ducking debug is not possible (end users should not see it).

Due to current limitations in the PNG skinning model, not all desired results can be achieved:

* Rollovers cannot be set, for neither the *dock* nor the *controlbar* button.
* Alternative icons (for the **off** state) cannot be set, for neither the *dock* nor the *controlbar* button.
* Contrary to regular controlbar buttons, the audiodescription controlbar button is given a margin of 5px to its left and to its right. You should take this into account, removing any margins you have in the button graphics.

Here's an example of a skin that includes custom Audiodescription graphics:

.. image:: ../assets/skinning.png
	:alt: Skinning example.

This skin, called **Stijl**, can be `freely downloaded here <http://www.longtailvideo.com/addons/skins/25/Stijl>`_.

XML Block
---------

Here is the XML code block you should include in your PNG skin to style the Audiodescription plugin:

.. code-block:: html

   ...
   <component name="audiodescription">
     <elements>
       <element name="controlbarButton" src="controlbar.png" />
       <element name="dockIcon" src="dock.png" />
     </elements>
   </component>

The two PNG images (*controlbar.png* and *dock.png*) should be placed in an **audiodescription** subdirectory of the main skin directory.




Changelog
=========

Version 2.0
-----------

* Migrated plugin to V5 API; new Audiodesription.as implements IPlugin and works natively with JW 5+. As of 2.0, the plugin doesn't work with the 4.x player anymore.
* Added support for ducking of the original audio. A fairly sophisticated feature that might ease the authoring of high volumes of audio descriptions. Do note it's pretty CPU-heavy and should only be used for short clips.
* Added basic support for PNG skinning, by allowing a custom controlbar and dock icon.

* Fixed an issue that threw an error when the playlistitem was unloaded while the MP3 was still transferring.
* Fixed an issue where the audiodescription of a previous video would still be in the player config after a LOAD event.