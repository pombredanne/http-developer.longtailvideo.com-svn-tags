package com.jeroenwijering.plugins {


import com.jeroenwijering.events.*;

import flash.display.*;
import flash.events.*;
import flash.geom.Rectangle;
import flash.text.*;

/**
* A simple plugin that displays a search bar.
**/
public class Searchbar extends MovieClip implements PluginInterface {


	/** Reference to the background clip. **/
	private var back:Sprite;
	/** Reference to the border clip. **/
	private var border:Sprite;
	/** Reference to the graphics. **/
	public var config:Object = {
		color:'cc0000',
		size:40,
		label:'Search',
		position:'bottom',
		script:'http://gdata.youtube.com/feeds/api/videos?vq=QUERY&format=5'
	};
	/** Reference to the textfield that contains the query. **/
	private var input:TextField;
	/** Reference to the textfield that labels the submit button. **/
	private var label:TextField;
	/** Reference to the button graphic.  **/
	private var submit:Sprite;
	/** Reference to the View of the player. **/
	private var view:AbstractView;


	/** Constructor. **/
	public function Searchbar():void {};


	/** Build the clip graphics. **/
	private function build():void {
		back = new Sprite();
		if(config['position'] == 'over') {
			back.graphics.beginFill(0x000000,0.7);
			back.graphics.drawRect(0,0,400,40);
		} else if (view.config['backcolor']) {
			back.graphics.beginFill(uint('0x'+view.config['backcolor']));
			back.graphics.drawRect(0,0,400,40);
		}
		back.graphics.beginFill(0xFFFFFF,0.07);
		back.graphics.drawRect(0,0,400,20);
		back.graphics.beginFill(0x000000,0.07);
		back.graphics.drawRect(0,20,400,20);
		addChild(back);
		border = new Sprite();
		border.graphics.beginFill(0x878787);
		border.graphics.drawRect(0,0,200,20);
		border.x = border.y = 10;
		addChild(border);
		input = new TextField();
		input.background = true;
		input.backgroundColor = 0xFFFFFF;
		input.type = TextFieldType.INPUT;
		input.defaultTextFormat = new TextFormat("_sans",11,0x000000);
		input.height = 18;
		input.x = input.y = 11;
		input.text = '...';
		addChild(input);
		submit = new Sprite();
		submit.graphics.beginFill(uint('0x'+config['color']));
		submit.graphics.drawRect(0,0,118,18);
		submit.graphics.beginFill(0xFFFFFF,0.08);
		submit.graphics.drawRect(0,0,118,9);
		submit.graphics.beginFill(0x000000,0.08);
		submit.graphics.drawRect(0,9,118,9);
		submit.y = 11;
		addChild(submit);
		label = new TextField();
		var fmt:TextFormat = new TextFormat("_sans",11,0xFFFFFF);
		fmt.align = 'center';
		label.defaultTextFormat = fmt;
		label.width = 118;
		label.text = config['label'];
		submit.addChild(label);
	};


	/** Start a search. **/
	private function clickHandler(evt:MouseEvent=null):void {
		var que:String = encodeURI(input.text);
		if(que.length > 2) {
			view.sendEvent('LOAD',config['script'].replace('QUERY',que));
		}
		input.text = '';
	};


	/** Clear the field on focus. **/
	private function focusHandler(evt:FocusEvent):void {
		if(input.text == '...') {
			input.text = '';
		}
	};


	/** The initialize call is invoked by the player View. **/
	public function initializePlugin(vie:AbstractView):void {
		view = vie;
		if(config['position'] == 'over') {
			view.addModelListener(ModelEvent.STATE,stateHandler);
		}
		view.addControllerListener(ControllerEvent.RESIZE,resizeHandler);
		if(view.config['lightcolor'] && !view.config['search.color']) {
			config['color'] = view.config['lightcolor'];
		}
		build();
		input.addEventListener(FocusEvent.FOCUS_IN,focusHandler);
		input.addEventListener(KeyboardEvent.KEY_DOWN,keyHandler);
		submit.addEventListener(MouseEvent.CLICK,clickHandler);
		submit.buttonMode = true;
		submit.mouseChildren = false;
		resizeHandler();
	};


	/** Start the search when pressing the enter key. **/
	private function keyHandler(evt:KeyboardEvent):void {
		if(evt.charCode == 13) {
			clickHandler();
		}
	};


	/** Handle a resize. **/
	private function resizeHandler(evt:ControllerEvent=undefined):void {
		back.width = view.config['width'];
		if(config['width']) {
			x = config['x'];
			y = config['y'];
			back.width = config['width'];
		}
		border.width = back.width - 20;
		input.width = back.width - 141;
		submit.x = back.width - 129;
	};


	/** Show the searchbox on video completed. **/
	private function stateHandler(evt:ModelEvent):void {
		switch(evt.data.newstate) {
			case ModelStates.BUFFERING:
			case ModelStates.PLAYING:
				visible = false;
				break;
			default:
				stage.focus = input;
				visible = true;
				break;
		}
	};


}


}