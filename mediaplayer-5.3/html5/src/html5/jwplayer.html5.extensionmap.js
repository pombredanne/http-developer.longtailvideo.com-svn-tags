/**
 * JW Player Video Media component
 *
 * @author zach
 * @version 1.0
 */
(function(jwplayer) {
	jwplayer.html5.extensionmap = {
		"3gp": "video/3gpp",
		"3gpp": "video/3gpp",
		"3g2": "video/3gpp2",
		"3gpp2": "video/3gpp2",
		"flv": "video/x-flv",
		"f4a": "audio/mp4",
		"f4b": "audio/mp4",
		"f4p": "video/mp4",
		"f4v": "video/mp4",
		"mov": "video/quicktime",
		"m4a": "audio/mp4",
		"m4b": "audio/mp4",
		"m4p": "audio/mp4",
		"m4v": "video/mp4",
		"mkv": "video/x-matroska",
		"mp4": "video/mp4",
		"sdp": "application/sdp",
		"vp6": "video/x-vp6",
		"aac": "audio/aac",
		"mp3": "audio/mp3",
		"ogg": "audio/ogg",
		"ogv": "video/ogg",
		"webm": "video/webm"
	};
})(jwplayer);
