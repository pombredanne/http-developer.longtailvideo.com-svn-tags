(function(jwplayer){

  var template = function(player, config, div) {
    
    player.onComplete(_hide);
    player.onPause(_hide);
    player.onTime(_count);
    function _count(event) {
        div.innerHTML = Math.round(event.duration - event.position);
        _style({opacity: 1});
    };
    function _hide() {
        _style({opacity: 0});
    };
    
    this.resize = function(width, height) {
        _style({
            position: 'absolute',
            margin: (height/2-30)+'px '+(width/2-30)+'px',
            background: 'black',
            color: 'red',
            fontSize: '32px',
            height: '60px',
            lineHeight: '60px',
            textAlign: 'center',
            opacity: 0,
            width: '60px'
        });
    };

    function _style(object) {
        for(var style in object) {
          div.style[style] = object[style];
        }
     };
  };

  jwplayer().registerPlugin('countdown', template);

})(jwplayer);