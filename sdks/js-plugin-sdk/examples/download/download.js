(function(jwplayer){

  var template = function(player, div, config) {
    
	var assets = {
		download: "http://www.longtailvideo.com/support/sites/default/files/download.png"
	}
	
    var goDownload = function() {
        var item = player.getPlaylistItem();
        if(item['downloadlink']) {
            document.location = item['downloadlink'];
        } else if(config.link) { 
            document.location = config.link;
        } else {
            document.location = item.file;
        }
    };
    
    function setup(evt) {
        player.getPlugin("dock").setButton(
            'downloadButton',
            goDownload,
            assets.download
        );
    };
    player.onReady(setup);
    
    this.resize = function(width, height) {};
  };

  jwplayer().registerPlugin('download', template);

})(jwplayer);