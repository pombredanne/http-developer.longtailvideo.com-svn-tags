(function(jwplayer){

  var template = function(player, config, div) {
    
    function setup(evt) {
        div.style.color = 'red';
        div.innerHTML = config.text;
    };
    player.onReady(setup);
    this.resize = function(width, height) {};
  };

  jwplayer().registerPlugin('helloworld', template);

})(jwplayer);