(function(jwplayer){

  var template = function(player, config, div) {

    player.onReady(_init);

    this.promote = function(html) {
        if(player.getRenderingMode() == 'flash') {
            var swf = document.getElementById(player.id);
            swf.promote(html);
        } else { 
            div.innerHTML = html;
            _styleLinks();
        }
    };
    
    this.resize = function(width, height) {
        if(String(width).indexOf('px') > 0) {
            height = Number(height.substr(0,height.length-2));
            width = Number(width.substr(0,width.length-2));
        }
        if(player.getRenderingMode() == 'flash') {
            _style(div, { display:'none' });
        } else { 
            _style(div, {
                background: 'black',
                color: 'white',
                lineHeight: '20px',
                margin: '20px',
                opacity: 0.8,
                padding:'10px',
                textAlign: 'center',
                top: 0,
                width: (width-60)+'px'
            });
        }
    };
    
    function _init() {
        if(config.html) {
            div.innerHTML = config.html;
        } else { 
            div.innerHTML = "No promo at this point...";
        }
        _styleLinks();
    };
    
    function _style(element,styles) {
        for(var property in styles) {
          element.style[property] = styles[property];
        }
     };
     
     function _styleLinks() {
        var links = div.getElementsByTagName('a');
        for(var i=0; i<links.length; i++) {
            _style(links[i], {color:'#09F'});
        }
    };
    
  };

  jwplayer().registerPlugin('promobox', template,'promobox.swf');

})(jwplayer);