package {

    import com.longtailvideo.jwplayer.player.IPlayer;
	import com.longtailvideo.jwplayer.plugins.IPlugin;
	import com.longtailvideo.jwplayer.plugins.PluginConfig;

    import flash.display.MovieClip;
    import flash.external.ExternalInterface;
    import flash.text.StyleSheet;
    import flash.text.TextField;


    /** Plugin for showing a promo text over the video. **/
    public class Promobox extends MovieClip implements IPlugin {


        /** Reference to the background. **/
        private var _back:MovieClip;
        /** Reference to the textfield. **/
        private var _field:TextField;
        /** Default HTML to load into the promo. **/
        private var _html:String = "No promo at this point ...";
        /** Style properties for links. **/
        private var _linkStyle:Object = { 
            color: '#0099FF',
            textDecoration: 'underline'
        };
        /* Reference to the JW Player. */
        private var _player:IPlayer;
        /* Stylesheet for text formatting. */
        private var _sheet:StyleSheet;
        /** Style properties for regular text. **/
        private var _textStyle:Object = {
            color: '#FFFFFF',
            fontSize: 16,
            leading: 5,
            textAlign: 'center'
        };


        /** Constructor; creates background and textfield. **/
        public function Promobox() {
            _back = new MovieClip();
            _back.x = _back.y = 20;
            _back.graphics.beginFill(0x0000,0.8);
            _back.graphics.drawRect(0,0,360,60);
            addChild(_back);
            _field = new TextField();
            _field.x = _field.y = 30;
            _field.autoSize = "center";
            _field.multiline = true;
            _field.wordWrap = true;
            _field.width = 400;
            _field.height = 10;
            addChild(_field);
            _sheet = new StyleSheet();
            _sheet.setStyle("p", _textStyle);
            _sheet.setStyle("a", _linkStyle);
            _field.styleSheet = _sheet;
        };


        /** Identifier string of this plugin in the JW Player. **/
        public function get id():String {
            return "promobox";
        };


        /** Connect to player API and expose javasript call. */
        public function initPlugin(player:IPlayer, config:PluginConfig):void {
            _player = player;
            if(config.html) {
                _html = config.html;
            }
            promote(_html);
            ExternalInterface.addCallback("promote",promote);
        };


        /** Resize the plugin, making the back extend over the field. */
        public function resize(width:Number, height:Number):void {
            _back.width = width - 40;
            _field.width = width - 60;
            _back.height = _field.height + 20;
        };


        /** Set a promotional text. */
        public function promote(html:String):void {
            _field.htmlText = '<p>'+html+'</p>';
            resize(_player.config['width'], _player.config['height']);
        };


};


}
