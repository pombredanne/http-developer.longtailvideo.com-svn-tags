# This is a simple script that compiles the plugin using MXMLC (free & cross-platform).
# Learn more at http://developer.longtailvideo.com/trac/wiki/PluginsCompiling
# To use, make sure you have downloaded and installed the Flex SDK in the following directory:

FLEXPATH=/Developer/SDKs/flex_sdk_3

echo "Compiling with MXMLC..."

$FLEXPATH/bin/mxmlc BreakingNews.as -sp ./ -o ../breakingnews.swf -library-path+=./  -use-network=false -optimize=true -incremental=false
