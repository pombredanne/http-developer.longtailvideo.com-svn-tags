package {

    import com.longtailvideo.jwplayer.player.IPlayer;
	import com.longtailvideo.jwplayer.plugins.IPlugin;
	import com.longtailvideo.jwplayer.plugins.PluginConfig;

    import flash.display.MovieClip;
    import flash.external.ExternalInterface;
    import flash.text.StyleSheet;
    import flash.text.TextField;
    import flash.system.Security;


    /** Plugin for showing a promo text over the video. **/
    public class BreakingNews extends MovieClip implements IPlugin {


        /** Reference to the background. **/
        private var _back:MovieClip;
        /** Reference to the textfield. **/
        private var _field:TextField;
        /* Reference to the JW Player. */
        private var _player:IPlayer;
        /* Stylesheet for text formatting. */
        private var _sheet:StyleSheet;
        /** Style properties for regular text. **/
        private var _textStyle:Object = {
            color: '#FFFFFF',
            fontSize: 15,
            textAlign: 'center'
        };


        /** Constructor; creates background and textfield. **/
        public function BreakingNews() {
            Security.allowDomain('*');
            Security.allowInsecureDomain('*');
            _back = new MovieClip();
            _back.graphics.beginFill(0x0000,0.6);
            _back.graphics.drawRect(0,0,400,46);
            addChild(_back);
            _field = new TextField();
            _field.selectable = false;
            _field.width = 400;
            _field.height = 30;
            _field.y = 15;
            addChild(_field);
            _sheet = new StyleSheet();
            _sheet.setStyle("p", _textStyle);
            _field.styleSheet = _sheet;
        };


        /** Identifier string of this plugin in the JW Player. **/
        public function get id():String {
            return "breakingnews";
        };


        /** Connect to player API and expose javasript call. */
        public function initPlugin(player:IPlayer, config:PluginConfig):void {
            _player = player;
            populate(config.text);
            ExternalInterface.addCallback("setBreakingNews",populate);
        };


        /** Resize the plugin, making the back extend over the field. */
        public function resize(width:Number, height:Number):void {
            _back.width = width;
            _field.width = width;
        };


        /** Set a promotional text. */
        public function populate(text:String):void {
            _field.htmlText = '<p>'+text+'</p>';
            resize(_player.config['width'], _player.config['height']);
        };


};


}
