(function(jwplayer){

  var template = function(player, config, div) {
    
    function _setup() {
        div.innerHTML = config.text;
    };
    player.onReady(_setup);
    
    this.setBreakingNews = function(text) {
        if(player.getRenderingMode() == 'flash') {
            var swf = document.getElementById(player.id)
            swf.setBreakingNews(text);
        } else { 
            div.innerHTML = text;
        }
    };
    
    this.resize = function(width, height) {
        if(player.getRenderingMode() == 'flash') {
            _style(div, { display:'none' });
        } else { 
            _style(div, {
                background: 'black',
                color: 'white',
                lineHeight: '46px',
                opacity: 0.6,
                textAlign: 'center',
                width: width+'px'
            });
        }
    };
    
    function _style(element,styles) {
        for(var property in styles) {
          element.style[property] = styles[property];
        }
     };
    
  };

  jwplayer().registerPlugin('breakingnews', template,'breakingnews.swf');

})(jwplayer);