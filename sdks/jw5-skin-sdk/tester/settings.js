var settings = {
	/** Player versions to test. **/
	players: {
		'5.10':'./players/5.10.swf',
		'5.9':'./players/5.9.swf',
		'5.8':'./players/5.8.swf',
		'5.7':'./players/5.7.swf',
		'5.6':'./players/5.6.swf',
		'5.5':'./players/5.5.swf',
		'5.4':'./players/5.4.swf',
		'5.3':'./players/5.3.swf',
		'5.2':'./players/5.2.swf',
		'5.1':'./players/5.1.swf',
		'5.0':'./players/5.0.swf'
	},
	/** Available plugins to test. **/
	plugins: {
	},
	/** Skins to test. **/
	skins: {
		none:'',
		beelden:'../skins/beelden/beelden.xml',
        lulu:'../skins/lulu/lulu.xml'
	},
	/** All the setup examples with their flashvars. **/
	examples: {
		1: {
			title:'FLV video',
			file:'../../testing/files/bunny.flv',
			image:'files/bunny.jpg',
			height:240,
			width:400
		},
		2: {
			title:'MP4 video',
			file:'../../testing/files/bunny.mp4',
			image:'files/bunny.jpg',
			height:240,
			width:400
		},
		3: {
			title:'MP3 audio',
			file:'files/bunny.mp3',
			height:20,
			width:400
		},
		4: {
			title:'AAC audio',
			file:'../../testing/files/bunny.m4a',
			height:20,
			width:400
		},
		5: {
			title:'JPG image',
			file:'files/bunny.jpg',
			height:240,
			width:400
		},
		6: {
			title:'PNG image',
			file:'files/bunny.png',
			height:240,
			width:400
		},
		7: {
			title:'Youtube video',
			file: 'http://youtube.com/watch?v=IBTE-RoMsvw',
			height: 240,
			width: 400
		},
		10: {},
		11: {
			title:'HTTP streamed FLV',
			file:'http://content.bitsontherun.com/videos/Qvxp3Jnv-68183.flv',
			type:'http',
			height:240,
			width:400
		},
		12: {
			title:'HTTP streamed MP4',
			file:'http://content.bitsontherun.com/videos/Qvxp3Jnv-483.mp4',
			type:'bitgravity',
			height:240,
			width:400
		},
		13: {
			title:'RTMP streamed FLV',
			file:'videos/Qvxp3Jnv-68183.flv',
			type:'rtmp',
			streamer:'rtmp://fms.12E5.edgecastcdn.net/0012E5',
			height:240,
			width:400
		},
		14: {
			title:'RTMP streamed MP4',
			file:'videos/Qvxp3Jnv-483.mp4',
			type:'rtmp',
			streamer:'rtmp://fms.12E5.edgecastcdn.net/0012E5',
			height:240,
			width:400
		},
		15: {
			title:'RTMP streamed MP3',
			file:'videos/Qvxp3Jnv-68182.mp3',
			type:'rtmp',
			streamer:'rtmp://fms.12E5.edgecastcdn.net/0012E5',
			height:240,
			width:400
		},
		16: {
			title:'RTMP live broadcast',
			file:'live24x7',
			streamer:'rtmp://llnwpm.fc.llnwd.net/llnwpm',
			type:'fcsubscribe',
			height:240,
			width:500
		},
		16: {
			title:'RTMP dynamic stream',
			file:'files/dynamic.xml',
			height:240,
			width:500
		},
		20: {},
		21: {
			title:'ASX playlist',
			file:'files/asx.xml',
			height:240,
			width:800,
			playlist:'right',
			playlistsize:400
		},
		22: {
			title:'ATOM playlist',
			file:'files/atom.xml',
			height:240,
			width:800,
			playlist:'right',
			playlistsize:400
		},
		23: {
			title:'iRSS playlist',
			file:'files/irss.xml',
			height:240,
			width:800,
			playlist:'right',
			playlistsize:400
		},
		24: {
			title:'mRSS playlist',
			file:'files/mrss.xml',
			height:240,
			width:800,
			playlist:'right',
			playlistsize:400
		},
		25: {
			title:'SMIL playlist',
			file:'files/smil.xml',
			height:240,
			width:800,
			playlist:'right',
			playlistsize:400
		},
		26: {
			title:'XSPF playlist',
			file:'files/xspf.xml',
			height:240,
			width:800,
			playlist:'right',
			playlistsize:400
		},
		30: {},
		31: {
			title:'Youtube playlist',
			file:'http://gdata.youtube.com/feeds/api/standardfeeds/recently_featured?v=2',
			height:240,
			width:800,
			playlist:'right',
			playlistsize:400
		},
		32: {
			title:'Multibitrate playlist',
			file:'files/bitrates.xml',
			height:240,
			width:800,
			playlist:'right',
			playlistsize:400,
			'rtmp.dynamic':true,
			plugins:'qualitymonitor'
		},
		33: {
			title:'Chapter playlist',
			file:'files/chapters.xml',
			height:240,
			width:800,
			playlist:'right',
			playlistsize:400
		},
		40: {},
		41: {
			title:'Different colors',
			file:'files/mrss.xml',
			height:240,
			width:800,
			playlist:'right',
			playlistsize:400,
			backcolor:'000000',
			frontcolor:'CCCCCC',
			lightcolor:'77AA22',
			screencolor:'FFFFFF'
		},
		42: {
			title:'Logo, title and no click',
			file:'files/mrss.xml',
			height:240,
			width:800,
			playlist:'right',
			playlistsize:400,
			icons:'false',
			displayclick:'none',
			logo:'files/logo.png',
			controlbar:'over'
		},
		43: {
			title:'Autostart, shuffle and repeat',
			file:'files/mrss.xml',
			repeat:'always',
			height:240,
			width:800,
			autostart:'true',
			frontcolor:'000000',
			lightcolor:'77AA22',
			shuffle:'true',
			repeat:'always',
			playlist:'right',
			playlistsize:400
		},
		44: {
			title:'Stretched, stacked and muted',
			file:'files/mrss.xml',
			stretching:'fill',
			height:240,
			width:600,
			playlist:'over',
			controlbar:'over',
			mute:"true",
			playlistsize:400
		},
		45: {
			title:'Loading from config xml',
			config:'files/config.xml',
			height:240,
			width:500
		},
		46: {
			title:'Start and duration flashvars',
			file:'files/bunny.mp3',
			height:20,
			width:400,
			start:5,
			duration:20,
			repeat:'always',
			autostart:'true'
		},
		50: {},
		51: {
			title:'Audiodescription and captions plugins',
			file:'../../testing/files/corrie.flv',
			height:240,
			width:500,
			plugins:'audiodescription,captions',
			'captions.file':'files/corrie.xml',
			'audiodescription.file':'files/corrie.mp3'
		},
		52: {
			title:'Autostarter plugin',
			file:'files/bunny.mp3',
			height:20,
			width:500,
			plugins:'autostarter'
		},
		53: {
			title:'Infobox and logobox plugins',
			file:'files/mrss.xml',
			height:320,
			width:400,
			plugins:'infobox,logobox',
			'infobox.color':'444444',
			'logobox.file':'files/watermark.png',
			'logobox.link':'http://www.longtailvideo.com'
		},
		54: {
			title:'Clickproxy plugin',
			height:240,
			width:800,
			file:'../../testing/files/bunny.flv',
			plugins:'clickproxy',
			'clickproxy.listener':'$.alert'
		},
		55: {
			title:'HD plugin',
			file:'../../testing/files/bunny.flv',
			height:240,
			width:500,
			plugins:'hd',
			dock:'true',
			'hd.file':'../../testing/files/bunny.mp4'
		},
		56: {
			title:'Livestream plugin',
			file:'files/bunny.png',
			height:260,
			width:500,
			plugins:'livestream',
			'livestream.file':'isight',
			'livestream.streamer':'rtmp://fml.lhr.12E5.edgecastcdn.net/2012E5'
		},
		57: {
			title:'Metaviewer plugin',
			file:'../../testing/files/bunny.flv',
			height:260,
			width:800,
			repeat:'always',
			stretching:'fill',
			plugins:'metaviewer',
			'metaviewer.position':'right',
			'metaviewer.size':400
		},
		58: {
			title:'Revolt plugin',
			file:'files/bunny.mp3',
			height:260,
			width:500,
			repeat:'always',
			plugins:'revolt',
			'revolt.gain':2
		},
		59: {
			title:'Searchbar plugin',
			file:'../../testing/files/bunny.flv',
			image:'files/bunny.jpg',
			stretching:'fill',
			playlist:'over',
			height:260,
			width:460,
			plugins:'searchbar'
		},
		60: {
			title:'Sharing plugin',
			dock:true,
			file:'../../testing/files/bunny.flv',
			height:260,
			width:460,
			plugins:'sharing',
			'sharing.code':'<embed src="http://content.bitsontherun.com/players/nPripu9l-1754.swf" width="400" height="250" allowscriptaccess="always" />',
			'sharing.link':'http://www.bigbuckbunny.org/'
		},
		61: {
			title:'Snapshot plugin',
			file:'../../testing/files/bunny.mp4',
			height:260,
			width:600,
			plugins:'snapshot',
			'snapshot.script':'files/snapshot.php'
		},
		62: {
			title:'Spoton plugin',
			file:'../../testing/files/bunny.flv',
			image:'files/bunny.jpg',
			height:240,
			width:400,
			plugins:'spoton'
		},
		63: {
			title:'Flow plugin',
			file:'http://gdata.youtube.com/feeds/api/standardfeeds/recently_featured?v=2',
			height:260,
			width:600,
			plugins:'flow'
		},
		64: {
			title:'Popout plugin',
			file:'../../testing/files/bunny.flv',
			height:260,
			width:400,
			dock:true,
			plugins:'popout',
			'popout.file':'files/popout.html'
		}
	}
}
