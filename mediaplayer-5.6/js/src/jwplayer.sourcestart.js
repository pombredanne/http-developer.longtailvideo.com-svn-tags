/**
 * JW Player Source start cap
 * 
 * This will appear at the top of the JW Player source
 * 
 * @version 5.6
 */

 if (typeof jwplayer == "undefined") {