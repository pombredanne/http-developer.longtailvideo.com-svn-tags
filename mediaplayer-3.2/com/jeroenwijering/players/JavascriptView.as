﻿/**
* Javascript user interface management of the players MCV pattern.
*
* @author	Jeroen Wijering
* @version	1.01
**/


import com.jeroenwijering.players.*;


class com.jeroenwijering.players.JavascriptView extends AbstractView { 


	/** Constructor **/
	function JavascriptView(ctr:AbstractController,car:Object,far:Array) {
		super(ctr,car,far);
		if(flash.external.ExternalInterface.available) {
			var scs:Boolean = flash.external.ExternalInterface.addCallback("sendEvent",this,sendEvent);
		}
	};


	/** override of the update receiver, forwarding all to javascript **/
	public function getUpdate(typ:String,pr1:Number,pr2:Number):Void { 
		if(flash.external.ExternalInterface.available) {
			flash.external.ExternalInterface.call("getUpdate",typ,pr1,pr2);
		}
	};


}